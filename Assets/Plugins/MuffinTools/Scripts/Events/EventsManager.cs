﻿using UnityEngine.Events;

using System.Collections.Generic;

namespace MuffinTools
{

/// <summary>
/// 
///		
/// 
/// </summary>
public class EventsManager : Singleton<EventsManager>
{

	#region Subclasses & Enums

		private class ObjectEvent : UnityEvent<object> { }

	#endregion


	#region Attributes

		// References

		private Dictionary<string, ObjectEvent> m_EventsList = null;

	#endregion

	
	#region Initialisation / Destruction

		protected EventsManager()
		{
			m_EventsList = new Dictionary<string, ObjectEvent>();
		}

	#endregion

	
	#region Public Methods

		/// <summary>
		/// Add a given listener to the named Event.
		/// If the event has never been initialized, create a new one.
		/// </summary>
		/// <param name="_EventName">The name of the Event you want to listen.</param>
		/// <param name="_Listener">The listener to add to the named Event.</param>
		public static void StartListening(string _EventName, UnityAction<object> _Listener)
		{
			ObjectEvent eventToListen = null;

			if(Instance.m_EventsList.TryGetValue(_EventName, out eventToListen))
			{
				eventToListen.AddListener(_Listener);
			}

			else
			{
				eventToListen = new ObjectEvent();
				eventToListen.AddListener(_Listener);

				Instance.m_EventsList.Add(_EventName, eventToListen);
			}
		}

		/// <summary>
		/// Remove a listener from the named Event.
		/// </summary>
		/// <param name="_EventName">The name of the Event you want to remove the listener.</param>
		/// <param name="_Listener">The listener to remove from the named Event.</param>
		public static void StopListening(string _EventName, UnityAction<object> _Listener)
		{
			ObjectEvent eventToListen = null;

			if (Instance.m_EventsList.TryGetValue(_EventName, out eventToListen))
			{
				eventToListen.RemoveListener(_Listener);
			}
		}

		/// <summary>
		/// Call the named Event.
		/// If the named Event doesn't exist, nothing happend.
		/// </summary>
		/// <param name="_EventName">The name of the Event to trigger.</param>
		/// <param name="_Parameter1">the parameter to give to the Event's listeners.</param>
		public static void TriggerEvent(string _EventName, object _Parameter1 = null)
		{
			ObjectEvent eventToTrigger = null;

			if (Instance.m_EventsList.TryGetValue(_EventName, out eventToTrigger))
			{
				eventToTrigger.Invoke(_Parameter1);
			}
		}

	#endregion

}

}