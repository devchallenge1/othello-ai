﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

///<summary>
/// 
///		The MaxAttribute block a value at a given number
///	if the value goes over that given number. The attribute
///	can be used on :
///		- Float
///		- Integer
///		- Vector2
///		- Vector3
/// 
///</summary>

public class MaxAttribute : PropertyAttribute
{

	#region Members

		private float m_Max = 0.0f;

	#endregion


	#region Initialisation / Destruction

		private MaxAttribute()
		{

		}

		public MaxAttribute(float _Max)
		{
			m_Max = _Max;
		}

		public MaxAttribute(int _Max)
		{
			m_Max = _Max;
		}

	#endregion


	#region Accessors

		public float Max
		{
			get { return m_Max; }
		}

	#endregion

}

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(MaxAttribute))]
public class MaxDrawer : PropertyDrawer
{

	#region UI

		public override void OnGUI(Rect _Position, SerializedProperty _Property, GUIContent _Label)
		{
			MaxAttribute	maxAttribute	= attribute as MaxAttribute;
			float			max				= maxAttribute.Max;

			switch(_Property.propertyType)
			{
				case SerializedPropertyType.Float :
					_Property.floatValue = GetValue(max, _Property.floatValue);
					break;

				case SerializedPropertyType.Integer :
					_Property.intValue = GetValue(max, _Property.intValue);
					break;

				case SerializedPropertyType.Vector2 :
					_Property.vector2Value = GetValue(max, _Property.vector2Value);
					break;

				case SerializedPropertyType.Vector3 :
					_Property.vector3Value = GetValue(max, _Property.vector3Value);
					break;

				default :
					EditorGUI.HelpBox(_Position, "The Max attribute can't be use for this property type.", MessageType.None);
					break;
			}

			EditorGUI.PropertyField(_Position, _Property);
		}

	#endregion

	
	#region Accessors

		private int GetValue(float _Max, int _Value)
		{
			return System.Convert.ToInt32(GetClampedValue(_Max, _Value));
		}

		private float GetValue(float _Max, float _Value)
		{
			return GetClampedValue(_Max, _Value);
		}

		private Vector2 GetValue(float _Max, Vector2 _Value)
		{
			_Value.x = GetClampedValue(_Max, _Value.x);
			_Value.y = GetClampedValue(_Max, _Value.y);

			return _Value;
		}

		private Vector3 GetValue(float _Max, Vector3 _Value)
		{
			_Value.x = GetClampedValue(_Max, _Value.x);
			_Value.y = GetClampedValue(_Max, _Value.y);
			_Value.z = GetClampedValue(_Max, _Value.z);

			return _Value;
		}

		private float GetClampedValue(float _Max, float _Value)
		{
			_Value = (_Value > _Max) ? _Max : _Value;

			return _Value;
		}

	#endregion

}

#endif