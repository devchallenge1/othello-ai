﻿using UnityEngine;

namespace MuffinTools
{

///<summary>
/// 
///		
/// 
///</summary>

[AddComponentMenu("MuffinTools/Multiples Spawner")]
public class MultiplesSpawner : MonoBehaviour
{

	#region Attributes

		// Delegates

		private delegate void IterationAction(GameObject _ObjectToSpawn, Vector3 _Position, Vector3 _Size);

		// References

		[Header("References")]

		[SerializeField]
		private GameObject m_ObjectToSpawn = null;

		// Settings

		[Header("Settings")]

		[SerializeField, Min(0)]
		private Vector3 m_DistanceBetweenObjects = Vector3.zero;

		[SerializeField, Min(1)]
		[Tooltip("The given values will be rounded down.")]
		private Vector3 m_IterationsPerAxis = Vector3.zero;

		[SerializeField]
		private bool m_DestroyAfterSpawn = false;

	#endregion

	
	#region Engine Methods

		private void Start()
		{
			SpawnObjects(GetObjectToSpawn(), SpawnObject);

			if(m_DestroyAfterSpawn)
			{
				Destroy(gameObject);
			}
		}

	#endregion

	
	#region Public Methods
	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods

		private void SpawnObject(GameObject _ObjectToSpawn, Vector3 _Position, Vector3 _Size)
		{
			GameObject obj = Instantiate(_ObjectToSpawn.gameObject, _Position, Quaternion.identity);
			MultiplesSpawner spawner = obj.GetComponent<MultiplesSpawner>();
			if (spawner != null)
			{
				Destroy(spawner);
			}
		}

		private void SpawnObjects(GameObject _ObjectToSpawn, IterationAction _IterationAction)
		{
			Vector3 extents = _ObjectToSpawn.GetExtents();

			Vector3 start = _ObjectToSpawn.transform.position;
			int xIterations = Mathf.FloorToInt(m_IterationsPerAxis.x);
			int yIterations = Mathf.FloorToInt(m_IterationsPerAxis.y);
			int zIterations = Mathf.FloorToInt(m_IterationsPerAxis.z);
			start.x -= (extents.x * xIterations * 2 + m_DistanceBetweenObjects.x * (xIterations - 1)) / 2;
			start.y -= (extents.y * yIterations * 2 + m_DistanceBetweenObjects.y * (yIterations - 1)) / 2;
			start.z -= (extents.z * zIterations * 2 + m_DistanceBetweenObjects.z * (zIterations - 1)) / 2;
			Vector3 position = start;

			for (int x = 0; x < m_IterationsPerAxis.x; x++)
			{
				position.x = start.x + extents.x + ((extents.x * x * 2) + (m_DistanceBetweenObjects.x * x));
				for (int y = 0; y < m_IterationsPerAxis.y; y++)
				{
					position.y = start.y + extents.y + ((extents.y * y * 2) + (m_DistanceBetweenObjects.y * y));
					for (int z = 0; z < m_IterationsPerAxis.z; z++)
					{
						position.z = start.z + extents.z + ((extents.z * z * 2) + (m_DistanceBetweenObjects.z * z));
						_IterationAction(_ObjectToSpawn, position, extents * 2);
					}
				}
			}
		}

	#endregion

	
	#region Accessors

		private GameObject GetObjectToSpawn()
		{
			return (m_ObjectToSpawn != null) ? m_ObjectToSpawn : gameObject;
		}

	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR

		private void DrawObjectGizmo(GameObject _ObjectToSpawn, Vector3 _Position, Vector3 _Size)
		{
			Gizmos.DrawWireCube(_Position, _Size);
		}

		private void OnDrawGizmosSelected()
		{
			SpawnObjects(GetObjectToSpawn(), DrawObjectGizmo);
		}

		#endif

	#endregion

}

}