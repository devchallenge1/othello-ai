﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace MuffinTools
{

///<summary>
/// 
///</summary>
public class SpriteAnimator : MonoBehaviour
{

	#region Attributes

		// References

		[Header("References")]

		[SerializeField]
		private SpriteAnimation[] m_Animations = { };

		private SpriteRenderer m_SpriteRenderer = null;
		private Image m_ImageRenderer = null;

		private List<int> m_UnplayedAnimationsIndexes = new List<int>();

		// Settings

		[Header("Settings")]

		[SerializeField]
		private bool m_PlayAtStart = true;

		[SerializeField, IfChecked("m_PlayAtStart")]
		private bool m_PlayRandomAtStart = true;

		// Flow

			// Settings

		private bool m_UseImageRenderer = false;

			// Statements

		private bool m_IsPlaying = false;

			// Current animation

		private SpriteAnimation m_PlayingAnimation = null;
		private float m_AnimationDuration = 0.0f;
		private float m_AnimationTimer = 0.0f;
		private bool m_Reverse = false;

	#endregion

	
	#region Engine Methods

		private void Awake()
		{
			SetRenderer();
			ResetUnplayedAnimations();
		}

		private void Start()
		{
			if(m_PlayAtStart)
			{
				int animationIdx = (m_PlayRandomAtStart) ? GetRandomAnimationIndex() : 0;
				Play(animationIdx);
			}
		}

		private void Update()
		{
			if(UpdateAnimation(Time.deltaTime))
			{
				UpdateRenderer();
			}
		}

	#endregion

	
	#region Public Methods

		public bool Play(int _AnimationIdx)
		{
			SpriteAnimation anim = GetAnimation(_AnimationIdx);
			if(CanPlay(anim))
			{
				m_PlayingAnimation = anim;
				m_AnimationDuration = anim.Sprites.Length * anim.Interval;
				m_AnimationTimer = 0.0f;
				m_IsPlaying = true;

				OnPlayAnimationSuccess(_AnimationIdx);
				return true;
			}
			return false;
		}

		public bool Play(string _AnimationName)
		{
			int idx = GetAnimationIndex(_AnimationName);
			if(idx != -1)
			{
				return Play(idx);
			}
			else
			{
				Debug.LogWarning("ERROR from SpriteAnimator.Play() : The animation \"" + _AnimationName + "\" doesn't exist on this SpriteAnimator.");
			}
			return false;
		}

		public bool PlayRandom()
		{
			int idx = GetRandomAnimationIndex();
			if(idx != -1)
			{
				return Play(idx);
			}
			else
			{
				Debug.LogWarning("ERROR from SpriteAnimator.PlayRandom() : No animations in list.");
			}

			return false;
		}

		/// <summary>
		/// Pauses the current animation.
		/// </summary>
		/// <returns>Returns true if the operation is successful, false if no animation was playing.</returns>
		public void Pause()
		{
			m_IsPlaying = false;
		}

		public void Stop()
		{
			m_PlayingAnimation = null;
			m_AnimationDuration = 0.0f;
			m_AnimationTimer = 0.0f;
			m_IsPlaying = false;
		}

		public void ResetUnplayedAnimations()
		{
			int count = m_Animations.Length;
			for(int i = 0; i < count; i++)
			{
				m_UnplayedAnimationsIndexes.Add(i);
			}
		}

	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods

		/// <summary>
		/// Checks if the given SpriteAnimation asset can be played :
		///		- not null
		///		- with at least 1 sprite
		/// </summary>
		private bool CanPlay(SpriteAnimation _Animation)
		{
			return
			(
				_Animation != null &&
				_Animation.Sprites.Length > 0
			);
		}

		/// <summary>
		/// Updates the animation timer and state.
		/// </summary>
		/// <returns>Returns true if the animation can be played at the current frame, otherwise false.</returns>
		private bool UpdateAnimation(float _DeltaTime)
		{
			if(IsPlaying)
			{
				m_AnimationTimer += _DeltaTime;
				if(m_AnimationTimer >= m_AnimationDuration)
				{
					if(m_PlayingAnimation.PlayMode != SpriteAnimationPlayMode.Once)
					{
						float difference = (m_AnimationTimer - m_AnimationDuration);
						m_AnimationTimer = 0.0f + difference;

						if(m_PlayingAnimation.PlayMode == SpriteAnimationPlayMode.PingPong)
						{
							m_Reverse = !m_Reverse;
						}
					}
					else
					{
						m_IsPlaying = false;
						return false;
					}
				}

				return true;
			}

			return false;
		}

		/// <summary>
		/// Updates the sprite to display in SpriteRenderer.
		/// </summary>
		private void UpdateRenderer()
		{
			int count = m_PlayingAnimation.Sprites.Length;
			float ratio = (m_Reverse) ? (Mathf.Abs(AnimationRatio - 1)) : AnimationRatio;
			int currentSprite = Mathf.FloorToInt(count * ratio);
			if(currentSprite >= count)
			{
				currentSprite = count - 1;
			}
			SetSprite(m_PlayingAnimation.Sprites[currentSprite]);
		}

		private void OnPlayAnimationSuccess(int _AnimationIdx)
		{
			int count = m_UnplayedAnimationsIndexes.Count;
			int removeAt = -1;
			for(int i = 0; i < count; i++)
			{
				if(m_UnplayedAnimationsIndexes[i] == _AnimationIdx)
				{
					removeAt = i;
					break;
				}
			}

			if(removeAt != -1)
			{
				m_UnplayedAnimationsIndexes.RemoveAt(removeAt);
			}
		}

	#endregion

	
	#region Accessors

		/// <summary>
		/// Set a sprite using SpriteRenderer or Image compoenent on this GameObject.
		/// </summary>
		private bool SetSprite(Sprite _Sprite)
		{
			if(m_UseImageRenderer)
			{
				if(m_ImageRenderer != null)
				{
					m_ImageRenderer.sprite = _Sprite;
					return false;
				}
			}

			if(m_SpriteRenderer != null)
			{
				m_SpriteRenderer.sprite = _Sprite;
				return true;
			}

			return false;
		}

		/// <summary>
		/// Sets the SpriteRenderer or Image component reference depending on what component is on this gameObject.
		/// </summary>
		private void SetRenderer()
		{
			m_SpriteRenderer = GetComponent<SpriteRenderer>();
			if(m_SpriteRenderer == null)
			{
				m_ImageRenderer = GetComponent<Image>();
				m_UseImageRenderer = true;
			}
		}

		/// <summary>
		/// Get the SpriteAnimation at the given index from the SpriteAnimation array.
		/// </summary>
		public SpriteAnimation GetAnimation(int _Index)
		{
			int count = m_Animations.Length;
			if(count > 0)
			{
				if(_Index >= 0 && _Index < count)
				{
					return m_Animations[_Index];
				}
			}
			return null;
		}

		public int GetRandomAnimationIndex()
		{
			int count = m_UnplayedAnimationsIndexes.Count;
			if(count > 0)
			{
				return (count > 0) ? m_UnplayedAnimationsIndexes[Random.Range(0, count)] : -1;
			}
			else
			{
				if(m_Animations.Length > 0)
				{
					ResetUnplayedAnimations();
					return GetRandomAnimationIndex();
				}
			}

			return -1;
		}

		private int GetAnimationIndex(string _AnimationName)
		{
			int count = m_Animations.Length;
			for (int i = 0; i < count; i++)
			{
				if(m_Animations[i].name == _AnimationName)
				{
					return i;
				}
			}
			return -1;
		}

		public bool IsPlaying
		{
			get
			{
				return
				(
					m_IsPlaying &&
					m_PlayingAnimation != null &&
					(
						m_PlayingAnimation.PlayMode != SpriteAnimationPlayMode.Once ||
						m_AnimationTimer < m_AnimationDuration
					)
				);
			}
		}

		private float AnimationRatio
		{
			get { return (m_AnimationDuration > 0.0f) ? (m_AnimationTimer / m_AnimationDuration) : 0.0f; }
		}

	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}

}