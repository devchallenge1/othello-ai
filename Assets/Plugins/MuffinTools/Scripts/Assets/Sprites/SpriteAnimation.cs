﻿using UnityEngine;

namespace MuffinTools
{

///<summary>
/// 
/// 
/// 
///</summary>

public enum SpriteAnimationPlayMode
{
	Once,
	Loop,
	PingPong
}

[CreateAssetMenu(fileName = "NewSpriteAnimation", menuName = "MuffinTools/Sprite Animation", order = 750)]
public class SpriteAnimation : ScriptableObject
{

	#region Enums & Subclasses

	#endregion

	
	#region Attributes

		// Settings

		[Header("Settings")]

		[SerializeField]
		private Sprite[] m_Sprites = { };

		[SerializeField, Min(0.001f)]
		[Tooltip("Defines the interval between two sprites.")]
		private float m_Interval = 0.1f;

		[SerializeField]
		private SpriteAnimationPlayMode m_PlayMode = SpriteAnimationPlayMode.Loop;

	#endregion

	
	#region Accessors

		public Sprite[] Sprites
		{
			get { return m_Sprites; }
		}

		public float Interval
		{
			get { return m_Interval; }
		}

		public SpriteAnimationPlayMode PlayMode
		{
			get { return m_PlayMode; }
		}

	#endregion

}

}