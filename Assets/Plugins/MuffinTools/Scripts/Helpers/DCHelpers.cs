﻿using UnityEngine;

namespace MuffinTools
{

/// <summary>
/// 
///		This class contains some methods for the annoying things.
/// 
/// </summary>
public class DCHelpers
{

	#region Scene Loading

		/// <summary>
		/// Loads a level using Scene Management if you're on a recent version, or
		/// Application.LoadLevel() method for versions lower than 5.3.
		/// </summary>
		public static void LoadLevel(string _LevelName)
		{
			#if UNITY_5_3_OR_NEWER
			UnityEngine.SceneManagement.SceneManager.LoadScene(_LevelName);
			#else
			Application.LoadLevel(_LevelName);
			#endif
		}

		/// <summary>
		/// Loads a level using Scene Management if you're on a recent version, or
		/// Application.LoadLevel() method for versions lower than 5.3.
		/// </summary>
		public static void LoadLevel(int _LevelIdx)
		{
			#if UNITY_5_3_OR_NEWER
			UnityEngine.SceneManagement.SceneManager.LoadScene(_LevelIdx);
			#else
			Application.LoadLevel(_LevelIdx);
			#endif
		}

	#endregion


	#region Colors

		/// <summary>
		/// Convert the given color to an Hexadecimal string.
		/// </summary>
		public static string ColorToHex(Color32 _Color)
		{
			string hex = _Color.r.ToString("X2") + _Color.g.ToString("X2") + _Color.b.ToString("X2");

			return hex;
		}

		/// <summary>
		/// Convert the given string in a Color struct.
		/// </summary>
		public static Color HexToColor(string hex)
		{
			byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
			byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
			byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);

			return new Color32(r, g, b, 255);
		}

	#endregion

}

}