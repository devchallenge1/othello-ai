﻿using UnityEngine;

namespace MuffinTools
{

///<summary>
/// 
///</summary>
public static class MathHelpers
{

	#region Public Methods

		/// <summary>
		/// Apply a given Drag value on a given Axis value.
		/// The Drag will make the axis value increase by the drag value
		/// if it's less than 0, or increase by this same value if it's
		/// more than 0.
		/// But if that modified value is respectively more than 0 or
		/// less than 0, the value is set to 0.
		/// 
		/// It's really useful to simulate a Drag force on a velocity
		/// axis, for example.
		/// </summary>
		/// <returns>Returns the dragged value.</returns>
		public static float Drag(float _Axis, float _Drag)
		{
			// If the axis effectively needs to be dragged
			if(_Axis != 0.0f && _Drag != 0.0f)
			{
				/*
				 * If the axis is more than 0, we decrease from it the Drag value.
				 * But, it can now be less than 0, and that's why we have to clamp
				 * the resulting value.
				 * 
				 * It's the inverse process if it's less than 0.
				 */

				if(_Axis > 0.0f)
				{
					_Axis -= _Drag;

					if(_Axis < 0.0f)
					{
						_Axis = 0.0f;
					}
				}

				else
				{
					_Axis += _Drag;

					if (_Axis > 0.0f)
					{
						_Axis = 0.0f;
					}
				}
			}

			return _Axis;
		}

		/// <summary>
		/// Contains the given angle value between 0 and 360.
		/// 
		/// [90		=	90	]
		/// [450	=	270	]
		/// [-90	=	270	]
		/// [-450	=	90	]
		/// </summary>
		/// <returns>Returns the given angle value between 0 and 360.</returns>
		public static float Angle360(float _Angle)
		{
			int times = Mathf.FloorToInt(_Angle / 360.0f);

			if(times != 0)
			{
				_Angle -= (360.0f * times);
			}

			return _Angle;
		}

	#endregion

}

}