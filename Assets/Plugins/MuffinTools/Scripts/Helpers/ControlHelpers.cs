﻿using UnityEngine;

using System.Collections.Generic;

namespace MuffinTools
{

/// <summary>
/// 
///		This class contains a bunch of Helper methods, generally
///	used in a Controller.
/// 
/// </summary>

public static class Controller
{

	#region Attributes
	#endregion


	#region Public Methods

		/// <summary>
		/// Make a raycast from the mouse position to the world.
		/// </summary>
		/// <typeparam name="T">The type of the component you want to get.</typeparam>
		/// <returns>Returns the component of the given type on the first hit collider.</returns>
		public static T GetPointedElement<T>()
		{
			return GetPointedElement<T>(Input.mousePosition, Mathf.Infinity, Physics.DefaultRaycastLayers);
		}

		/// <summary>
		/// Make a raycast from the given screen position to the world.
		/// </summary>
		/// <typeparam name="T">The type of the component you want to get.</typeparam>
		/// <param name="_ScreenPosition">The screen position, start point of the ray.</param>
		/// <returns>Returns the component of the given type on the first hit collider.</returns>
		public static T GetPointedElement<T>(Vector3 _ScreenPosition)
		{
			return GetPointedElement<T>(_ScreenPosition, Mathf.Infinity, Physics.DefaultRaycastLayers);
		}

		/// <summary>
		/// Make a raycast from the given screen position to the world.
		/// </summary>
		/// <typeparam name="T">The type of the component you want to get.</typeparam>
		/// <param name="_ScreenPosition">The screen position, start point of the ray.</param>
		/// <param name="_MaxDistance">The maximum distance of the ray.</param>
		/// <returns>Returns the component of the given type on the first hit collider.</returns>
		public static T GetPointedElement<T>(Vector3 _ScreenPosition, float _MaxDistance)
		{
			return GetPointedElement<T>(_ScreenPosition, _MaxDistance, Physics.DefaultRaycastLayers);
		}
		
		/// <summary>
		/// Make a raycast from the given screen position to the world.
		/// </summary>
		/// <typeparam name="T">The type of the component you want to get.</typeparam>
		/// <param name="_ScreenPosition">The screen position, start point of the ray.</param>
		/// <param name="_MaxDistance">The maximum distance of the ray.</param>
		/// <param name="_LayerMask">The collision layers you want to collide/ignore.</param>
		/// <returns>Returns the component of the given type on the first hit collider.</returns>
		public static T GetPointedElement<T>(Vector3 _ScreenPosition, float _MaxDistance, int _LayerMask)
		{
			Ray			toWorld	= Camera.main.ScreenPointToRay(_ScreenPosition);
			RaycastHit	rayHit;

			if(Physics.Raycast(toWorld, out rayHit, _MaxDistance, _LayerMask))
			{
				return rayHit.collider.GetComponent<T>();
			}

			return default(T);
		}
		
		/// <summary>
		/// Make a raycast all from the mouse position to the world.
		/// </summary>
		/// <typeparam name="T">The type of the components you want to get.</typeparam>
		/// <returns>Returns all the first found components of the given type on all given hit objects.</returns>
		public static List<T> GetPointedElements<T>()
		{
			return GetPointedElements<T>(Input.mousePosition, Mathf.Infinity, Physics.DefaultRaycastLayers);
		}

		/// <summary>
		/// Make a raycast all from the given screen position to the world.
		/// </summary>
		/// <typeparam name="T">The type of the components you want to get.</typeparam>
		/// <param name="_ScreenPosition">The screen position, start point of the ray.</param>
		/// <returns>Returns all the first found components of the given type on all given hit objects.</returns>
		public static List<T> GetPointedElements<T>(Vector3 _ScreenPosition)
		{
			return GetPointedElements<T>(Input.mousePosition, Mathf.Infinity, Physics.DefaultRaycastLayers);
		}

		/// <summary>
		/// Make a raycast all from the given screen position to the world.
		/// </summary>
		/// <typeparam name="T">The type of the components you want to get.</typeparam>
		/// <param name="_ScreenPosition">The screen position, start point of the ray.</param>
		/// <param name="_MaxDistance">The maximum distance of the ray.</param>
		/// <returns>Returns all the first found components of the given type on all given hit objects.</returns>
		public static List<T> GetPointedElements<T>(Vector3 _ScreenPosition, float _MaxDistance)
		{
			return GetPointedElements<T>(Input.mousePosition, _MaxDistance, Physics.DefaultRaycastLayers);
		}

		/// <summary>
		/// Make a raycast all from the given screen position to the world.
		/// </summary>
		/// <typeparam name="T">The type of the components you want to get.</typeparam>
		/// <param name="_ScreenPosition">The screen position, start point of the ray.</param>
		/// <param name="_MaxDistance">The maximum distance of the ray.</param>
		/// <param name="_LayerMask">The collision layers you want to collide/ignore.</param>
		/// <returns>Returns all the first found components of the given type on all given hit objects.</returns>
		public static List<T> GetPointedElements<T>(Vector3 _ScreenPosition, float	_MaxDistance, int _LayerMask)
		{
			Ray				toWorld		= Camera.main.ScreenPointToRay(_ScreenPosition);
			RaycastHit[]	rayHits		= Physics.RaycastAll(toWorld, _MaxDistance, _LayerMask);

			return FindHitObjectsOfType<T>(rayHits);
		}

	#endregion


	#region Protected Methods
	#endregion


	#region Private Methods

		/// <summary>
		/// Find the first found component of the given type on all given hit objects.
		/// </summary>
		/// <typeparam name="T">The type of the component you want to get.</typeparam>
		/// <param name="_RaycastHits">The array of hits.</param>
		/// <returns>Returns all the first found components of the given type on all given hit objects.</returns>
		private static List<T> FindHitObjectsOfType<T>(RaycastHit[] _RaycastHits)
		{
			List<T> elements = new List<T>();

			for (int i = 0; i < _RaycastHits.Length; i++)
			{
				T comp = _RaycastHits[i].collider.GetComponent<T>();

				if (comp != null)
				{
					elements.Add(comp);
				}
			}

			return elements;
		}

	#endregion


	#region Actions
	#endregion


	#region Accessors
	#endregion

}

}