﻿using UnityEngine;

///<summary>
///
///		A GameAction class must implement the controller and the action logic.
///		The Action must be very simple to match the pattern.
///		An Action must be placed on a GameObject that has a parent with GameActionsManager component.
///	Then, it can have a reference to that GameActionsManager and access to all its features.
///	
///		Statements : you can use GameActionsManager's statements to transmit informations like
///	"isJumping", "isDashing", ... You have to check if these statement exist in the Action logic
///	manually, but this feature avoid you to have references to all other Actions of a Character, or
///	too much events to trigger and listen.
/// 
///</summary>

namespace MuffinTools
{

public abstract class GameAction : MonoBehaviour
{

	#region Attributes

        // References

        private GameActionsManager m_GameActionsManager = null;

	#endregion


	#region Public Methods

        ///<summary>
        /// Called after a GameActionsManager is assigned to this GameAction.
        ///</summary>
        public void GAInit()
        {

        }

        ///<summary>
        /// Called at the just-assigned GameActionsManager Awake().
        ///</summary>
        public virtual void GAAwake()
        {

        }

        ///<summary>
        /// Called at the just-assigned GameActionsManager Start().
        ///</summary>
        public virtual void GAStart()
        {

        }

        ///<summary>
        /// Called at the just-assigned GameActionsManager Update().
        ///</summary>
        public virtual void GAUpdate()
        {

        }

        ///<summary>
        /// Called at the just-assigned GameActionsManager FixedUpdate().
        ///</summary>
        public virtual void GAFixedUpdate()
        {

        }

        ///<summary>
        /// Called at the just-assigned GameActionsManager LateUpdate().
        ///</summary>
        public virtual void GALateUpdate()
        {

        }

        ///<summary>
        /// Called at the just-assigned GameActionsManager OnDestroy().
        ///</summary>
        public virtual void GADestroy()
        {

        }

        ///<summary>
        /// Assign a GameActionsManager to this Action if it's not already assigned.
        ///</summary>
        ///<returns>Returns true if the operation is successful, otherwise false.</returns>
        public bool AssignManager(GameActionsManager _Manager)
        {
            if(_Manager != null)
            {
                if(m_GameActionsManager == null)
                {
                    m_GameActionsManager = _Manager;
                }

                if(m_GameActionsManager == _Manager)
                {
                    return true;
                }
            }

            return false;
        }

	#endregion


	#region Protected Methods
	#endregion


	#region Private Methods
	#endregion


	#region Accessors

        public GameActionsManager GameActionsManager
        {
            get { return m_GameActionsManager; }
        }

	#endregion


	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}

}