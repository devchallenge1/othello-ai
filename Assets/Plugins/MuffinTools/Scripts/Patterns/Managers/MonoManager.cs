﻿using UnityEngine;

using System.Collections.Generic;

namespace MuffinTools
{

/// <summary>
/// 
/// 
/// 
/// </summary>
/// 
/// <typeparam name="T">The managed elements' type.</typeparam>

public abstract class MonoManager<T> : MonoBehaviour
{

	#region Attributes

		// Resources

		[Header("Manager resources")]

		[SerializeField]
		private	List<T>	m_Elements = new List<T>();

	#endregion


	#region Protected Methods

		/// <summary>
		/// Called when an element has successfully been added in the list.
		/// </summary>
		/// <param name="_Element">The just-added element.</param>
		protected virtual void OnAddElement(T _Element)
		{
			
		}

		/// <summary>
		/// Called just before an element will be destroyed.
		/// </summary>
		protected virtual void OnRemoveElement(T _Element)
		{
			
		}

	#endregion

	
	#region Accessors

		/// <summary>
		/// Add an element in the list.
		/// </summary>
		/// <param name="_Element">The element to add.</param>
		public virtual void Add(T _Element)
		{
			m_Elements.Add(_Element);

			OnAddElement(_Element);
		}

		/// <summary>
		/// Add an element in the list if it's not already in.
		/// </summary>
		/// <param name="_Element">The element to add.</param>
		public virtual void AddOnce(T _Element)
		{
			if (!m_Elements.Contains(_Element))
			{
				m_Elements.Add(_Element);

				OnAddElement(_Element);
			}
		}

		/// <summary>
		/// Remove an element from the list.
		/// </summary>
		/// <param name="_Element">The element to remove.</param>
		public virtual void Remove(T _Element)
		{
			OnRemoveElement(_Element);

			m_Elements.Remove(_Element);
		}

		public List<T> Elements
		{
			get { return m_Elements; }
		}

		public int Count
		{
			get { return m_Elements.Count; }
		}

		public T this [int _Idx]
		{
			get { return m_Elements[_Idx]; }
		}

	#endregion

}

}