﻿using UnityEngine;
using UnityEditor;

namespace MuffinTools
{

///<summary>
/// 
///</summary>
public class MuffinToolsPreferences : Editor
{

	#region Attributes

		// Constants

		private const string c_MTHomeFolderPrefsKey		= "MuffinTools_HomeFolder";

		private const string c_MTDefaultHomeFolder		= "Assets/Plugins/MuffinTools";
		private const string c_MTResourcesFolderPath	= "/Resources";

	#endregion


	#region Engine Methods

		[PreferenceItem("MuffinTools")]
		public static void OnPreferencesGUI()
		{
			DrawPreferences();
		}

	#endregion


	#region Public Methods
	#endregion


	#region Private Methods

		/// <summary>
		/// Draws preference fields in Unity's Preferences window.
		/// </summary>
		private static void DrawPreferences()
		{
			string	homeFolder	= EditorGUILayout.TextField("Home folder", HomeFolder);

			if(GUI.changed)
			{
				EditorPrefs.SetString(c_MTHomeFolderPrefsKey, homeFolder);
			}
		}

	#endregion


	#region Accessors

		/// <summary>
		/// Gets the Muffin Tools home folder.
		/// </summary>
		public static string HomeFolder
		{
			get
			{
				if(!EditorPrefs.HasKey(c_MTHomeFolderPrefsKey))
				{
					EditorPrefs.SetString(c_MTHomeFolderPrefsKey, c_MTDefaultHomeFolder);
				}
				return EditorPrefs.GetString(c_MTHomeFolderPrefsKey);
			}
		}

		public static string ResourcesFolder
		{
			get { return HomeFolder + c_MTResourcesFolderPath; }
		}

	#endregion

}

}