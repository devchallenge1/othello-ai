﻿using UnityEngine;
using UnityEditor;

namespace MuffinTools
{

/// <summary>
/// 
///		Use this class to make an Editor class with a pointer on the
///	typed target class instance. It avoids you to use the "target"
///	accessor and cast that variable each time you need it.
/// 
/// </summary>
/// 
/// <typeparam name="T">The type of the target.</typeparam>

public class TargetEditor<T> : Editor
	where T : Object
{

	#region Attributes

		private T m_Target = null;

	#endregion


	#region Initialisation

		protected virtual void OnEnable()
		{
			SetTarget();
		}

	#endregion


	#region Protected Methods
	#endregion


	#region Accessors

		/// <summary>
		/// Set target as the class' template type.
		/// </summary>
		private void SetTarget()
		{
			m_Target = target as T;
		}

		public T TypedTarget
		{
			get { return m_Target; }
		}

	#endregion

}

}