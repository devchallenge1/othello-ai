﻿using System;
using System.Reflection;
using System.Collections.Generic;

namespace MuffinTools
{

///<summary>
/// 
///		
/// 
///</summary>
public static class ReflectionHelpers
{

	#region Inheritance

		/// <summary>
		/// Checks if the given Type is an inheritor of one of the given parent Types.
		/// </summary>
		/// <param name="_ParentAsInheritor">If the given Type To Check is same as the Parent and
		/// this parameter is true, the given type will be recognized as an inheritor.</param>
		public static bool IsInheritorOf(Type _TypeToCheck, IEnumerable<Type> _ParentsList, bool _ParentAsInheritor = true)
		{
			foreach(Type parent in _ParentsList)
			{
				if(IsInheritorOf(_TypeToCheck, parent))
				{
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Checks if the given Type is an inheritor of the given parent Type.
		/// </summary>
		/// <param name="_ParentAsInheritor">If the given Type To Check is same as the Parent and
		/// this parameter is true, the given type will be recognized as an inheritor.</param>
		public static bool IsInheritorOf(Type _TypeToCheck, Type _Parent, bool _ParentAsInheritor = true)
		{
			// If the given Type is same as the given Parent
			if(_TypeToCheck == _Parent)
			{
				return _ParentAsInheritor;
			}

			Type lastCheckedParent = _TypeToCheck.BaseType;

			while (lastCheckedParent != null)
			{
				if (lastCheckedParent == _Parent)
				{
					return true;
				}

				lastCheckedParent = lastCheckedParent.BaseType;
			}

			return false;
		}

	#endregion


	#region Assemblies

		/// <summary>
		/// Gets the named Assembly in the entire solution.
		/// </summary>
		/// <returns>Returns the found assembly, or null if no assembly has the given name.</returns>
		public static Assembly GetAssemblyOfName(string _AssemblyName)
		{
			Assembly[] projectAssemblies = AppDomain.CurrentDomain.GetAssemblies();

			int count = projectAssemblies.Length;

			for (int i = 0; i < count; i++)
			{
				if (projectAssemblies[i].GetName().Name == _AssemblyName)
				{
					return projectAssemblies[i];
				}
			}

			return null;
		}

	#endregion

}

}