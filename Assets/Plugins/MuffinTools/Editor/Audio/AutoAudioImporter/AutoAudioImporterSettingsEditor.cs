﻿using UnityEngine;
using UnityEditor;

namespace MuffinTools
{

///<summary>
/// 
/// 
/// 
///</summary>
[CustomEditor(typeof(AutoAudioImporterSettings))]
public class AutoAudioImporterSettingsEditor : TargetEditor<AutoAudioImporterSettings>
{

	#region Attributes

		private const float c_MinSoundLength = 0.001f;

		private SerializedProperty m_MaxShortSoundLength = null;
		private SerializedProperty m_MaxMediumSoundLength = null;

	#endregion


	#region Engine Methods

		protected override void OnEnable ()
		{
			base.OnEnable();

			m_MaxShortSoundLength = serializedObject.FindProperty("m_MaxShortSoundLength");
			m_MaxMediumSoundLength = serializedObject.FindProperty("m_MaxMediumSoundLength");
		}

		public override void OnInspectorGUI()
		{
			EditorGUILayout.BeginVertical();
			{
				EditorGUILayout.LabelField("Warning", EditorStyles.boldLabel);

				EditorGUILayout.LabelField
				(
					"If you want this tool to work fine, take notice of some particular cases\n" +
					"All the platforms doesn't support all of the compression formats. So, supported formats for each platform are :\n" +
					"    - Standalone/PC/Mac/Linux\n" +
					"          - PCM\n" +
					"          - ADPCM\n" +
					"          - Vorbis\n" +
					"    - iOS & Android\n" +
					"          - PCM\n" +
					"          - ADPCM\n" +
					"          - Vorbis\n" +
					"          - MP3\n" +
					"    - WebGL\n" +
					"          - AAC",
					EditorStyles.helpBox
				);
			}
			EditorGUILayout.EndVertical();

			base.OnInspectorGUI();

			PostProcessProperties();
		}

	#endregion


	#region Private Methods

		private void PostProcessProperties()
		{
			float maxShortSoundLength = m_MaxShortSoundLength.floatValue;
			if(maxShortSoundLength < c_MinSoundLength)
			{
				maxShortSoundLength = c_MinSoundLength;
				m_MaxShortSoundLength.floatValue = maxShortSoundLength;
			}

			if(m_MaxMediumSoundLength.floatValue < (maxShortSoundLength + c_MinSoundLength))
			{
				m_MaxMediumSoundLength.floatValue = maxShortSoundLength + c_MinSoundLength;
			}

			serializedObject.ApplyModifiedPropertiesWithoutUndo();
		}

	#endregion

}

}