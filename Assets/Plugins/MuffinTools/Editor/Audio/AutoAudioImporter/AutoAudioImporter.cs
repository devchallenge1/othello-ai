﻿using UnityEngine;
using UnityEditor;

namespace MuffinTools
{

///<summary>
/// 
///		This postprocessor class will override AudioImporter settings for each AudioFile that will be (re)imported in the project.
/// 
///</summary>
public class AutoAudioImporter : AssetPostprocessor
{

	#region Subclasses & Enums

		public enum SoundSize
		{
			Short,
			ShortFrequent,
			Medium,
			MediumFrequent,
			Long
		}

	#endregion


	#region Attributes

		private const string c_SettingsAssetPath = "/Editor/Audio/AutoAudioImporter/AutoAudioImporterSettings.asset";

	#endregion


	#region Engine Methods

		/// <summary>
		/// Called when an audio file is (re)imported.
		/// </summary>
		private void OnPreprocessAudio()
		{
			AudioImporter importer = assetImporter as AudioImporter;

			if(importer != null)
			{
				// Load the AutoAudioImporterSettings
				AutoAudioImporterSettings settingsAsset = GetSettingsAsset();
				if(settingsAsset != null)
				{
					// Try to get the AudioClip object relative to the imported audio file.
					AudioClip clip = AssetDatabase.LoadAssetAtPath<AudioClip>(importer.assetPath);
					if(clip != null)
					{
						if (!settingsAsset.UpdateAudioImporterSettings(importer, GetSoundSize(clip, settingsAsset)))
						{
							Debug.LogWarning("ERROR from AutoAudioImporter.OnPreprocessAudio() : An error has occur during the AudioImporter update.");
						}
					}
					// If the AudioClip can't be found, it means that the audio file is imported for the first time.
					// So, let Unity postprocess the asset, then reimport it to call again this method, with an AudioClip.
					else
					{
						importer.SaveAndReimport();
					}
				}
			}
		}

	#endregion


	#region Accessors

		/// <summary>
		/// Gets the AutoAudioImporterSettings asset that must be in MuffinTools files.
		/// 
		/// Note : if the file is not at the expected path, you can change the constant value in this class' attributes region.
		/// If it doesn't exist in the project, you can create one by uncomment the "[CreateAssetMenu]" line in AutoAudioImporterSettings.cs.
		/// Then, go to Assets > Create > Muffin Tools > New Auto Audio Importer Settings, and place it at the same path as the value of the
		/// "SettingsAssetPath" constant.
		/// </summary>
		private AutoAudioImporterSettings GetSettingsAsset()
		{
			string path = MuffinToolsPreferences.HomeFolder + c_SettingsAssetPath;
			AutoAudioImporterSettings settingsAsset = AssetDatabase.LoadAssetAtPath<AutoAudioImporterSettings>(path);

			if(settingsAsset == null)
			{
				Debug.LogWarning("ERROR from AutoAudioImporter.GetSettingsAsset() : No settings asset at the specified path (" + path + ").");
			}

			return settingsAsset;
		}

		private SoundSize GetSoundSize(AudioClip _Clip, AutoAudioImporterSettings _Settings)
		{
			float size = _Clip.length;
			if(size < _Settings.MaxShortSoundSize)
			{
				return (_Settings.IsFrequentSound(_Clip.name)) ? SoundSize.ShortFrequent : SoundSize.Short;
			}
			else if(size < _Settings.MaxMediumSoundSize)
			{
				return (_Settings.IsFrequentSound(_Clip.name)) ? SoundSize.MediumFrequent : SoundSize.Medium;
			}
			else
			{
				return SoundSize.Long;
			}
		}

	#endregion

}

}