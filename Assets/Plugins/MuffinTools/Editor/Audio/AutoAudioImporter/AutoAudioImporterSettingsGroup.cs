﻿using UnityEngine;
using UnityEditor;

namespace MuffinTools
{

///<summary>
///	
///		Used by AutoAudioImporterSettings.
///		Defines the AudioImporter settings for a specified kind of sound.
/// 
///</summary>
[System.Serializable]
public struct AutoAudioImporterSettingsGroup
{

	#region Attributes

		[SerializeField]
		private string m_GroupName;

		[SerializeField]
		private AutoAudioImporter.SoundSize m_SoundSize;

		[SerializeField]
		private bool m_PreloadAudioData;

		[SerializeField]
		private bool m_LoadInBackground;

		[SerializeField]
		private bool m_ForceToMono;

		[SerializeField]
		private AutoAudioImporterPlatformSettings[] m_PlatformSettings;

	#endregion


	#region Public methods

		/// <summary>
		/// Updates the settings of the given AudioImporter.
		/// </summary>
		public bool UpdateAudioImporterSettings(AudioImporter _Importer)
		{
			// Update the "global" AudioImporter settings
			_Importer.preloadAudioData	= m_PreloadAudioData;
			_Importer.loadInBackground	= m_LoadInBackground;
			_Importer.forceToMono		= m_ForceToMono;

			bool success = true;
			int count = m_PlatformSettings.Length;
			for(var i = 0; i < count; i++)
			{
				// Update the AudioImporter settings for each platform
				if(!m_PlatformSettings[i].UpdateAudioImporterSettings(_Importer))
				{
					success = false;
				}
			}

			return success;
		}

	#endregion


	#region Accessors

		public AutoAudioImporter.SoundSize SoundSize
		{
			get { return m_SoundSize; }
		}

		private string GroupName
		{
			get { return m_GroupName; }
		}

	#endregion

}

}