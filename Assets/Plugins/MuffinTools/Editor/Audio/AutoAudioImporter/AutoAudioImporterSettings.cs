﻿using UnityEngine;
using UnityEditor;

namespace MuffinTools
{

///<summary>
/// 
///</summary>
//[CreateAssetMenu(fileName = "AutoAudioImporterSettings", menuName = "Muffin Tools/New Auto Audio Importer Settings", order = 750)]
public class AutoAudioImporterSettings : ScriptableObject
{

	#region Subclasses & enums
	#endregion


	#region Attributes

		[Header("Sounds Definition")]

		[SerializeField, Min(0.0f)]
		[Tooltip("Defined the length (in seconds) that an audio clip must have before it's considered as a \"medium\" sound.")]
		private float m_MaxShortSoundLength = 3.0f;

		[SerializeField, Min(0.0f)]
		[Tooltip("Defined the length (in seconds) that an audio clip must have before it's considered as a \"long\" sound.")]
		private float m_MaxMediumSoundLength = 11.0f;

		[SerializeField]
		private string[] m_FrequentSoundNames = { "Footstep", "Jump" };

		[Header("Auto Audio Importer Settings")]

		[SerializeField]
		private AutoAudioImporterSettingsGroup[] m_Settings = { };

	#endregion


	#region Public methods

		/// <summary>
		/// Updates the settings of the given AudioImporter.
		/// </summary>
		public bool UpdateAudioImporterSettings(AudioImporter _Importer, AutoAudioImporter.SoundSize _SoundSize)
		{
			int index = GetSettingsGroupForSoundSize(_SoundSize);
			if(index != -1)
			{
				return m_Settings[index].UpdateAudioImporterSettings(_Importer);
			}
			return false;
		}

		/// <summary>
		/// Returns true if the given clip name contains one of the "frequent sound names".
		/// </summary>
		public bool IsFrequentSound(string _ClipName)
		{
			int count = m_FrequentSoundNames.Length;
			for(int i = 0; i < count; i++)
			{
				if(_ClipName.Contains(m_FrequentSoundNames[i]))
				{
					return true;
				}
			}
			return false;
		}

	#endregion


	#region Private methods

		/// <summary>
		/// Returns the index of the settings group that matches with the given sound size.
		/// </summary>
		private int GetSettingsGroupForSoundSize(AutoAudioImporter.SoundSize _SoundSize)
		{
			int count = m_Settings.Length;
			for(int i = 0; i < count; i++)
			{
				if(m_Settings[i].SoundSize == _SoundSize)
				{
					return i;
				}
			}
			return -1;
		}

	#endregion


	#region Accessors

		public float MaxShortSoundSize
		{
			get { return m_MaxShortSoundLength; }
		}

		public float MaxMediumSoundSize
		{
			get { return m_MaxMediumSoundLength; }
		}

	#endregion

}

}