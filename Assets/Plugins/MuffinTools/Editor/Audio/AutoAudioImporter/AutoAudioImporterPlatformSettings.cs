﻿using UnityEngine;
using UnityEditor;

namespace MuffinTools
{

///<summary>
/// 
///		Used by AutoAudioImporterSettingsGroup.
///		Defines the AudioImporter settings for a specified platform.
/// 
///</summary>
[System.Serializable]
public struct AutoAudioImporterPlatformSettings
{

	#region Subclasses & Enums

		private enum TargetPlatform
		{
			StandaloneMacLinuxPC,
			Android,
			iOS,
			WebGL
		}
	
	#endregion


	#region Attributes

		[SerializeField]
		private TargetPlatform m_Platform;

		[SerializeField]
		private AudioClipLoadType m_LoadType;

		[SerializeField]
		private AudioCompressionFormat m_CompressionFormat;

		[SerializeField, Range(1, 100)]
		[Tooltip("This setting will be used only if the compression format is set to Vorbis.")]
		private int m_SoundQuality;

	#endregion

	
	#region Initialisation / Destruction
	#endregion

	
	#region Public Methods

		/// <summary>
		/// Updates the AudioImporterSampleSettings of the given AudioImporter for the defined Platform.
		/// </summary>
		public bool UpdateAudioImporterSettings(AudioImporter _Importer)
		{
			AudioImporterSampleSettings sampleSettings = _Importer.GetOverrideSampleSettings(Platform);

			sampleSettings.loadType				= m_LoadType;
			sampleSettings.compressionFormat	= m_CompressionFormat;

			// Set the compression quality if the selected compression format is Vorbis.
			if(m_CompressionFormat == AudioCompressionFormat.Vorbis)
			{
				sampleSettings.quality = m_SoundQuality / 100.0f;
			}

			return _Importer.SetOverrideSampleSettings(Platform, sampleSettings);
		}

	#endregion

	
	#region Private Methods
	#endregion

	
	#region Accessors

		private AudioClipLoadType LoadType
		{
			get { return m_LoadType; }
		}

		/// <summary>
		/// Get the platform name relative to the selected Target Platform.
		/// </summary>
		private string Platform
		{
			get
			{
				string platformName = "Standalone";
				switch(m_Platform)
				{
					case TargetPlatform.Android:
						platformName = "Android";
						break;
					case TargetPlatform.iOS:
						platformName = "iOS";
						break;
					case TargetPlatform.WebGL:
						platformName = "WebGL";
						break;

					default:
						break;
				}
				return platformName;
			}
		}

	#endregion

}

}