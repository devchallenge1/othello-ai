﻿#region Headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

	using UnityEngine;
	//using UnityEngine.UI;
	
	//using MuffinTools;
	//using MovementEffects;

#endregion


///<summary>
/// 
///		This class represents the implementation of a Player.
///		The AIs MUST inherit of this class.
/// 
///</summary>

public abstract class PlayerAsset : ScriptableObject, IPlayer
{

	#region Attributes
		
		// Settings

		[Header("Player Settings")]

		[SerializeField]
		private string m_PlayerName = string.Empty;

		[SerializeField]
		private PlayerTurn m_Turn = null;

	#endregion

	
	#region Public Methods

		/// <summary>
		/// Called when this Player's turn begins.
		/// Calls OnBeginTurn().
		/// </summary>
		public void BeginTurn(PlayerTurn _Turn)
		{
			m_Turn = _Turn;
			OnBeginTurn();
		}

		/// <summary>
		/// Called when this Player's turn ends (just-after it placed a pawn on the board).
		/// </summary>
		public void EndTurn()
		{
			if(m_Turn != null && IsTurnActive)
			{
				Debug.LogError("A Player (" + PlayerName + ") attempted to end its turn manually. You must use AppendPawn() to do a move, and the game will automatically end your turn. Or you can also call Surrender() if you're a COWARD...");
			}

			BoardState state = m_Turn.GetBoardStateCopy();
			m_Turn = null;
			OnEndTurn(state);
		}

		/// <summary>
		/// Ends the game... making the current player lose.
		/// </summary>
		public void Surrender()
		{
			if(m_Turn != null && IsTurnActive)
			{
				Debug.LogWarning("The player \"" + PlayerName + "\" attempted to surrender while it's not his turn.");
				return;
			}

			Debug.Log("TO DO : PlayerAsset.Surrender()");

			OnSurrender();
		}

		/// <summary>
		/// Called each frame during this PlayerAsset's turn.
		/// </summary>
		public virtual void OnUpdate(float _DeltaTime)
		{
			
		}

	#endregion

	
	#region Protected Methods

		/// <summary>
		/// Add a new pawn on the board at the given position.
		/// </summary>
		protected bool AppendPawn(Coords _Coords)
		{
			if (m_Turn != null)
			{
				if (m_Turn.AppendPawn(_Coords))
				{
					OnAppendPawnSuccess(_Coords);
					return true;
				}

				else
				{
					OnAppendPawnFailure(_Coords);
				}
			}
			else
			{
				Debug.LogError("No active PlayerTurn for this Player (" + PlayerName + ").");
			}

			return false;
		}

		/// <summary>
		/// Add a new pawn on the board at the given position.
		/// </summary>
		protected bool AppendPawn(int _X, int _Y)
		{
			return AppendPawn(new Coords(_X, _Y));
		}

		/// <summary>
		/// Called when this Player's turn begins.
		/// </summary>
		protected abstract void OnBeginTurn();

		/// <summary>
		/// Called just-after this Player's turn ends.
		/// </summary>
		/// <param name="_BoardState">The current board state after this Player had placed a pawn on it.
		/// </param>
		protected virtual void OnEndTurn(BoardState _BoardState) { }

		/// <summary>
		/// Called just-after this Player surrenders.
		/// </summary>
		protected virtual void OnSurrender() { }

		/// <summary>
		/// Called just-after this Player added a new pawn on the board successfully at the given position.
		/// </summary>
		protected virtual void OnAppendPawnSuccess(Coords _Position) { }

		/// <summary>
		/// Called just-after this Player attempted to add a new pawn on the board, but it failed.
		/// </summary>
		protected virtual void OnAppendPawnFailure(Coords _Position) { }

	#endregion

	
	#region Accessors

		public bool IsTurnActive
		{
			get { return (m_Turn != null) ? m_Turn.IsTurnActive : false; }
		}

		public string PlayerName
		{
			get { return (!string.IsNullOrEmpty(m_PlayerName)) ? m_PlayerName : name; }
			set { if(!string.IsNullOrEmpty(value)) { m_PlayerName = value; } }
		}

		protected BoardState GetBoardStateCopy()
		{
			return (m_Turn != null) ? m_Turn.GetBoardStateCopy() : null;
		}

		protected EPlayerSide PlayerSide
		{
			get { return m_Turn.PlayerSide; }
		}

	#endregion

}