﻿///<summary>
/// 
///		Used for the Gameplay_BeginTurn event.
/// 
///</summary>
public class WinnerInfos
{

	private string		m_PlayerName;
	private EPlayerSide m_PlayerSide;
	private int			m_PawnsCount;

	public WinnerInfos(string _Name, EPlayerSide _Side, int _PawnsCount)
	{
		m_PlayerName = _Name;
		m_PlayerSide = _Side;
		m_PawnsCount = _PawnsCount;
	}

	public string		PlayerName	{ get { return m_PlayerName; } }
	public EPlayerSide	PlayerSide	{ get { return m_PlayerSide; } }
	public int			PawnsCount	{ get { return m_PawnsCount; } }

}