﻿#region Headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;
	
	//using UnityEngine;
	//using UnityEngine.UI;

	//using MuffinTools;
	//using MovementEffects;

	//[System.Serializable]

#endregion


///<summary>
/// 
///		Represents a Player on the board, in the scene.
/// 
///</summary>
public class BoardPlayer : IPlayer
{

	#region Attributes

		// References

		// The instance of the Player render in the scene
		private Player m_Player = null;

		// Settings

		private EPlayerSide m_PlayerSide = EPlayerSide.Black;

	#endregion

	
	#region Initialization / Destruction

		public BoardPlayer(Player _Player, EPlayerSide _PlayerSide)
		{
			m_Player = _Player;
			m_PlayerSide = _PlayerSide;
		}

	#endregion

	
	#region Public Methods

		public void BeginTurn(PlayerTurn _PlayerTurn)
		{
			m_Player.BeginTurn(_PlayerTurn);
		}

		public void EndTurn()
		{
			m_Player.EndTurn();
		}

		public void Surrender()
		{
			m_Player.Surrender();
		}

	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods
	#endregion

	
	#region Accessors

		public EPlayerSide PlayerSide
		{
			get { return m_PlayerSide; }
		}

		public string PlayerName
		{
			get { return m_Player.PlayerName; }
		}

	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}