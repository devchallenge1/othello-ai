﻿#region Headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

	using UnityEngine;
	//using UnityEngine.UI;

	//using MuffinTools;
	//using MovementEffects;

	//[AddComponentMenu("Scripts/Player")]
	//[RequireComponent(typeof())]

#endregion


///<summary>
/// 
///</summary>
public class Player : MonoBehaviour
{

	#region Attributes

		// References

		private PlayerAsset m_PlayerAsset = null;

	#endregion

	
	#region Engine Methods
	#endregion

	
	#region Public Methods

		public void BeginTurn(PlayerTurn _PlayerTurn)
		{
			m_PlayerAsset.BeginTurn(_PlayerTurn);
		}

		public void EndTurn()
		{
			m_PlayerAsset.EndTurn();
		}

		public void Surrender()
		{
			m_PlayerAsset.Surrender();
		}

		public void Update()
		{
			if(m_PlayerAsset.IsTurnActive)
			{
				m_PlayerAsset.OnUpdate(Time.deltaTime);
			}
		}

	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods
	#endregion

	
	#region Accessors

		public string PlayerName
		{
			get { return m_PlayerAsset.PlayerName; }
		}

		/// <summary>
		/// Sets the PlayerAsset if it's not already initialized, or log an error.
		/// </summary>
		public bool SetPlayerAsset(PlayerAsset _PlayerAsset)
		{
			if(m_PlayerAsset == null)
			{
				m_PlayerAsset = Object.Instantiate<PlayerAsset>(_PlayerAsset);
				return true;
			}

			else
			{
				Debug.LogError("The PlayerAsset of this Player (" + name + ") is already assign. You wanted to assign the Player Asset \"" + m_PlayerAsset + "\" (I think the game owned you...)");
			}

			return false;
		}

	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}