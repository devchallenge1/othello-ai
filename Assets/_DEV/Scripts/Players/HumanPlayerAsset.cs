﻿#region Headers

	using System.Collections.Generic;

	using UnityEngine;

#endregion


///<summary>
/// 
///</summary>

[CreateAssetMenu(fileName = "NewHumanPlayerAsset", menuName = "Othello - Player/Human", order = 750)]
public class HumanPlayerAsset : PlayerAsset
{

	#region Attributes

		// Settings

		[Header("Settings")]

		[SerializeField]
		private bool m_ActivateDebug = false;

	#endregion

	
	#region Engine Methods
	#endregion

	
	#region Public Methods

		public override void OnUpdate(float _DeltaTime)
		{
			CheckControl();
		}

	#endregion

	
	#region Protected Methods

		protected override void OnBeginTurn()
		{
			if(m_ActivateDebug)
			{
				BoardState boardState = GetBoardStateCopy();
				List<BoardMove> possibleMoves = boardState.FindPossibleMoves(PlayerSide);
				OthelloHelpers.DebugBoardMovesList(possibleMoves);
			}
		}

	#endregion

	
	#region Private Methods

		private void CheckControl()
		{
			if(Input.GetMouseButtonDown(0))
			{
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit rayHit;

				if(Physics.Raycast(ray, out rayHit))
				{
					GameObject obj = rayHit.collider.gameObject;
					Cell cell = obj.GetComponent<Cell>();

					if(cell != null)
					{
						AppendPawn(cell.Coords);
					}
				}
			}
		}

	#endregion

	
	#region Accessors
	#endregion

}