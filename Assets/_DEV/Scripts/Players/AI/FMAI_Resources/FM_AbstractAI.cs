﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FM_AbstractAI {
    protected EPlayerSide playerSide;
    protected EPlayerSide oppositeSide;
    protected int treeDepth = 3;

    protected int turnCount = 0;
    protected int findPossibleMoveCount = 0;
    protected int appendPawnCount = 0;
    protected int countPawnCount = 0;
    protected int nodeIntanceCount = 0;
    protected int hasPossibleMoveCount = 0;

    protected TimeSpan tsFindPossibleMove = TimeSpan.Zero;
    protected TimeSpan tsLoopAppendPawn = TimeSpan.Zero;
    protected TimeSpan tsMain = TimeSpan.Zero;
    protected TimeSpan tsBuildTree = TimeSpan.Zero;
    protected TimeSpan tsInit = TimeSpan.Zero;
    protected TimeSpan tsGetMove = TimeSpan.Zero;
    protected TimeSpan tsMainAppendPawn = TimeSpan.Zero;
    protected TimeSpan tsClearAI = TimeSpan.Zero;
    protected TimeSpan tsComputeScore = TimeSpan.Zero;
    protected TimeSpan tsNodeInstantiate = TimeSpan.Zero;
    protected TimeSpan tsHasPossibleMove = TimeSpan.Zero;

    public FM_AbstractAI(EPlayerSide _playerSide, int _treeDepth)
    {
        playerSide = _playerSide;
        oppositeSide = ComputeOppositePlayer(playerSide);
        treeDepth = _treeDepth;
        Init();
    }

    private void Init()
    {
        findPossibleMoveCount = 0;
        appendPawnCount = 0;
        countPawnCount = 0;
        nodeIntanceCount = 0;
        hasPossibleMoveCount = 0;

        tsFindPossibleMove = TimeSpan.Zero;
        tsLoopAppendPawn = TimeSpan.Zero;
        tsMain = TimeSpan.Zero;
        tsInit = TimeSpan.Zero;
        tsBuildTree = TimeSpan.Zero;
        tsGetMove = TimeSpan.Zero;
        tsMainAppendPawn = TimeSpan.Zero;
        tsClearAI = TimeSpan.Zero;
        tsComputeScore = TimeSpan.Zero;
        tsNodeInstantiate = TimeSpan.Zero;
        tsHasPossibleMove = TimeSpan.Zero;
    }

    public EPlayerSide PlayerSide
    {
        get { return playerSide; }
    }

    public EPlayerSide OppositeSide
    {
        get { return oppositeSide; }
    }

    public abstract Coords GetMove(BoardState _boardState);


    protected EPlayerSide ComputeOppositePlayer(EPlayerSide _playerColor)
    {
        EPlayerSide oppositeColor = EPlayerSide.White;
        switch (_playerColor)
        {
            case EPlayerSide.Black:
                oppositeColor = EPlayerSide.White;
                break;
            case EPlayerSide.White:
                oppositeColor = EPlayerSide.Black;
                break;
        }
        return oppositeColor;
    }
}
