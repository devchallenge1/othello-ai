﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FM_MinimalAlphaBeta : FM_AbstractAI
{
    private int[,] weightingGrid;
    private int gridWidth = 0;
    private int gridHeight = 0;
    private bool initDone = false;

    private int maxWeigthValue = 100;
    private int normalWeigthValue = 1;
    private int minWeigthValue = -50;

    public FM_MinimalAlphaBeta(EPlayerSide _playerSide, int _treeDepth) : base(_playerSide, _treeDepth)
    {
    }

    public override Coords GetMove(BoardState _boardState)
    {
        Debug.Log("AI Minimal - GetMove");
        gridHeight = _boardState.Height;
        gridWidth = _boardState.Width;
        Init();
        Coords move = ComputeAlphaBetaTree(_boardState);

        Debug.Log("AI Minimal - GetMove - Return Move : "+move.ToString());
        return move;
    }

    private Coords ComputeAlphaBetaTree(BoardState _theBoard)
    {
        int val = int.MaxValue;
        Coords move = Coords.upLeft;

        List<BoardMove> possibleMove = _theBoard.FindPossibleMoves(PlayerSide);
        if (possibleMove.Count == 1)
        {
            move = possibleMove[0].startPosition;
        }
        else
        {
            for (int i = 0; i < possibleMove.Count; i++)
            {
                Coords movePossible = possibleMove[i].startPosition;
                BoardState boardStateCopy = ScriptableObject.Instantiate<BoardState>(_theBoard);
                boardStateCopy.AppendPawn(movePossible.x, movePossible.y, PlayerSide);
                Debug.Log("ComputeAlphaBetaTree - Tree Depth : " + treeDepth);
                int resultat = ComputeBetaTree(boardStateCopy, treeDepth - 1, int.MinValue, int.MaxValue);
                //GC.Collect();
                if (resultat < val)
                {
                    val = resultat;

                    move = possibleMove[i].startPosition;
                }
            }
        }
        return move;
    }

    //Coup de l'ai ==> le maximum de point
    private int ComputeAlphaTree(BoardState _theBoard, int _depth, int _alpha, int _beta)
    {
        Debug.Log("ComputeAlphaTree - param Depth : " + _depth);
        int alpha = int.MinValue;
        bool canPlayerMove = _theBoard.HasPossibleMoves(PlayerSide);
        if (canPlayerMove)
        {
            List<BoardMove> possibleMove = _theBoard.FindPossibleMoves(PlayerSide);
            for (int i = 0; i < possibleMove.Count; i++)
            {
                Coords movePossible = possibleMove[i].startPosition;
                BoardState boardStateCopy = ScriptableObject.Instantiate<BoardState>(_theBoard);
                boardStateCopy.AppendPawn(movePossible.x, movePossible.y, PlayerSide);

                bool canPlayerMoveNextTurn = boardStateCopy.HasPossibleMoves(PlayerSide);
                bool canOppositeMoveNextTurn = boardStateCopy.HasPossibleMoves(OppositeSide);
                bool isEndNextTurn = (canPlayerMoveNextTurn == false && canOppositeMoveNextTurn == false);
                if (isEndNextTurn || ((_depth - 1) < 0))
                {
                    int totalPlayer = boardStateCopy.CountPawns(PlayerSide);
                    int totalOpposite = boardStateCopy.CountPawns(OppositeSide);
                    int oppositeScore = totalOpposite - _theBoard.CountPawns(OppositeSide);
                    int playerScore = totalPlayer - _theBoard.CountPawns(PlayerSide);

                    alpha = EvaluateMove(movePossible, playerScore, oppositeScore, totalPlayer, totalOpposite, true, isEndNextTurn);
                }
                else
                {
                    int resultat = ComputeBetaTree(boardStateCopy, _depth - 1, Math.Max(_alpha, alpha), _beta);
                    //GC.Collect();
                    alpha = Math.Max(alpha, resultat);
                    if (alpha >= _beta)
                    {
                        return alpha;
                    }
                }
            }
        }
        else
        {
            Debug.Log("ComputeAlphaTree - Player cannot move - param Depth : " + _depth);
            int resultat = ComputeBetaTree(_theBoard, _depth - 1, Math.Max(_alpha, alpha), _beta);
            alpha = Math.Max(alpha, resultat);
            if (alpha >= _beta)
            {
                return alpha;
            }

        }

        return alpha;

        //bool canOppositeMove = _theBoard.HasPossibleMoves(OppositeSide);

        //bool isEnd = (canPlayerMove == false && canOppositeMove == false);
        //if (isEnd || _depth < 0)
        //{
        //    int totalPlayer = boardStateCopy.CountPawns(_playerSide);
        //    int totalOpposite = boardStateCopy.CountPawns(oppositePlayer);
        //    int oppositeScore = totalOpposite - _theBoard.CountPawns(oppositePlayer);
        //    int playerScore = totalPlayer - _theBoard.CountPawns(_playerSide);
        //    alpha = EvaluateMove(_move, playerScore, oppositeScore, totalPlayer, totalOpposite, _playerSide == PlayerSide, isEnd);
        //}
        //else
        //{
        //    List<BoardMove> possibleMove = boardStateCopy.FindPossibleMoves(oppositePlayer);
        //    for (int i = 0; i < possibleMove.Count; i++)
        //    {
        //        int resultat = ComputeBetaTree(boardStateCopy, possibleMove[i].startPosition, _depth - 1, oppositePlayer, Math.Max(_alpha, alpha), _beta);
        //        alpha = Math.Max(alpha, resultat);
        //        if (alpha >= _beta)
        //        {
        //            return alpha;
        //        }
        //    }
        //}
        //return alpha;
    }

    // Coup de l'adversaire ==> minimum de point
    private int ComputeBetaTree(BoardState _theBoard, int _depth, int _alpha, int _beta)
    {
        Debug.Log("ComputeBetaTree - param Depth : " + _depth);
        int beta = int.MaxValue;

        bool canOppositeMove = _theBoard.HasPossibleMoves(OppositeSide);
        if (canOppositeMove) {
            List<BoardMove> possibleMove = _theBoard.FindPossibleMoves(OppositeSide);
            for (int i = 0; i < possibleMove.Count; i++)
            {
                Coords movePossible = possibleMove[i].startPosition;
                BoardState boardStateCopy = ScriptableObject.Instantiate<BoardState>(_theBoard);
                boardStateCopy.AppendPawn(movePossible.x, movePossible.y, OppositeSide);


                bool canPlayerMoveNextTurn = boardStateCopy.HasPossibleMoves(PlayerSide);
                bool canOppositeMoveNextTurn = boardStateCopy.HasPossibleMoves(OppositeSide);
                bool isEndNextTurn = (canPlayerMoveNextTurn == false && canOppositeMoveNextTurn == false);
                if (isEndNextTurn || ((_depth - 1) < 0))
                {
                    int totalPlayer = boardStateCopy.CountPawns(PlayerSide);
                    int totalOpposite = boardStateCopy.CountPawns(OppositeSide);
                    int oppositeScore = totalOpposite - _theBoard.CountPawns(OppositeSide);
                    int playerScore = totalPlayer - _theBoard.CountPawns(PlayerSide);

                    beta = EvaluateMove(movePossible, playerScore, oppositeScore, totalPlayer, totalOpposite, false, isEndNextTurn);
                }
                else {
                    int resultat = ComputeAlphaTree(boardStateCopy, _depth - 1, _alpha, Math.Min(_beta, beta));
                    //GC.Collect();
                    beta = Math.Min(beta, resultat);
                    if (beta < _alpha)
                    {
                        return beta;
                    }
                }
            }
        }
        else
        {
            Debug.Log("ComputeAlphaTree - Opposite cannot move - param Depth : " + _depth);
            int resultat = ComputeAlphaTree(_theBoard, _depth - 1, _alpha, Math.Min(_beta, beta));
            beta = Math.Min(beta, resultat);
            if (beta < _alpha)
            {
                return beta;
            }
        }
        return beta;

        //bool canPlayerMove = _theBoard.HasPossibleMoves(PlayerSide);
        //bool canOppositeMove = _theBoard.HasPossibleMoves(OppositeSide);

        //bool isEnd = (canPlayerMove == false && canOppositeMove == false);
        //if (isEnd || _depth < 0 || !canOppositeMove)
        //{
        //    int totalPlayer = boardStateCopy.CountPawns(_playerSide);
        //    int totalOpposite = boardStateCopy.CountPawns(oppositePlayer);
        //    int oppositeScore = totalOpposite - _theBoard.CountPawns(oppositePlayer);
        //    int playerScore = totalPlayer - _theBoard.CountPawns(_playerSide);

        //    beta = EvaluateMove(_move, playerScore, oppositeScore, totalPlayer, totalOpposite, _playerSide == PlayerSide, isEnd);
        //}
        //else
        //{
        //    if (canPlayerMove)
        //    {
        //        List<BoardMove> possibleMove = boardStateCopy.FindPossibleMoves(oppositePlayer);
        //        for (int i = 0; i < possibleMove.Count; i++)
        //        {
        //            int resultat = ComputeAlphaTree(boardStateCopy, possibleMove[i].startPosition, _depth - 1, oppositePlayer, _alpha, Math.Min(_beta, beta));
        //            beta = Math.Min(beta, resultat);
        //            if (beta < _alpha)
        //            {
        //                return beta;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        int resultat = ComputeAlphaTree(boardStateCopy, Coords.upLeft, _depth, oppositePlayer, _alpha, Math.Min(_beta, beta));
        //        beta = Math.Min(beta, resultat);
        //        if (beta < _alpha)
        //        {
        //            return beta;
        //        }
        //    }

        //}
        //return beta;
    }



    private int EvaluateMove(Coords _move, int _playerScore, int _oppositeScore, int _totalPlayerPawn, int _totalOppositePawn, bool isMax, bool _end)
    {
        //Debug.Log("EvalAlphaBEta - Start");
        int score = 0;
        int weight = -1;
        if (_move.x >= 0 && _move.x < gridHeight && _move.y >= 0 && _move.y < gridWidth)
        {
            weight = weightingGrid[_move.x, _move.y];

        }
        int multiplicator = -1;
        if (isMax)
        {
            multiplicator = 1;
        }
        if (_end)
        {
            if (_totalPlayerPawn > _totalOppositePawn)
            {
                score = 10000 * multiplicator;
            }
            else if (_totalPlayerPawn < _totalOppositePawn)
            {
                score = -10000 * multiplicator;
            }
            else
            {
                score = 0;
            }
        }
        else {
            if (weight == minWeigthValue)
            {
                score = (-1000 - (_totalPlayerPawn - _totalOppositePawn)) * multiplicator;
            }
            else if (weight == maxWeigthValue)
            {
                score = (1000 + (_totalPlayerPawn - _totalOppositePawn)) * multiplicator;
            }
            else
            {
                score = ((_playerScore - _oppositeScore) * (_totalPlayerPawn - _totalOppositePawn)) * multiplicator;
            }
        }
        return score;
    }


    private void Init()
    {
        if (!initDone)
        {
            initDone = true;
            InitWeightingGrid();
        }
    }
    private void InitWeightingGrid()
    {
        weightingGrid = new int[gridHeight, gridWidth];
        for (int i = 0; i < gridHeight; i++)
        {
            for (int j = 0; j < gridWidth; j++)
            {
                weightingGrid[i, j] = normalWeigthValue;
            }
        }
        weightingGrid[0, 0] = maxWeigthValue;
        weightingGrid[gridHeight - 1, 0] = maxWeigthValue;
        weightingGrid[0, gridWidth - 1] = maxWeigthValue;
        weightingGrid[gridHeight - 1, gridWidth - 1] = maxWeigthValue;

        weightingGrid[0, 1] = minWeigthValue;
        weightingGrid[1, 1] = minWeigthValue;
        weightingGrid[1, 0] = minWeigthValue;

        weightingGrid[gridHeight - 2, 0] = minWeigthValue;
        weightingGrid[gridHeight - 2, 1] = minWeigthValue;
        weightingGrid[gridHeight - 1, 1] = minWeigthValue;

        weightingGrid[gridHeight - 2, gridWidth - 1] = minWeigthValue;
        weightingGrid[gridHeight - 2, gridWidth - 2] = minWeigthValue;
        weightingGrid[gridHeight - 1, gridWidth - 2] = minWeigthValue;

        weightingGrid[0, gridWidth - 2] = minWeigthValue;
        weightingGrid[1, gridWidth - 2] = minWeigthValue;
        weightingGrid[1, gridWidth - 1] = minWeigthValue;
    }

}
