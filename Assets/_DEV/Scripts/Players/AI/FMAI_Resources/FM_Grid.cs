﻿using System.Collections;
using System.Collections.Generic;

public class FM_Grid {
    private CellState[,] theGrid;
    private int width = 0;
    private int height = 0;

    public FM_Grid(BoardState _boardState)
    {
        height = _boardState.Height;
        width = _boardState.Width;

        theGrid = _boardState.GetGrid();
    }

    public void AddPawn(int x, int y, EPlayerSide player)
    {
        if (IsInRange(x, y) && IsEmptyCell(x, y))
        {
            CellState playerCellState = CellState.GetCellStateForPlayer(player);
            theGrid[x, y] = playerCellState;
        }
    }

    private bool ReturnLeftCells(int x, int y, CellState playerCell)
    {
        int returnedCount = 0;
        for (int i = x-1 ; i >= 0; i--)
        {
            bool ok = ReturnOppositeCell(i, y, playerCell);
            if (ok)
            {
                returnedCount++;
            }
            else
            {
                break;
            }
        }
        if (returnedCount > 0)
        {
            return true;
        }

        return false;
    }

    private bool ReturnOppositeCell(int x, int y, CellState playerCell)
    {
        CellState current = theGrid[x, y];
        bool isCellFull = !current.isFree;
        bool isOppositePlayer = !(current.playerSide == playerCell.playerSide);
        if (isCellFull && isOppositePlayer)
        {
            theGrid[x,y] = playerCell;
            return true;
        }
        return false;
    }

    public bool IsEmptyCell(int x, int y)
    {
        CellState state = theGrid[x, y];
        return state.isFree;
    }

    private bool IsInRange(int x, int y)
    {
        return
            (
                x >= 0 && x < height &&
                y >= 0 && y < width
            );
    }

}
