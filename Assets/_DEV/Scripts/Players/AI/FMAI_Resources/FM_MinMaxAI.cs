﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FM_MinMaxAI : FM_AbstractAI
{
    private FM_Node root;

    public FM_MinMaxAI(EPlayerSide _playerSide, int _treeDepth) : base(_playerSide, _treeDepth)
    {
    }

    public override Coords GetMove(BoardState _boardState)
    {
        ComputeTree(_boardState);
        Coords result = ComputeMinMax();
        return result;
    }

    private void ComputeTree(BoardState _boardState)
    {
        BuildTree(_boardState, treeDepth);
    }

    private Coords ComputeMinMax()
    {
        //Debug.Log("GetMove - Start");
        Coords theBestMove = Coords.upLeft;
        int maxVal = int.MinValue;

        List<FM_Node> listNode = root.GetChildren();
        for (int i = 0; i < listNode.Count; i++)
        {
            int val = Min(listNode[i], treeDepth);
            if (val > maxVal)
            {
                maxVal = val;
                theBestMove = listNode[i].GetMove();
            }
        }
        //Debug.Log("GetMove - End");
        return theBestMove;
    }

    private void BuildTree(BoardState _theBoard, int _depth)
    {
        //Debug.Log("BuildTree - Start");
        System.Diagnostics.Stopwatch countPawnWatcher = System.Diagnostics.Stopwatch.StartNew();
        int totalPlayer = _theBoard.CountPawns(PlayerSide);
        int totalOpposite = _theBoard.CountPawns(ComputeOppositePlayer(PlayerSide));
        countPawnWatcher.Stop();
        countPawnCount += 2;
        tsComputeScore = tsComputeScore.Add(countPawnWatcher.Elapsed);


        System.Diagnostics.Stopwatch nodeInstantiateWatcher = System.Diagnostics.Stopwatch.StartNew();
        root = new FM_Node(ComputeOppositePlayer(PlayerSide), Coords.upLeft, totalPlayer, totalOpposite, totalPlayer, totalOpposite);
        nodeInstantiateWatcher.Stop();
        nodeIntanceCount++;
        tsNodeInstantiate = tsNodeInstantiate.Add(nodeInstantiateWatcher.Elapsed);

        if (_theBoard.HasPossibleMoves(PlayerSide))
        {
            System.Diagnostics.Stopwatch findPossibleMovesWatcher = System.Diagnostics.Stopwatch.StartNew();
            List<BoardMove> possibleMove = _theBoard.FindPossibleMoves(PlayerSide);
            findPossibleMoveCount++;
            findPossibleMovesWatcher.Stop();
            tsFindPossibleMove = tsFindPossibleMove.Add(findPossibleMovesWatcher.Elapsed);
            for (int i = 0; i < possibleMove.Count; i++)
            {
                FM_Node newNode = BuildRecursiveTree(_theBoard, possibleMove[i].startPosition, _depth - 1, PlayerSide);
                root.AddChild(newNode);
            }
        }
        else
        {
            if (_theBoard.HasPossibleMoves(ComputeOppositePlayer(PlayerSide)))
            {
                FM_Node newNode = BuildRecursiveTree(_theBoard, Coords.upLeft, _depth - 1, PlayerSide);
                root.AddChild(newNode);
            }
        }
        //Debug.Log("BuildTree - End");
    }

    private FM_Node BuildRecursiveTree(BoardState _theBoard, Coords _move, int _depth, EPlayerSide _playerSide)
    {
        //Debug.Log("BuildRecursiveTree - Start - Depth : " + _depth);
        BoardState boardStateCopy = ScriptableObject.Instantiate<BoardState>(_theBoard);
        if (boardStateCopy.CanAppendPawn(_move.x, _move.y, _playerSide))
        {
            System.Diagnostics.Stopwatch appendPawnWatcher = System.Diagnostics.Stopwatch.StartNew();
            boardStateCopy.AppendPawn(_move.x, _move.y, _playerSide);
            appendPawnCount++;
            appendPawnWatcher.Stop();
            tsLoopAppendPawn = tsLoopAppendPawn.Add(appendPawnWatcher.Elapsed);
        }

        System.Diagnostics.Stopwatch countPawnWatcher = System.Diagnostics.Stopwatch.StartNew();
        int oppositeScore = boardStateCopy.CountPawns(ComputeOppositePlayer(_playerSide)) - _theBoard.CountPawns(ComputeOppositePlayer(_playerSide));
        int playerScore = boardStateCopy.CountPawns(_playerSide) - _theBoard.CountPawns(_playerSide);

        int totalPlayer = _theBoard.CountPawns(PlayerSide);
        int totalOpposite = _theBoard.CountPawns(ComputeOppositePlayer(PlayerSide));
        countPawnWatcher.Stop();
        countPawnCount += 6;
        tsComputeScore = tsComputeScore.Add(countPawnWatcher.Elapsed);


        System.Diagnostics.Stopwatch nodeInstantiateWatcher = System.Diagnostics.Stopwatch.StartNew();
        FM_Node theNode = new FM_Node(_playerSide, _move, playerScore, oppositeScore, totalPlayer, totalOpposite);
        nodeInstantiateWatcher.Stop();
        nodeIntanceCount++;
        tsNodeInstantiate = tsNodeInstantiate.Add(nodeInstantiateWatcher.Elapsed);

        if (_depth >= 0)
        {
            EPlayerSide theRealPlayer = ComputeOppositePlayer(_playerSide);

            System.Diagnostics.Stopwatch hasPossibleMoveWatcher = System.Diagnostics.Stopwatch.StartNew();
            bool canPlayerMove = _theBoard.HasPossibleMoves(theRealPlayer);
            bool canOppositeMove = _theBoard.HasPossibleMoves(ComputeOppositePlayer(theRealPlayer));
            hasPossibleMoveWatcher.Stop();
            hasPossibleMoveCount += 2;
            tsHasPossibleMove = tsHasPossibleMove.Add(hasPossibleMoveWatcher.Elapsed);

            if (canPlayerMove)
            {
                System.Diagnostics.Stopwatch findPossibleMovesWatcher = System.Diagnostics.Stopwatch.StartNew();
                List<BoardMove> possibleMove = _theBoard.FindPossibleMoves(theRealPlayer);
                findPossibleMoveCount++;
                findPossibleMovesWatcher.Stop();
                tsFindPossibleMove = tsFindPossibleMove.Add(findPossibleMovesWatcher.Elapsed);
                for (int i = 0; i < possibleMove.Count; i++)
                {
                    FM_Node newNode = BuildRecursiveTree(_theBoard, possibleMove[i].startPosition, _depth - 1, theRealPlayer);
                    theNode.AddChild(newNode);
                }
            }
            else
            {
                if (canOppositeMove)
                {
                    FM_Node newNode = BuildRecursiveTree(_theBoard, Coords.upLeft, _depth - 1, theRealPlayer);
                    theNode.AddChild(newNode);
                }
            }
        }
        //Debug.Log("BuildRecursiveTree - End - Depth : " + _depth);
        return theNode;
    }

    private Coords GetMove()
    {
        //Debug.Log("GetMove - Start");
        Coords theBestMove = Coords.upLeft;
        int maxVal = int.MinValue;

        List<FM_Node> listNode = root.GetChildren();
        for (int i = 0; i < listNode.Count; i++)
        {
            int val = Min(listNode[i], treeDepth);
            if (val > maxVal)
            {
                maxVal = val;
                theBestMove = listNode[i].GetMove();
            }
        }
        //Debug.Log("GetMove - End");
        return theBestMove;
    }

    private int Min(FM_Node _actualNode, int _depth)
    {
        //Debug.Log("Min - Start - Depth : " + _depth);
        if (_depth == 0 || _actualNode.GetChildren().Count == 0)
        {
            return Eval(_actualNode, _depth);
        }
        int minVal = int.MaxValue;
        List<FM_Node> listNode = _actualNode.GetChildren();
        for (int i = 0; i < listNode.Count; i++)
        {
            int val = Max(listNode[i], _depth - 1);
            if (val < minVal)
            {
                minVal = val;
            }
        }

        //Debug.Log("Min - End - Depth : " + _depth);
        return minVal;
    }

    private int Max(FM_Node _actualNode, int _depth)
    {
        //Debug.Log("Max - Start - Depth : " + _depth);
        if (_depth == 0 || _actualNode.GetChildren().Count == 0)
        {
            return Eval(_actualNode, _depth);
        }
        int maxVal = int.MinValue;
        List<FM_Node> listNode = _actualNode.GetChildren();
        for (int i = 0; i < listNode.Count; i++)
        {
            int val = Min(listNode[i], _depth - 1);
            if (val > maxVal)
            {
                maxVal = val;
            }
        }
        //Debug.Log("Max - End - Depth : " + _depth);

        return maxVal;
    }

    private int Eval(FM_Node _endNode, int _depth)
    {
        //Debug.Log("Eval - Start - Depth : " + _depth);
        int score = 0;

        EPlayerSide lastPlayer = _endNode.GetPlayer();
        int oppositeScore = _endNode.GetOppositeScore();
        int playerScore = _endNode.GetPlayerScore();
        int totalPlayerPawn = _endNode.GetCountPlayerPawn();
        int totalOppositePawn = _endNode.GetCountOppositePawn();
        if (_depth == 0)
        {
            if (lastPlayer == PlayerSide)
            {
                if (playerScore > oppositeScore)
                {
                    score = playerScore - oppositeScore;
                }
                else if (oppositeScore > playerScore)
                {
                    score = -(oppositeScore - playerScore);
                }
                else
                {
                    score = totalPlayerPawn - totalOppositePawn;
                }
            }
            else
            {
                if (playerScore > oppositeScore)
                {
                    score = -(playerScore - oppositeScore);
                }
                else if (oppositeScore > playerScore)
                {
                    score = (oppositeScore - playerScore);
                }
                else
                {
                    score = -(totalPlayerPawn - totalOppositePawn);
                }
            }
        }
        else
        {
            if (lastPlayer == PlayerSide)
            {
                if (totalPlayerPawn > totalOppositePawn)
                {
                    score = 100000 + playerScore;
                }
                else if (totalOppositePawn > totalPlayerPawn)
                {
                    score = -(100000 + oppositeScore);
                }
                else
                {
                    score = 0;
                }
            }
            else
            {
                if (totalPlayerPawn > totalOppositePawn)
                {
                    score = -(100000 + playerScore);
                }
                else if (totalOppositePawn > totalPlayerPawn)
                {
                    score = 100000 + oppositeScore;
                }
                else
                {
                    score = 0;
                }
            }
        }
        //Debug.Log("Eval - Start - Depth : " + _depth);

        return score;
    }


}
