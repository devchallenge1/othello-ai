﻿using System.Collections;
using System.Collections.Generic;

public class FM_Node {
        //BoardState actualBoard;
        EPlayerSide actualPlayer;
        Coords actualMove;
        List<FM_Node> children;

        int scorePlayer;
        int scoreOpposite;
        int actualPlayerCount;
        int actualOppositeCount;

        public FM_Node(EPlayerSide _player, Coords _move, int _scorePlayer, int _scoreOpposite, int _actualPlayerCount, int _actualOppositeCount)
        {
            //actualBoard = _board;
            actualPlayer = _player;
            actualMove = _move;
            children = new List<FM_Node>();
            scorePlayer = _scorePlayer;
            scoreOpposite = _scoreOpposite;
            actualPlayerCount = _actualPlayerCount;
            actualOppositeCount = _actualOppositeCount;
        }
        private EPlayerSide ComputeOppositePlayer()
        {
            EPlayerSide oppositeColor = EPlayerSide.White;
            switch (actualPlayer)
            {
                case EPlayerSide.Black:
                    oppositeColor = EPlayerSide.White;
                    break;
                case EPlayerSide.White:
                    oppositeColor = EPlayerSide.Black;
                    break;
            }
            return oppositeColor;
        }

        public void AddChild(FM_Node _node)
        {
            children.Add(_node);
        }
        public List<FM_Node> GetChildren()
        {
            return children;
        }
        public Coords GetMove()
        {
            return actualMove;
        }
        public EPlayerSide GetPlayer()
        {
            return actualPlayer;
        }
        public int GetOppositeScore()
        {
            return scoreOpposite;
        }
        public int GetPlayerScore()
        {
            return scorePlayer;
        }
        public int GetCountPlayerPawn()
        {
            return actualPlayerCount;
        }
        public int GetCountOppositePawn()
        {
            return actualOppositeCount;
        }
}
