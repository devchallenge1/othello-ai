﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FM_AlphaBetaAI : FM_AbstractAI
{
    private FM_Node root;

    private int[,] weightingGrid;
    private int gridWidth = 0;
    private int gridHeight = 0;
    private bool initDone = false;

    private int maxWeigthValue = 100;
    private int normalWeigthValue = 1;
    private int minWeigthValue = -50;

    public FM_AlphaBetaAI(EPlayerSide _playerSide, int _treeDepth) : base(_playerSide, _treeDepth)
    {
    }

    private void Init()
    {
        if (!initDone)
        {
            initDone = true;
            InitWeightingGrid();
        }
    }
    private void InitWeightingGrid()
    {
        weightingGrid = new int[gridHeight, gridWidth];
        for (int i = 0; i < gridHeight; i++)
        {
            for (int j = 0; j < gridWidth; j++)
            {
                weightingGrid[i, j] = normalWeigthValue;
            }
        }
        weightingGrid[0, 0] = maxWeigthValue;
        weightingGrid[gridHeight - 1, 0] = maxWeigthValue;
        weightingGrid[0, gridWidth - 1] = maxWeigthValue;
        weightingGrid[gridHeight - 1, gridWidth - 1] = maxWeigthValue;

        weightingGrid[0, 1] = minWeigthValue;
        weightingGrid[1, 1] = minWeigthValue;
        weightingGrid[1, 0] = minWeigthValue;

        weightingGrid[gridHeight - 2, 0] = minWeigthValue;
        weightingGrid[gridHeight - 2, 1] = minWeigthValue;
        weightingGrid[gridHeight - 1, 1] = minWeigthValue;

        weightingGrid[gridHeight - 2, gridWidth - 1] = minWeigthValue;
        weightingGrid[gridHeight - 2, gridWidth - 2] = minWeigthValue;
        weightingGrid[gridHeight - 1, gridWidth - 2] = minWeigthValue;

        weightingGrid[0, gridWidth - 2] = minWeigthValue;
        weightingGrid[1, gridWidth - 2] = minWeigthValue;
        weightingGrid[1, gridWidth - 1] = minWeigthValue;
    }


    public override Coords GetMove(BoardState _boardState)
    {
        gridHeight = _boardState.Height;
        gridWidth = _boardState.Width;
        Init();
        ComputeTree(_boardState);
        Coords result = ComputeAlphaBeta();
        return result;
    }

    private void ComputeTree(BoardState _boardState)
    {
        BuildTree(_boardState, treeDepth);
    }

    private void BuildTree(BoardState _theBoard, int _depth)
    {
        //Debug.Log("BuildTree - Start");
        System.Diagnostics.Stopwatch countPawnWatcher = System.Diagnostics.Stopwatch.StartNew();
        int totalPlayer = _theBoard.CountPawns(PlayerSide);
        int totalOpposite = _theBoard.CountPawns(ComputeOppositePlayer(PlayerSide));
        countPawnWatcher.Stop();
        countPawnCount += 2;
        tsComputeScore = tsComputeScore.Add(countPawnWatcher.Elapsed);


        System.Diagnostics.Stopwatch nodeInstantiateWatcher = System.Diagnostics.Stopwatch.StartNew();
        root = new FM_Node(ComputeOppositePlayer(PlayerSide), Coords.upLeft, totalPlayer, totalOpposite, totalPlayer, totalOpposite);
        nodeInstantiateWatcher.Stop();
        nodeIntanceCount++;
        tsNodeInstantiate = tsNodeInstantiate.Add(nodeInstantiateWatcher.Elapsed);

        if (_theBoard.HasPossibleMoves(PlayerSide))
        {
            System.Diagnostics.Stopwatch findPossibleMovesWatcher = System.Diagnostics.Stopwatch.StartNew();
            List<BoardMove> possibleMove = _theBoard.FindPossibleMoves(PlayerSide);
            findPossibleMoveCount++;
            findPossibleMovesWatcher.Stop();
            tsFindPossibleMove = tsFindPossibleMove.Add(findPossibleMovesWatcher.Elapsed);
            for (int i = 0; i < possibleMove.Count; i++)
            {
                FM_Node newNode = BuildRecursiveTree(_theBoard, possibleMove[i].startPosition, _depth - 1, PlayerSide);
                root.AddChild(newNode);
            }
        }
        else
        {
            if (_theBoard.HasPossibleMoves(ComputeOppositePlayer(PlayerSide)))
            {
                FM_Node newNode = BuildRecursiveTree(_theBoard, Coords.upLeft, _depth - 1, PlayerSide);
                root.AddChild(newNode);
            }
        }
        //Debug.Log("BuildTree - End");
    }

    private FM_Node BuildRecursiveTree(BoardState _theBoard, Coords _move, int _depth, EPlayerSide _playerSide)
    {
        //Debug.Log("BuildRecursiveTree - Start - Depth : " + _depth);
        BoardState boardStateCopy = ScriptableObject.Instantiate<BoardState>(_theBoard);
        if (boardStateCopy.CanAppendPawn(_move.x, _move.y, _playerSide))
        {
            System.Diagnostics.Stopwatch appendPawnWatcher = System.Diagnostics.Stopwatch.StartNew();
            boardStateCopy.AppendPawn(_move.x, _move.y, _playerSide);
            appendPawnCount++;
            appendPawnWatcher.Stop();
            tsLoopAppendPawn = tsLoopAppendPawn.Add(appendPawnWatcher.Elapsed);
        }

        System.Diagnostics.Stopwatch countPawnWatcher = System.Diagnostics.Stopwatch.StartNew();
        int oppositeScore = boardStateCopy.CountPawns(ComputeOppositePlayer(_playerSide)) - _theBoard.CountPawns(ComputeOppositePlayer(_playerSide));
        int playerScore = boardStateCopy.CountPawns(_playerSide) - _theBoard.CountPawns(_playerSide);

        int totalPlayer = _theBoard.CountPawns(PlayerSide);
        int totalOpposite = _theBoard.CountPawns(ComputeOppositePlayer(PlayerSide));
        countPawnWatcher.Stop();
        countPawnCount += 6;
        tsComputeScore = tsComputeScore.Add(countPawnWatcher.Elapsed);


        System.Diagnostics.Stopwatch nodeInstantiateWatcher = System.Diagnostics.Stopwatch.StartNew();
        FM_Node theNode = new FM_Node(_playerSide, _move, playerScore, oppositeScore, totalPlayer, totalOpposite);
        nodeInstantiateWatcher.Stop();
        nodeIntanceCount++;
        tsNodeInstantiate = tsNodeInstantiate.Add(nodeInstantiateWatcher.Elapsed);

        if (_depth >= 0)
        {
            EPlayerSide theRealPlayer = ComputeOppositePlayer(_playerSide);

            System.Diagnostics.Stopwatch hasPossibleMoveWatcher = System.Diagnostics.Stopwatch.StartNew();
            bool canPlayerMove = _theBoard.HasPossibleMoves(theRealPlayer);
            bool canOppositeMove = _theBoard.HasPossibleMoves(ComputeOppositePlayer(theRealPlayer));
            hasPossibleMoveWatcher.Stop();
            hasPossibleMoveCount += 2;
            tsHasPossibleMove = tsHasPossibleMove.Add(hasPossibleMoveWatcher.Elapsed);

            if (canPlayerMove)
            {
                System.Diagnostics.Stopwatch findPossibleMovesWatcher = System.Diagnostics.Stopwatch.StartNew();
                List<BoardMove> possibleMove = _theBoard.FindPossibleMoves(theRealPlayer);
                findPossibleMoveCount++;
                findPossibleMovesWatcher.Stop();
                tsFindPossibleMove = tsFindPossibleMove.Add(findPossibleMovesWatcher.Elapsed);
                for (int i = 0; i < possibleMove.Count; i++)
                {
                    FM_Node newNode = BuildRecursiveTree(_theBoard, possibleMove[i].startPosition, _depth - 1, theRealPlayer);
                    theNode.AddChild(newNode);
                }
            }
            else
            {
                if (canOppositeMove)
                {
                    FM_Node newNode = BuildRecursiveTree(_theBoard, Coords.upLeft, _depth - 1, theRealPlayer);
                    theNode.AddChild(newNode);
                }
            }
        }
        //Debug.Log("BuildRecursiveTree - End - Depth : " + _depth);
        return theNode;
    }


    private Coords ComputeAlphaBeta()
    {
        //Debug.Log("GetMove - Start");
        Coords theBestMove = Coords.upLeft;
        int maxVal = int.MinValue;

        List<FM_Node> listNode = root.GetChildren();
        for (int i = 0; i < listNode.Count; i++)
        {
            int val = Alpha(listNode[i], int.MinValue, int.MaxValue);
            if (val > maxVal)
            {
                maxVal = val;
                theBestMove = listNode[i].GetMove();
            }
        }
        //Debug.Log("GetMove - End");
        return theBestMove;
    }


    private int Alpha(FM_Node _node, int _alpha, int _beta)
    {
        if (_node.GetChildren().Count == 0)
        {
            return EvalAlphaBeta(_node);
        }
        int alpha = int.MinValue;
        for (int i = 0; i < _node.GetChildren().Count; i++)
        {
            int val = Beta(_node.GetChildren()[i], Math.Max(_alpha, alpha), _beta);
            if (val > alpha)
            {
                alpha = val;
            }
            if (alpha >= _beta)
            {
                return alpha;
            }
        }
        return alpha;
    }

    private int Beta(FM_Node _node, int _alpha, int _beta)
    {
        if (_node.GetChildren().Count == 0)
        {
            return EvalAlphaBeta(_node);
        }
        int beta = int.MaxValue;
        for (int i = 0; i < _node.GetChildren().Count; i++)
        {
            int val = Beta(_node.GetChildren()[i], _alpha, Math.Min(_beta, beta));
            if (val > beta)
            {
                beta = val;
            }
            if (_alpha >= beta)
            {
                return beta;
            }
        }
        return beta;
    }


    private int EvalAlphaBeta(FM_Node _endNode)
    {
        //Debug.Log("EvalAlphaBEta - Start");
        int score = 0;

        //EPlayerSide lastPlayer = _endNode.GetPlayer();
        int oppositeScore = _endNode.GetOppositeScore();
        int playerScore = _endNode.GetPlayerScore();
        int totalPlayerPawn = _endNode.GetCountPlayerPawn();
        int totalOppositePawn = _endNode.GetCountOppositePawn();
        Coords move = _endNode.GetMove();
        int weight = -1;
        if (move.x >= 0 && move.x < gridHeight && move.y >= 0 && move.y < gridWidth)
        {
            weight = weightingGrid[move.x, move.y];

        }

        if (weight == minWeigthValue)
        {
            score = -100000 - (totalPlayerPawn - totalOppositePawn);
        }
        else if (weight == maxWeigthValue)
        {
            score = 100000 + (totalPlayerPawn - totalOppositePawn);
        }
        else
        {
            score = (playerScore - oppositeScore) * (totalPlayerPawn - totalOppositePawn);
        }
        return score;
    }

}
