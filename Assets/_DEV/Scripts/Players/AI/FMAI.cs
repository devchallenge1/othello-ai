﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MovementEffects;

[CreateAssetMenu(fileName = "FMAI", menuName = "Othello - Player/FM AI", order = 750)]
public class FMAI : PlayerAsset
{
    private bool minMaxAI = false;
    private bool alphaBetaAI = false;
    private FM_AbstractAI ai = null;
    private bool initDone = false;

    public int treeDepth = 3;

    protected override void OnBeginTurn()
    {
        Main();
    }

    private void Main()
    {
        Init();
        Coords move = ai.GetMove(this.GetBoardStateCopy());
        this.AppendPawn(move);
    }

    private void Init()
    {
        if (! initDone)
        {
            initDone = true;

            if (minMaxAI)
            {
                ai = new FM_MinMaxAI(PlayerSide, treeDepth);
            }
            else if(alphaBetaAI)
            {
                ai = new FM_AlphaBetaAI(PlayerSide, treeDepth);
            }
            else
            {
                ai = new FM_MinimalAlphaBeta(PlayerSide, treeDepth);
            }
        }
    }

    //private void Main()
    //{
    //    //Debug.ClearDeveloperConsole();
    //    System.Diagnostics.Stopwatch mainWatcher = System.Diagnostics.Stopwatch.StartNew();

    //    BoardState BSCopy = this.GetBoardStateCopy();
    //    gridHeight = BSCopy.Height;
    //    gridWidth = BSCopy.Width;

    //    System.Diagnostics.Stopwatch initWatcher = System.Diagnostics.Stopwatch.StartNew();
    //    this.Init();
    //    initWatcher.Stop();
    //    tsInit = initWatcher.Elapsed;

    //    System.Diagnostics.Stopwatch buildTreeWatcher = System.Diagnostics.Stopwatch.StartNew();
    //    this.BuildTree(BSCopy, treeDepth);
    //    buildTreeWatcher.Stop();
    //    tsBuildTree = buildTreeWatcher.Elapsed;


        
    //    System.Diagnostics.Stopwatch getMoveWatcher = System.Diagnostics.Stopwatch.StartNew();
    //    Coords move = this.GetMoveAlphaBeta();
    //    getMoveWatcher.Stop();
    //    tsGetMove = getMoveWatcher.Elapsed;


    //    System.Diagnostics.Stopwatch clearAIWatcher = System.Diagnostics.Stopwatch.StartNew();
    //    ClearAI();
    //    clearAIWatcher.Stop();
    //    tsClearAI = clearAIWatcher.Elapsed;

    //    System.Diagnostics.Stopwatch appendPawnWatcher = System.Diagnostics.Stopwatch.StartNew();
    //    this.AppendPawn(move);
    //    appendPawnWatcher.Stop();
    //    tsMainAppendPawn = appendPawnWatcher.Elapsed;


    //    mainWatcher.Stop();
    //    tsMain = mainWatcher.Elapsed;

    //    PrintDebug();

    //}

    //private void PrintDebug()
    //{
    //    if (turnCount == 1)
    //    {
    //        Debug.Log("Board size - x : "+gridHeight+" - y : "+gridWidth);
    //        string weigthGridDebug = "";
    //        for (int i = 0; i < gridHeight; i++)
    //        {
    //            for(int j = 0; j < gridWidth; j++)
    //            {
    //                if (j==0)
    //                {
    //                    weigthGridDebug += "|";
    //                }
    //                weigthGridDebug += String.Format(" {0,-5} |", weightingGrid[i,j]);
    //            }
    //            weigthGridDebug += "\n";
    //        }
    //        Debug.Log(weigthGridDebug);
    //    }

    //    string mainTime = String.Format("{0:hh\\:mm\\:ss}", tsMain);
    //    string buildTreeTime = String.Format("{0:hh\\:mm\\:ss}", tsBuildTree);
    //    string getMoveTime = String.Format("{0:hh\\:mm\\:ss}", tsGetMove);
    //    string clearAITime = String.Format("{0:hh\\:mm\\:ss}", tsClearAI);
    //    string finalAppendPawnTime = String.Format("{0:hh\\:mm\\:ss}", tsMainAppendPawn);
    //    string findPossibleMoveTime = String.Format("{0:hh\\:mm\\:ss}", tsFindPossibleMove);
    //    string nodeInstantiateTime = String.Format("{0:hh\\:mm\\:ss}", tsNodeInstantiate);
    //    string countPawnTime = String.Format("{0:hh\\:mm\\:ss}", tsComputeScore);
    //    string appendPawnTime = String.Format("{0:hh\\:mm\\:ss}", tsLoopAppendPawn);
    //    string hasPossibleMoveTime = String.Format("{0:hh\\:mm\\:ss}", tsHasPossibleMove);
    //    string initTime = String.Format("{0:hh\\:mm\\:ss}", tsInit);
        
    //    Debug.Log("-------- Turn : " + turnCount + " --------"                                               + "\n"
    //            + "Main : " + mainTime                                                                       + "\n"
    //            + "|--> Init : " + initTime                                                                  + "\n"
    //            + "|--> BuildTree (Depth : " + treeDepth + ") : " + buildTreeTime                            + "\n"
    //            + "| |--> FindPossibleMove (Call : " + findPossibleMoveCount + ") : " + findPossibleMoveTime + "\n"
    //            + "| |--> AppendPawn (Call : " + appendPawnCount + "): " + appendPawnTime                    + "\n"
    //            + "| |--> NodeInstantiate (Call : " + nodeIntanceCount + ") : " + nodeInstantiateTime        + "\n"
    //            + "| |--> HasPossibleMove (Call : " + hasPossibleMoveCount + "): " + hasPossibleMoveTime     + "\n"
    //            + "| |--> CountPawn (Call : " + countPawnCount + "): " + countPawnTime                       + "\n"
    //            + "|--> GetMove : " + getMoveTime                                                            + "\n"
    //            + "|--> ClearAI : " + clearAITime                                                            + "\n"
    //            + "|--> finalAppendPawn : " + finalAppendPawnTime);
        
    //}

    //private void ClearAI()
    //{
    //    RecursiveCleanTree(root);
    //}

    //private void RecursiveCleanTree(Node _node)
    //{
    //    List<Node> children = _node.GetChildren();
    //    if (children.Count > 0)
    //    {
    //        for (int i = 0; i < children.Count; i++)
    //        {
    //            RecursiveCleanTree(children[i]);
    //        }
    //    }
    //    _node = null;

    //}

    //private void Init()
    //{
    //    if (! initDone)
    //    {
    //        initDone = true;
    //        treeDepth = 5;
    //        InitWeightingGrid();
    //    }
    //    //if (turnCount%2 == 0)
    //    //{
    //    //    treeDepth += 1;
    //    //}
    //    turnCount += 1;
    //    findPossibleMoveCount = 0;
    //    appendPawnCount = 0;
    //    countPawnCount = 0;
    //    nodeIntanceCount = 0;
    //    hasPossibleMoveCount = 0;

    //    tsFindPossibleMove = TimeSpan.Zero;
    //    tsLoopAppendPawn = TimeSpan.Zero;
    //    tsMain = TimeSpan.Zero;
    //    tsInit = TimeSpan.Zero;
    //    tsBuildTree = TimeSpan.Zero;
    //    tsGetMove = TimeSpan.Zero;
    //    tsMainAppendPawn = TimeSpan.Zero;
    //    tsClearAI = TimeSpan.Zero;
    //    tsComputeScore = TimeSpan.Zero;
    //    tsNodeInstantiate = TimeSpan.Zero;
    //    tsHasPossibleMove = TimeSpan.Zero;


    //    Debug.Log("Init ok");
    //}

    //private void InitWeightingGrid()
    //{
    //    weightingGrid = new int[gridHeight, gridWidth];
    //    for (int i = 0; i < gridHeight; i++)
    //    {
    //        for (int j = 0; j < gridWidth; j++)
    //        {
    //            weightingGrid[i,j] = 1;
    //        }
    //    }
    //    weightingGrid[0, 0] = 100;
    //    weightingGrid[gridHeight-1, 0] = 100;
    //    weightingGrid[0, gridWidth-1] = 100;
    //    weightingGrid[gridHeight - 1, gridWidth - 1] = 100;

    //    weightingGrid[0, 1] = -50;
    //    weightingGrid[1, 1] = -50;
    //    weightingGrid[1, 0] = -50;
        
    //    weightingGrid[gridHeight - 2, 0] = -50;
    //    weightingGrid[gridHeight - 2, 1] = -50;
    //    weightingGrid[gridHeight - 1, 1] = -50;

    //    weightingGrid[gridHeight - 2, gridWidth - 1] = -50;
    //    weightingGrid[gridHeight - 2, gridWidth - 2] = -50;
    //    weightingGrid[gridHeight - 1, gridWidth - 2] = -50;

    //    weightingGrid[0, gridWidth - 2] = -50;
    //    weightingGrid[1, gridWidth - 2] = -50;
    //    weightingGrid[1, gridWidth - 1] = -50;
    //}

    //private void BuildTree(BoardState _theBoard, int _depth)
    //{
    //    //Debug.Log("BuildTree - Start");
    //    System.Diagnostics.Stopwatch countPawnWatcher = System.Diagnostics.Stopwatch.StartNew();
    //    int totalPlayer = _theBoard.CountPawns(PlayerSide);
    //    int totalOpposite = _theBoard.CountPawns(ComputeOppositePlayer(PlayerSide));
    //    countPawnWatcher.Stop();
    //    countPawnCount += 2;
    //    tsComputeScore = tsComputeScore.Add(countPawnWatcher.Elapsed);


    //    System.Diagnostics.Stopwatch nodeInstantiateWatcher = System.Diagnostics.Stopwatch.StartNew();
    //    root = new Node(ComputeOppositePlayer(PlayerSide), Coords.upLeft, totalPlayer, totalOpposite, totalPlayer, totalOpposite);
    //    nodeInstantiateWatcher.Stop();
    //    nodeIntanceCount++;
    //    tsNodeInstantiate = tsNodeInstantiate.Add(nodeInstantiateWatcher.Elapsed);

    //    if (_theBoard.HasPossibleMoves(PlayerSide))
    //    {
    //        System.Diagnostics.Stopwatch findPossibleMovesWatcher = System.Diagnostics.Stopwatch.StartNew();
    //        List<BoardMove> possibleMove = _theBoard.FindPossibleMoves(PlayerSide);
    //        findPossibleMoveCount++;
    //        findPossibleMovesWatcher.Stop();
    //        tsFindPossibleMove = tsFindPossibleMove.Add(findPossibleMovesWatcher.Elapsed);
    //        for (int i = 0; i < possibleMove.Count; i++)
    //        {
    //            Node newNode = BuildRecursiveTree(_theBoard, possibleMove[i].startPosition, _depth-1, PlayerSide);
    //            root.AddChild(newNode);
    //        }
    //    }
    //    else
    //    {
    //        if (_theBoard.HasPossibleMoves(ComputeOppositePlayer(PlayerSide)))
    //        {
    //            Node newNode = BuildRecursiveTree(_theBoard, Coords.upLeft, _depth-1, PlayerSide);
    //            root.AddChild(newNode);
    //        }
    //    }
    //    //Debug.Log("BuildTree - End");
    //}

    //private Node BuildRecursiveTree(BoardState _theBoard, Coords _move, int _depth, EPlayerSide _playerSide)
    //{
    //    //Debug.Log("BuildRecursiveTree - Start - Depth : " + _depth);
    //    BoardState boardStateCopy = ScriptableObject.Instantiate<BoardState>(_theBoard);
    //    if (boardStateCopy.CanAppendPawn(_move.x, _move.y, _playerSide))
    //    {
    //        System.Diagnostics.Stopwatch appendPawnWatcher = System.Diagnostics.Stopwatch.StartNew();
    //        boardStateCopy.AppendPawn(_move.x, _move.y, _playerSide);
    //        appendPawnCount++;
    //        appendPawnWatcher.Stop();
    //        tsLoopAppendPawn = tsLoopAppendPawn.Add(appendPawnWatcher.Elapsed);
    //    }

    //    System.Diagnostics.Stopwatch countPawnWatcher = System.Diagnostics.Stopwatch.StartNew();
    //    int oppositeScore = boardStateCopy.CountPawns(ComputeOppositePlayer(_playerSide)) - _theBoard.CountPawns(ComputeOppositePlayer(_playerSide));
    //    int playerScore = boardStateCopy.CountPawns(_playerSide) - _theBoard.CountPawns(_playerSide);

    //    int totalPlayer = _theBoard.CountPawns(PlayerSide);
    //    int totalOpposite = _theBoard.CountPawns(ComputeOppositePlayer(PlayerSide));
    //    countPawnWatcher.Stop();
    //    countPawnCount += 6;
    //    tsComputeScore = tsComputeScore.Add(countPawnWatcher.Elapsed);


    //    System.Diagnostics.Stopwatch nodeInstantiateWatcher = System.Diagnostics.Stopwatch.StartNew();
    //    Node theNode = new Node(_playerSide, _move, playerScore,oppositeScore, totalPlayer, totalOpposite);
    //    nodeInstantiateWatcher.Stop();
    //    nodeIntanceCount++;
    //    tsNodeInstantiate = tsNodeInstantiate.Add(nodeInstantiateWatcher.Elapsed);

    //    if (_depth >= 0) {
    //        EPlayerSide theRealPlayer = ComputeOppositePlayer(_playerSide);

    //        System.Diagnostics.Stopwatch hasPossibleMoveWatcher = System.Diagnostics.Stopwatch.StartNew();
    //        bool canPlayerMove = _theBoard.HasPossibleMoves(theRealPlayer);
    //        bool canOppositeMove = _theBoard.HasPossibleMoves(ComputeOppositePlayer(theRealPlayer));
    //        hasPossibleMoveWatcher.Stop();
    //        hasPossibleMoveCount += 2;
    //        tsHasPossibleMove = tsHasPossibleMove.Add(hasPossibleMoveWatcher.Elapsed);

    //        if (canPlayerMove)
    //        {
    //            System.Diagnostics.Stopwatch findPossibleMovesWatcher = System.Diagnostics.Stopwatch.StartNew();
    //            List<BoardMove> possibleMove = _theBoard.FindPossibleMoves(theRealPlayer);
    //            findPossibleMoveCount++;
    //            findPossibleMovesWatcher.Stop();
    //            tsFindPossibleMove = tsFindPossibleMove.Add(findPossibleMovesWatcher.Elapsed);
    //            for (int i = 0; i < possibleMove.Count; i++)
    //            {
    //                Node newNode = BuildRecursiveTree(_theBoard, possibleMove[i].startPosition, _depth - 1, theRealPlayer);
    //                theNode.AddChild(newNode);
    //            }
    //        }
    //        else
    //        {
    //            if (canOppositeMove)
    //            {
    //                Node newNode = BuildRecursiveTree(_theBoard, Coords.upLeft, _depth - 1, theRealPlayer);
    //                theNode.AddChild(newNode);
    //            }
    //        }
    //    }
    //    //Debug.Log("BuildRecursiveTree - End - Depth : " + _depth);
    //    return theNode;
    //}

    //private Coords GetMove()
    //{
    //    //Debug.Log("GetMove - Start");
    //    Coords theBestMove = Coords.upLeft;
    //    int maxVal = int.MinValue;

    //    List<Node> listNode = root.GetChildren();
    //    for (int i = 0; i < listNode.Count; i ++)
    //    {
    //        int val = Min(listNode[i], treeDepth);
    //        if (val > maxVal)
    //        {
    //            maxVal = val;
    //            theBestMove = listNode[i].GetMove();
    //        }
    //    }
    //    //Debug.Log("GetMove - End");
    //    return theBestMove;
    //}

    //private Coords GetMoveAlphaBeta()
    //{
    //    //Debug.Log("GetMove - Start");
    //    Coords theBestMove = Coords.upLeft;
    //    int maxVal = int.MinValue;

    //    List<Node> listNode = root.GetChildren();
    //    for (int i = 0; i < listNode.Count; i++)
    //    {
    //        int val = Alpha(listNode[i], int.MinValue, int.MaxValue);
    //        if (val > maxVal)
    //        {
    //            maxVal = val;
    //            theBestMove = listNode[i].GetMove();
    //        }
    //    }
    //    //Debug.Log("GetMove - End");
    //    return theBestMove;
    //}

    //private int Min(Node _actualNode, int _depth)
    //{
    //    //Debug.Log("Min - Start - Depth : " + _depth);
    //    if (_depth == 0 || _actualNode.GetChildren().Count == 0)
    //    {
    //        return Eval(_actualNode, _depth);
    //    }
    //    int minVal = int.MaxValue;
    //    List<Node> listNode = _actualNode.GetChildren();
    //    for (int i = 0; i < listNode.Count; i++)
    //    {
    //        int val = Max(listNode[i], _depth -1);
    //        if (val < minVal)
    //        {
    //            minVal = val;
    //        }
    //    }

    //    //Debug.Log("Min - End - Depth : " + _depth);
    //    return minVal;
    //}

    //private int Max(Node _actualNode, int _depth)
    //{
    //    //Debug.Log("Max - Start - Depth : " + _depth);
    //    if (_depth == 0 || _actualNode.GetChildren().Count == 0)
    //    {
    //        return Eval(_actualNode, _depth);
    //    }
    //    int maxVal = int.MinValue;
    //    List<Node> listNode = _actualNode.GetChildren();
    //    for (int i = 0; i < listNode.Count; i++)
    //    {
    //        int val = Min(listNode[i], _depth - 1);
    //        if (val > maxVal)
    //        {
    //            maxVal = val;
    //        }
    //    }
    //    //Debug.Log("Max - End - Depth : " + _depth);

    //    return maxVal;
    //}

    //private int Alpha(Node _node, int _alpha, int _beta)
    //{
    //    if (_node.GetChildren().Count == 0)
    //    {
    //        return EvalAlphaBeta(_node);
    //    }
    //    int alpha = int.MinValue;
    //    for (int i = 0; i < _node.GetChildren().Count; i ++)
    //    {
    //        int val = Beta(_node.GetChildren()[i], Math.Max(_alpha, alpha),_beta);
    //        if (val > alpha)
    //        {
    //            alpha = val;
    //        }
    //        if (alpha >= _beta)
    //        {
    //            return alpha;
    //        }
    //    }
    //    return alpha;
    //}

    //private int Beta(Node _node, int _alpha, int _beta)
    //{
    //    if (_node.GetChildren().Count == 0)
    //    {
    //        return EvalAlphaBeta(_node);
    //    }
    //    int beta = int.MaxValue;
    //    for (int i = 0; i < _node.GetChildren().Count; i++)
    //    {
    //        int val = Beta(_node.GetChildren()[i], _alpha, Math.Min(_beta, beta));
    //        if (val > beta)
    //        {
    //            beta = val;
    //        }
    //        if (_alpha >= beta )
    //        {
    //            return beta;
    //        }
    //    }
    //    return beta;
    //}

    //private int Eval(Node _endNode, int _depth)
    //{
    //    //Debug.Log("Eval - Start - Depth : " + _depth);
    //    int score = 0;

    //    EPlayerSide lastPlayer = _endNode.GetPlayer();
    //    int oppositeScore = _endNode.GetOppositeScore();
    //    int playerScore = _endNode.GetPlayerScore();
    //    int totalPlayerPawn = _endNode.GetCountPlayerPawn();
    //    int totalOppositePawn = _endNode.GetCountOppositePawn();
    //    if (_depth == 0)
    //    {
    //        if (lastPlayer == PlayerSide)
    //        {
    //            if (playerScore > oppositeScore)
    //            {
    //                score = playerScore - oppositeScore;
    //            }
    //            else if (oppositeScore > playerScore) 
    //            {
    //                score = -(oppositeScore - playerScore);
    //            }
    //            else
    //            {
    //                score = totalPlayerPawn - totalOppositePawn;
    //            }
    //        }
    //        else
    //        {
    //            if (playerScore > oppositeScore)
    //            {
    //                score = - (playerScore - oppositeScore);
    //            }
    //            else if (oppositeScore > playerScore)
    //            {
    //                score = (oppositeScore - playerScore);
    //            }
    //            else
    //            {
    //                score = - (totalPlayerPawn - totalOppositePawn);
    //            }
    //        }
    //    }
    //    else
    //    {
    //        if (lastPlayer == PlayerSide)
    //        {
    //            if (totalPlayerPawn > totalOppositePawn)
    //            {
    //                score = 100000 + playerScore;
    //            }
    //            else if (totalOppositePawn > totalPlayerPawn)
    //            {
    //                score = -(100000 + oppositeScore);
    //            }
    //            else
    //            {
    //                score = 0;
    //            }
    //        }
    //        else
    //        {
    //            if (totalPlayerPawn > totalOppositePawn)
    //            {
    //                score = -(100000 + playerScore);
    //            }
    //            else if (totalOppositePawn > totalPlayerPawn)
    //            {
    //                score = 100000 + oppositeScore;
    //            }
    //            else
    //            {
    //                score = 0;
    //            }
    //        }
    //    }
    //    //Debug.Log("Eval - Start - Depth : " + _depth);

    //    return score;
    //}

    //private int EvalAlphaBeta(Node _endNode)
    //{
    //    //Debug.Log("EvalAlphaBEta - Start");
    //    int score = 0;

    //    EPlayerSide lastPlayer = _endNode.GetPlayer();
    //    int oppositeScore = _endNode.GetOppositeScore();
    //    int playerScore = _endNode.GetPlayerScore();
    //    int totalPlayerPawn = _endNode.GetCountPlayerPawn();
    //    int totalOppositePawn = _endNode.GetCountOppositePawn();
    //    Coords move = _endNode.GetMove();
    //    int weight = -1;
    //    if (move.x >= 0 && move.x < gridHeight && move.y >= 0 && move.y < gridWidth)
    //    {
    //        weight = weightingGrid[move.x, move.y];

    //    }
    //    if (lastPlayer == PlayerSide)
    //    {
    //        if (totalPlayerPawn > totalOppositePawn)
    //        {
    //            score = 100000 + playerScore* weight;
    //        }
    //        else if (totalOppositePawn > totalPlayerPawn)
    //        {
    //            score = -(100000 + oppositeScore* weight);
    //        }
    //        else
    //        {
    //            score = 0;
    //        }
    //    }
    //    else
    //    {
    //        if (totalPlayerPawn > totalOppositePawn)
    //        {
    //            score = -(100000 + playerScore* weight);
    //        }
    //        else if (totalOppositePawn > totalPlayerPawn)
    //        {
    //            score = 100000 + oppositeScore* weight;
    //        }
    //        else
    //        {
    //            score = 0;
    //        }
    //    }
    //    //Debug.Log("Eval - END");

    //    return score;
    //}

    //private EPlayerSide ComputeOppositePlayer(EPlayerSide _playerColor)
    //{
    //    EPlayerSide oppositeColor = EPlayerSide.White;
    //    switch (_playerColor)
    //    {
    //        case EPlayerSide.Black:
    //            oppositeColor = EPlayerSide.White;
    //            break;
    //        case EPlayerSide.White:
    //            oppositeColor = EPlayerSide.Black;
    //            break;
    //    }
    //    return oppositeColor;
    //}

    //private class Node
    //{
    //    //BoardState actualBoard;
    //    EPlayerSide actualPlayer;
    //    Coords actualMove;
    //    List<Node> children;

    //    int scorePlayer;
    //    int scoreOpposite;
    //    int actualPlayerCount;
    //    int actualOppositeCount;

    //    public Node(EPlayerSide _player, Coords _move, int _scorePlayer, int _scoreOpposite, int _actualPlayerCount, int _actualOppositeCount)
    //    {
    //        //actualBoard = _board;
    //        actualPlayer = _player;
    //        actualMove = _move;
    //        children = new List<Node>();
    //        scorePlayer = _scorePlayer;
    //        scoreOpposite = _scoreOpposite;
    //        actualPlayerCount = _actualPlayerCount;
    //        actualOppositeCount = _actualOppositeCount;
    //    }
    //    private EPlayerSide ComputeOppositePlayer()
    //    {
    //        EPlayerSide oppositeColor = EPlayerSide.White;
    //        switch (actualPlayer)
    //        {
    //            case EPlayerSide.Black:
    //                oppositeColor = EPlayerSide.White;
    //                break;
    //            case EPlayerSide.White:
    //                oppositeColor = EPlayerSide.Black;
    //                break;
    //        }
    //        return oppositeColor;
    //    }
        
    //    public void AddChild(Node _node)
    //    {
    //        children.Add(_node);
    //    }
    //    public List<Node> GetChildren()
    //    {
    //        return children;
    //    }
    //    public Coords GetMove()
    //    {
    //        return actualMove;
    //    }
    //    public EPlayerSide GetPlayer()
    //    {
    //        return actualPlayer;
    //    }
    //    public int GetOppositeScore()
    //    {
    //        return scoreOpposite;
    //    }
    //    public int GetPlayerScore()
    //    {
    //        return scorePlayer;
    //    }
    //    public int GetCountPlayerPawn()
    //    {
    //        return actualPlayerCount;
    //    }
    //    public int GetCountOppositePawn()
    //    {
    //        return actualOppositeCount;
    //    }
    //}
}
