﻿#region Headers

	using System.Collections.Generic;

	using UnityEngine;

	using MovementEffects;

#endregion


///<summary>
/// 
///		At each turn, this AI will play the highest-score move.
/// 
///</summary>
[CreateAssetMenu(fileName = "BestMoveAI", menuName = "Othello - Player/Best Move AI", order = 750)]
public class BestMoveAI : PlayerAsset
{

	#region Attributes

		// Settings

		[Header("Settings")]

		[SerializeField]
		private float m_ThinkInterval = 1.0f;

	#endregion

	
	#region Public Methods

		protected override void OnBeginTurn()
		{
			Timing.RunCoroutine(DoTurn());
		}

	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods

		private IEnumerator<float> DoTurn()
		{
			yield return Timing.WaitForSeconds(m_ThinkInterval);

			BoardState board = GetBoardStateCopy();
			List<BoardMove> moves = board.FindPossibleMoves(PlayerSide);

			int count = moves.Count;
			if(count > 0)
			{
				int score = moves[0].positions.Count;
				int index = 0;

				for(int i = 1; i < count; i++)
				{
					if(moves[i].positions.Count > score)
					{
						score = moves[i].positions.Count;
						index = i;
					}
				}

				if (!AppendPawn(moves[index].startPosition))
				{
					Debug.LogError("ERROR from BestMoveAI : The selected move is apparently not possible... (" + moves[index].startPosition + ")");
				}
			}

			else
			{
				Debug.LogError("ERROR from BestMoveAI : No moves available.");
			}
		}

	#endregion

	
	#region Accessors
	#endregion

}