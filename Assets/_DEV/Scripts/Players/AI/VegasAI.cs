﻿#region Headers

	using System.Collections.Generic;

	using UnityEngine;

	using MovementEffects;

#endregion


///<summary>
/// 
///		At each turn, this AI will randomly select a possible move.
/// 
///</summary>
[CreateAssetMenu(fileName = "VegasAI", menuName = "Othello - Player/Vegas AI", order = 750)]
public class VegasAI : PlayerAsset
{

	#region Attributes

		// Settings

		[Header("Settings")]

		[SerializeField]
		private float m_ThinkInterval = 1.0f;

	#endregion

	
	#region Public Methods

		protected override void OnBeginTurn()
		{
			Timing.RunCoroutine(DoTurn());
		}

	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods

		private IEnumerator<float> DoTurn()
		{
			yield return Timing.WaitForSeconds(m_ThinkInterval);

			BoardState board = GetBoardStateCopy();
			List<BoardMove> moves = board.FindPossibleMoves(PlayerSide);
			int index = Random.Range(0, moves.Count);

			if (!AppendPawn(moves[index].startPosition))
			{
				Debug.LogError("ERROR from VegasAI : The selected move is apparently not possible... (" + moves[index].startPosition + ")");
			}	
		}

	#endregion

	
	#region Accessors
	#endregion

}