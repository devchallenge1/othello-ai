﻿#region Headers

	using UnityEngine;

	using MuffinTools;

#endregion


///<summary>
/// 
///		Main class for handling game configurations such as :
///			- grid settings
///			- game prefabs
///			- player settings
///			- ...
///			
///		Note that this class is a Singleton. You can't create more than one GameConfiguration in the
///	project. But you can edit it and get the game settings everywhere in the code.
/// 
///</summary>

//[CreateAssetMenu(fileName = "NewGameConfiguration", menuName = "Othello - Settings/Game Configuration", order = 750)]
public class GameConfiguration : SOSingleton<GameConfiguration>
{


	#region Attributes

		// References

		[Header("References")]

			// General

		[SerializeField]
		private GameSettings m_GameSettings = null;

		[SerializeField]
		private GameResources m_GameResources = null;

		// Settings

		[Header("Settings")]

		[SerializeField]
		private string m_AIPlayersPath = "Resources/Players";

		[SerializeField]
		private string m_DefaultAssetsPath = "Resources/DefaultAssets";

	#endregion

	
	#region Accessors

		public static GameSettings GameSettings
		{
			get { return Instance.m_GameSettings; }
			set { Instance.m_GameSettings = value; }
		}

		public static GameResources GameResources
		{
			get { return Instance.m_GameResources; }
			set { Instance.m_GameResources = value; }
		}

		public string AIPlayersPath
		{
			get { return m_AIPlayersPath; }
		}

		public static PlayerAsset[] PossiblePlayers
		{
			get { return Resources.LoadAll<PlayerAsset>(Instance.m_AIPlayersPath); }
		}

		public static PlayerAsset DefaultPlayer
		{
			get { return Resources.Load<PlayerAsset>(Instance.m_DefaultAssetsPath); }
		}

		public static GameSettings DefaultSettings
		{
			get { return Resources.Load<GameSettings>(Instance.m_DefaultAssetsPath); }
		}

		public static GameResources DefaultResources
		{
			get { return Resources.Load<GameResources>(Instance.m_DefaultAssetsPath); }
		}

		public static BoardState DefaultBoard
		{
			get { return Resources.Load<BoardState>(Instance.m_DefaultAssetsPath); }
		}

	#endregion

}