﻿#region Headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

	using UnityEngine;
	//using UnityEngine.UI;
	
	//using MuffinTools;
	//using MovementEffects;

#endregion


///<summary>
/// 
///</summary>

[CreateAssetMenu(fileName = "NewGameResources", menuName = "Othello - Settings/Game Resources", order = 750)]
public class GameResources : ScriptableObject
{


	#region Attributes

		[SerializeField]
		private Player m_PlayerPrefab = null;

		[SerializeField]
		private Board m_BoardPrefab = null;

		[SerializeField]
		private Cell m_CellPrefab = null;

		[SerializeField]
		private Sprite m_PawnSprite = null;

	#endregion

	
	#region Accessors

		public Player PlayerPrefab
		{
			get { return m_PlayerPrefab; }
		}

		public Board BoardPrefab
		{
			get { return m_BoardPrefab; }
		}

		public Cell CellPrefab
		{
			get { return m_CellPrefab; }
		}

		public Sprite PawnSprite
		{
			get { return m_PawnSprite; }
		}

	#endregion

}