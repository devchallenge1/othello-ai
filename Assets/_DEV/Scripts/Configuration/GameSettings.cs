﻿#region Headers

	using UnityEngine;

#endregion


///<summary>
/// 
///		Contains all game settings, such as :
///			- player settings
///			- grid settings
///			- board settings
///			- ...
/// 
///</summary>

[CreateAssetMenu(fileName = "NewGameSettings", menuName = "Othello - Settings/Game Settings", order = 750)]
public class GameSettings : ScriptableObject
{

	#region Enums & Subclasses

		public enum EPlayerChoice
		{
			Player1,
			Player2,
			Random
		}

	#endregion


	#region Attributes

		// Settings

			// Players

		[Header("Players settings")]

		[SerializeField]
		private PlayerColor[] m_PlayerColors = { };

		[SerializeField]
		private PlayerAsset[] m_Players = { };

		[SerializeField]
		private EPlayerChoice m_FirstPlayer = EPlayerChoice.Random;

			// Board

		[Header("Board settings")]

		[SerializeField]
		private BoardState m_BoardInitialState = null;

	#endregion

	
	#region Accessors

		/// <summary>
		/// Gets the Player Color for the given player side.
		/// </summary>
		public Color GetPlayerColor(EPlayerSide _Side)
		{
			int count = m_PlayerColors.Length;
			for(int i = 0; i < count; i++)
			{
				if(m_PlayerColors[i].playerSide == _Side)
				{
					return m_PlayerColors[i].color;
				}
			}

			Debug.LogWarning("The Player Color can't be found for the given player side (" + OthelloHelpers.GetPlayerSideName(_Side) + "). Maybe the GameSettings asset in GameConfiguration is not correctly set.");
			return Color.white;
		}

		/// <summary>
		/// Gets the defined first player, or select one randomly.
		/// </summary>
		public EPlayerChoice GetFirstPlayer()
		{
			if (m_FirstPlayer == EPlayerChoice.Random)
			{
				int rnd = Random.Range(0, 2);
				return (rnd == 0) ? EPlayerChoice.Player1 : EPlayerChoice.Player2;
			}

			else
			{
				return m_FirstPlayer;
			}
		}

		public BoardState InitialBoardState
		{
			get { return m_BoardInitialState; }
		}

		public PlayerAsset[] Players
		{
			get { return m_Players; }
		}

		public void SetPlayer(int _Index, PlayerAsset _Asset, string _Name)
		{
			if(m_Players.Length < GameManager.c_NbPlayers)
			{
				m_Players = new PlayerAsset[2]
				{
					GameConfiguration.DefaultPlayer,
					GameConfiguration.DefaultPlayer
				};
			}

			if(_Index >= 0 && _Index < GameManager.c_NbPlayers)
			{
				m_Players[_Index] = _Asset;
				_Asset.PlayerName = _Name;
			}
		}

	#endregion

}