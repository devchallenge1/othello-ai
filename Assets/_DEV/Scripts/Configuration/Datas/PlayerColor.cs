﻿#region Headers

	using UnityEngine;

#endregion

[System.Serializable]
public struct PlayerColor
{

	#region Attributes

		public EPlayerSide playerSide;
		public Color color;

	#endregion

}