﻿#region Headers

	using UnityEngine;

	using MuffinTools;

#endregion

public class BoardCam : MonoBehaviour
{

	#region Attributes

		[Header("Settings")]

		[SerializeField]
		private float m_EdgeOffset = 1.0f;

	#endregion

	
	#region Engine Methods

		private void OnEnable()
		{
			EventsManager.StartListening(EventsList.c_BoardInit, OnBoardInit);
		}

		private void OnDisable()
		{
			EventsManager.StopListening(EventsList.c_BoardInit, OnBoardInit);
		}

	#endregion

	
	#region Public Methods
	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods

		private void OnBoardInit(object _Params)
		{
			Coords dimensions = (Coords)_Params;
			int val = Mathf.Max(dimensions.x, dimensions.y) / 2;

			GetComponent<Camera>().orthographicSize = val + m_EdgeOffset;
			transform.position = new Vector3(val - 0.5f, -(val - 0.5f), -10);
		}

	#endregion

	
	#region Accessors
	#endregion

}