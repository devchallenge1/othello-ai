﻿#region Headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

	//using UnityEngine;
	//using UnityEngine.UI;
	
	//using MuffinTools;
	//using MovementEffects;

	//[System.Serializable]

#endregion


///<summary>
/// 
///</summary>
public struct PlayerEventInfos
{

	#region Attributes

		// Settings

		public string playerName;
		public int playerScore;
		public int opponentScore;
		public EPlayerSide playerSide;

	#endregion

	
	#region Initialization / Destruction

		public PlayerEventInfos(EPlayerSide _PlayerSide, string _PlayerName = null, int _PlayerScore = 0, int _OpponentScore = 0)
		{
			playerName		= _PlayerName;
			playerScore		= _PlayerScore;
			playerSide		= _PlayerSide;
			opponentScore	= _OpponentScore;
		}

	#endregion

}