﻿///<summary>
/// 
///		Contains constants for all the Game Events.
/// 
///</summary>
public class EventsList
{

	#region Gameplay loop

		/**
		 *	Triggerd by GameManager when the game starts.
		 */
		public const string c_GameStart = "Gameplay_GameStart";

		/**
		 *	Triggered by GameManager when the game is about to end (no possible moves
		 *	for both players, but winner not already chosen).
		 */
		public const string c_GameEnd = "Gameplay_GameEnd";

		/**
		 *	Triggered by GameManager when the game ended and the winner is found.
		 *	@param BoardState The board state of the winner, or null if ex aequo.
		 */
		public const string c_GameWon = "Gameplay_GameWon";

		/**
		 *	Triggered by GameManager when a Player's turn starts.
		 *	@params EPlayerSide
		 */
		public const string c_GameBeginTurn = "Gameplay_BeginTurn";

		/**
		 *	Triggered by GameManager when a Player's turn ends.
		 */
		public const string c_GameEndTurn = "Gameplay_EndTurn";

		/**
		 *	Triggered by GameManager when the next turn starts wait to begin.
		 */
		public const string c_GameRequireNextTurn = "Gameplay_RequireNextTurn";

	#endregion


	#region Players

		/**
		 *	Triggered by Board when a Player has successfully appened a pawn
		 *	@param PlayerEventInfos
		 */
		public const string c_PlayerMoveSuccess = "Player_MoveSuccess";

		/**
		 *	Triggered by GameManager when a Player has been initialized
		 *	for both players, but winner not already chosen).
		 *	@param PlayerEventInfos
		 */
		public const string c_PlayerInit = "Player_Init";

	#endregion


	#region Board

		/**
		 *	Triggered by GameManager when the board has just been initialized
		 *	@param Coords Contains dimensions of the board (x = width, y = height)
		 */
		public const string c_BoardInit = "Board_Init";

	#endregion

}