﻿#region Headers

	using UnityEngine;

	using MuffinTools;

#endregion

///<summary>
/// 
///		Log infos in console for each triggered game event.
/// 
///</summary>
public class GameEventsLogger : MonoBehaviour
{

	#region Attributes
	#endregion

	
	#region Engine Methods

		private void OnEnable()
		{
			EventsManager.StartListening(EventsList.c_GameStart, OnGameStart);
			EventsManager.StartListening(EventsList.c_GameEnd, OnGameEnd);
			EventsManager.StartListening(EventsList.c_GameWon, OnGameWon);
		}

		private void OnDisable()
		{
			EventsManager.StopListening(EventsList.c_GameStart, OnGameStart);
			EventsManager.StopListening(EventsList.c_GameEnd, OnGameEnd);
			EventsManager.StopListening(EventsList.c_GameWon, OnGameWon);
		}

	#endregion

	
	#region Private Methods

		private void OnGameStart(object _Params)
		{
			Debug.Log("GAME EVENT : Game started");
		}

		private void OnGameEnd(object _Params)
		{
			Debug.Log("GAME EVENT : Game ended");
		}

		private void OnGameWon(object _WinnerInfos)
		{
			WinnerInfos infos = _WinnerInfos as WinnerInfos;
			if(infos == null)
			{
				Debug.Log("GAME EVENT : Ex aequo");
			}

			else
			{
				Debug.Log("GAME EVENT : Game won by " + infos.PlayerName);
			}
		}

	#endregion

}