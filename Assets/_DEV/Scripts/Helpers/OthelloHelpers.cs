﻿#region Headers

	using System;
	using System.Collections.Generic;

	using UnityEngine;

#endregion

///<summary>
/// 
///		Some helper methods for the game.
/// 
///</summary>
public static class OthelloHelpers
{

	#region Accessors

		/// <summary>
		/// Get player side as a name instead of an enum value
		/// </summary>
		public static string GetPlayerSideName(EPlayerSide _Side)
		{
			return Enum.GetName(typeof(EPlayerSide), _Side);
		}

	#endregion


	#region Debug & Tests

		public static void DebugBoardMovesList(List<BoardMove> _Moves)
		{
			int count = _Moves.Count;
			for (int i = 0; i < count; i++)
			{
				Debug.Log(GetPlayerSideName(_Moves[i].playerSide) + " possible moves [" + i + "] : " + _Moves[i].startPosition + " / score = " + _Moves[i].positions.Count);
				for (int j = 0; j < _Moves[i].positions.Count; j++)
				{
					Debug.Log("\t[" + j + "] : " + _Moves[i].positions[j]);
				}
				Debug.Log("___________________________________________________\n");
			}
		}

	#endregion

}