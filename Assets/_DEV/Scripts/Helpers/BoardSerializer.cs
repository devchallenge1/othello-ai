﻿#region Headers

	using System;

	using UnityEngine;

#endregion


///<summary>
/// 
///		Helper to serialize and deserialize a BoardState's grid.
/// 
///</summary>
public class BoardSerializer
{

	#region Enums & Subclasses
		
		/// <summary>
		/// Represents the association between a character (for serialized board strings) and
		/// a CellState.
		/// </summary>
		private struct CellStateSerializationRule
		{
			public char character;
			public CellState cellState;

			public CellStateSerializationRule(char _Char, CellState _State)
			{
				character = _Char;
				cellState = _State;
			}
		}

		/// <summary>
		/// Represents the deserialization result, containing info about width and height of
		/// the deserialized grid.
		/// </summary>
		public class BoardDeserializationResult
		{
			private int m_Width = 0;
			private int m_Height = 0;
			private CellState[,] m_Grid = null;

			public BoardDeserializationResult(int _Width, int _Height, CellState[,] _Grid)
			{
				m_Width		= _Width;
				m_Height	= _Height;
				m_Grid		= _Grid;
			}

			public int			Width	{ get { return m_Width; } }
			public int			Height	{ get { return m_Height; } }
			public CellState[,] Grid	{ get { return m_Grid; } }
		}

	#endregion


	#region Attributes

		private const char c_Empty = 'E';
		private const char c_Black = 'B';
		private const char c_White = 'W';

		private static readonly CellStateSerializationRule[] s_SerializationRules;

	#endregion

	
	#region Initialization / Destruction

		static BoardSerializer()
		{
			s_SerializationRules = new CellStateSerializationRule[4]
			{
				new CellStateSerializationRule(c_Empty, CellState.Empty),
				new CellStateSerializationRule(c_Empty, CellState.Empty2),
				new CellStateSerializationRule(c_Black, CellState.Black),
				new CellStateSerializationRule(c_White, CellState.White)
			};
		}

	#endregion

	
	#region Public Methods

		/// <summary>
		/// Returns a serialized empty board with given dimensions.
		/// </summary>
		public string SerializeEmptyBoard(int _Width, int _Height)
		{
			char emptyCellChar = GetCharForCellState(CellState.Empty);
			string serializedBoard = string.Empty;
			for(int y = 0; y < _Height; y++)
			{
				for(int x = 0; x < _Width; x++)
				{
					serializedBoard += emptyCellChar;
				}

				if(y < _Height - 1)
				{
					serializedBoard += Environment.NewLine;
				}
			}

			return serializedBoard;
		}

		public string SerializeBoard(CellState[,] _Grid)
		{
			int width = _Grid.GetLength(0);
			int height = _Grid.GetLength(1);

			string serializedBoard = string.Empty;
			for(int y = 0; y < height; y++)
			{
				for(int x = 0; x < width; x++)
				{
					serializedBoard += GetCharForCellState(_Grid[x, y]);
				}

				if(y < height - 1)
				{
					serializedBoard += Environment.NewLine;
				}
			}

			return serializedBoard;
		}

		/// <summary>
		/// Deserialize a given serialized board string.
		/// </summary>
		/// <returns>Returns null if the given string doens't represent a rectangular board, or if it's empty.</returns>
		public BoardDeserializationResult DeserializeBoard(string _SerializedBoard)
		{
			string[] lines = _SerializedBoard.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
			int height = lines.Length;

			if(height == 0) 
			{
				Debug.LogWarning("The given serialized board is empty");
				return null;
			}

			int width = lines[0].Length;
			// Check that the given string array is rectangular
			for(int i = 0; i < height; i++)
			{
				if(lines[i].Length != width)
				{
					Debug.LogWarning("The given serialized board must be rectangular (one (or more) of the lines is shorter or longer than the others)");
					return null;
				}
			}

			// Make the grid
			CellState[,] grid = new CellState[width, height];
			for(int y = 0; y < height; y++)
			{
				for(int x = 0; x < width; x++)
				{
					grid[x, y] = GetCellStateForChar(lines[y][x]);
				}
			}

			return new BoardDeserializationResult(width, height, grid);
		}

	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods
	#endregion

	
	#region Accessors

		/// <summary>
		/// Get the CellState in serialization rules array relative to the given character.
		/// </summary>
		/// <returns>Returns CellState.Empty if no binding has been found.</returns>
		private CellState GetCellStateForChar(char _Char)
		{
			int count = s_SerializationRules.Length;
			for(int i = 0; i < count; i++)
			{
				if(s_SerializationRules[i].character == _Char)
				{
					return s_SerializationRules[i].cellState;
				}
			}

			Debug.Log("No relative CellState for character '" + _Char + "'");
			return CellState.Empty;
		}

		/// <summary>
		/// Get the character in serialization rules array relative to the given CellState.
		/// </summary>
		/// <returns>Returns 'E' if no binding has been found.</returns>
		private char GetCharForCellState(CellState _CellState)
		{
			int count = s_SerializationRules.Length;
			for (int i = 0; i < count; i++)
			{
				if (s_SerializationRules[i].cellState == _CellState)
				{
					return s_SerializationRules[i].character;
				}
			}

			Debug.Log("No relative character for " + _CellState);
			return c_Empty;
		}

	#endregion

}