﻿#region Headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

	using UnityEngine;
	//using UnityEngine.UI;

	//using MuffinTools;
	//using MovementEffects;

	//[AddComponentMenu("Scripts/Cell")]
	//[RequireComponent(typeof())]
	using DG.Tweening;

#endregion


///<summary>
/// 
///</summary>
public class Cell : MonoBehaviour
{

    #region Attributes

		// Reference

		[Header("References")]

		[SerializeField]
		private SpriteRenderer m_PawnRenderer = null;

        private Animator m_PawnAnimator = null;

		// Settings

		[SerializeField, Min(0.0f)]
		private float m_SpriteAnimationDuration = 0.5f;

        // Flow

		private CellState m_CellState = CellState.Empty;
		private Coords m_Coords = Coords.zero;

    #endregion


    #region Engine Methods

        private void Awake()
        {
            m_PawnAnimator = (m_PawnRenderer != null) ? m_PawnRenderer.gameObject.GetComponent<Animator>() : null;
        }

    #endregion


    #region Public Methods

		/// <summary>
		/// Updates the renderer of this Cell using the given CellState.
		/// </summary>
        public void UpdateState(CellState _CellState)
        {
            if
			(
				_CellState.isFree != m_CellState.isFree ||
				_CellState.playerSide != m_CellState.playerSide
			)
			{
                CellState lastState = m_CellState;
				m_CellState = _CellState;
				ApplyState(lastState, m_CellState);
			}
        }

	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods

		/// <summary>
		/// Applies this Cell style for the given CellState.
		/// </summary>
		private void ApplyState(CellState _FromState, CellState _ToState)
		{
			if(m_PawnRenderer != null)
			{
				if(_ToState.isFree)
				{
					m_PawnRenderer.sprite = null;
				}

				else
				{
					m_PawnRenderer.sprite = GetPawnSprite();
					m_PawnRenderer.DOColor(GetPlayerColor(m_CellState.playerSide), m_SpriteAnimationDuration);

                    if (m_PawnAnimator != null)
                    {
                        string trigger = (_FromState.isFree) ? "Spawn" : "Bounce";
                        m_PawnAnimator.SetTrigger(trigger);
                    }
				}
			}

			else
			{
				Debug.LogWarning("You must set a SpriteRenderer for Pawns on Cell components");
			}
		}

	#endregion

	
	#region Accessors

		public Coords Coords
		{
			get { return m_Coords; }
			set
			{
				m_Coords = value;
				name += " (" + m_Coords.x + "; " + m_Coords.y + ")";
			}
		}

		private Sprite GetPawnSprite()
		{
			try
			{
				return GameConfiguration.GameResources.PawnSprite;
			}
			catch
			{
				Debug.LogError("The Pawn sprite has not been set in GameResources in the GameConfiguration asset");
				return null;
			}
		}

		private Color GetPlayerColor(EPlayerSide _PlayerSide)
		{
			try
			{
				return GameConfiguration.GameSettings.GetPlayerColor(_PlayerSide);
			}
			catch
			{
				Debug.LogError("The GameSettings asset has not been assigned to the GameConfiguration asset");
				return Color.white;
			}
		}

	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}