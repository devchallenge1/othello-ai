﻿#region Headers

	using System.Collections.Generic;

	using UnityEngine;

#endregion


///<summary>
/// 
///</summary>

[CreateAssetMenu(fileName = "NewBoardState", menuName = "Othello - Assets/Board State", order = 750)]
public class BoardState : ScriptableObject, IBoard
{

	#region Attributes

		// Constants

		public const int c_MinBoardDimension = 2;
		public const int c_MaxBoardDimension = 1000;
		public const int c_DefaultBoardDimension = 8;

		// Delegates

		public delegate void SetCellStateCallback(Coords _Position, CellState _NewCellState);
		
		// Settings

		[SerializeField, TextArea(8, 1000)]
		private string m_SerializedGrid = string.Empty;

		// Flow

		private CellState[,] m_Grid = null;

		public SetCellStateCallback OnSetCellState;

		// Cache

		private int m_Width		= 0;
		private int m_Height	= 0;

	#endregion

	
	#region Engine Methods

		/// <summary>
		/// On asset initialization, make the grid.
		/// </summary>
		private void OnEnable()
		{
			if(string.IsNullOrEmpty(m_SerializedGrid))
			{
				m_SerializedGrid = MakeEmptySerializedGrid(c_DefaultBoardDimension, c_DefaultBoardDimension);
			}

			if(m_Grid == null)
			{
				InitGrid(m_SerializedGrid);
			}
		}

	#endregion

	
	#region Public Methods

		/// <summary>
		/// Check a player can append a pawn at the given position :
		///		- The position must be in grid rang
		///		- The move must be possible
		/// </summary>
		/// <returns>Returns true if the pawn can be appended, otherwise false.</returns>
		public bool CanAppendPawn(int _X, int _Y, EPlayerSide _PlayerSide)
		{
			return (HasPossibleMoveAt(_X, _Y, _PlayerSide));
		}

		/// <summary>
		/// If the given position is a free cell, append the pawn.
		/// IMPORTANT : This method will not check if the move is possible. The pawn will
		/// be appended and the move will be resolved, even if no score is made.
		/// Call CanAppendPawn() before to check if the move is possible.
		/// </summary>
		/// <returns>Returns true if the new pawn has been successfully appended,
		/// otherwise false.</returns>
		public bool AppendPawn(int _X, int _Y, EPlayerSide _PlayerSide)
		{
			if(!IsCellAvailable(_X, _Y))
			{
				return false;
			}

			List<BoardMove> moves = BuildMovesFrom(_X, _Y, _PlayerSide);
			//OthelloHelpers.DebugBoardMovesList(moves);
			ResolveMoves(moves);

			return true;
		}

		/// <summary>
		/// Count all pawns on the board.
		/// </summary>
		public int CountPawns()
		{
			int pawns = 0;
			for(int x = 0; x < m_Width; x++)
			{
				for(int y = 0; y < m_Height; y++)
				{
					if(!m_Grid[x, y].isFree)
					{
						pawns++;
					}
				}
			}
			return pawns;
		}

		/// <summary>
		/// Count all pawns of the given player on the board.
		/// </summary>
		public int CountPawns(EPlayerSide _PlayerSide)
		{
			int pawns = 0;
			for (int x = 0; x < m_Width; x++)
			{
				for (int y = 0; y < m_Height; y++)
				{
					if (!m_Grid[x, y].isFree && m_Grid[x, y].playerSide == _PlayerSide)
					{
						pawns++;
					}
				}
			}
			return pawns;
		}

		/// <summary>
		/// Run the grid to get all the possible moves for the given player.
		/// </summary>
		public List<BoardMove> FindPossibleMoves(EPlayerSide _PlayerSide)
		{
			List<BoardMove> allMoves = new List<BoardMove>();
			for(int x = 0; x < m_Width; x++)
			{
				for(int y = 0; y < m_Height; y++)
				{
					List<BoardMove> moves = BuildMovesFrom(x, y, _PlayerSide);
					int count = moves.Count;
					for(int i = 0; i < count; i++)
					{
						allMoves.Add(moves[i]);
					}
				}
			}

			return allMoves;
		}

		/// <summary>
		/// Check if the given player has a possible moves.
		/// NOTE : this method will stop when the first possible move for the given
		/// player is found. So you better use this method for a simple check instead
		/// of using GetPossibleMoves() and count results.
		/// </summary>
		public bool HasPossibleMoves(EPlayerSide _PlayerSide)
		{
			for(int x = 0; x < m_Width; x++)
			{
				for(int y = 0; y < m_Height; y++)
				{
					if(BuildMovesFrom(x, y, _PlayerSide).Count > 0)
					{
						return true;
					}
				}
			}
			return false;
		}

		/// <summary>
		/// Check if the move is possible at the given position.
		/// </summary>
		public bool HasPossibleMoveAt(int _X, int _Y, EPlayerSide _Player)
		{
			// If the given position is out of range or not free
			if(!IsCellAvailable(_X, _Y))
			{
				return false;
			}

			Coords start = new Coords(_X, _Y);

			// For each cell around the start position
			int count = Coords.around.Length;
			for(int i = 0; i < count; i++)
			{
				BoardMove move = new BoardMove(_Player, start);
				// The direction where to run the grid
				Coords runVector = Coords.around[i];
				Coords currentPosition = start + runVector;

				int test = 0;

				// While the current position is in range and a free
				while (IsInRange(currentPosition) && test < 50)
				{
					// If the cell is free, the move is not possible
					if (m_Grid[currentPosition.x, currentPosition.y].isFree)
					{
						test++;
						break;
					}

					// If the current cell is occupied with an opponent pawn
					else if (m_Grid[currentPosition.x, currentPosition.y].playerSide != _Player)
					{
						move.positions.Add(currentPosition);
						currentPosition += runVector;
						test++;
					}

					// Else, if the current cell is occupied with this player's pawn
					else
					{
						// If the move has for score at least 1, a move is possible
						if(move.positions.Count > 0)
						{
							return true;
						}
						break;
					}
				}
			}

			return false;
		}

	#endregion


	#region Private Methods

		/// <summary>
		/// Makes a serialized board string with the given dimensions.
		/// </summary>
		private string MakeEmptySerializedGrid(int _Width, int _Height)
		{
			BoardSerializer serializer = new BoardSerializer();
			return serializer.SerializeEmptyBoard(_Width, _Height);
		}

		/// <summary>
		/// Initialized this BoardState grid from the given serialized board string.
		/// </summary>
		private void InitGrid(string _SerializedGrid)
		{
			BoardSerializer serializer = new BoardSerializer();
			BoardSerializer.BoardDeserializationResult result = serializer.DeserializeBoard(m_SerializedGrid);

			if(result != null)
			{
				if(AreDimensionsValid(result.Width, result.Height))
				{
					m_Width		= result.Width;
					m_Height	= result.Height;
					m_Grid		= result.Grid;
				}
				
				else
				{
					Debug.LogWarning("The deserialized board is smaller or bigger than min/max dimensions (" + c_MinBoardDimension + " / " + c_MaxBoardDimension + "). The Grid hasn't been initialized");
				}
			}
			else
			{
				Debug.LogError("Board deserialization failed (" + name + ")");
			}
		}

		/// <summary>
		/// Build BoardMove list from the given position on the grid.
		/// </summary>
		private List<BoardMove> BuildMovesFrom(int _X, int _Y, EPlayerSide _Player)
		{
			List<BoardMove> moves = new List<BoardMove>();

			// If the given position is out of range or not free
			if (!IsCellAvailable(_X, _Y))
			{
				return moves;
			}

			Coords start = new Coords(_X, _Y);

			// For each cell around the start position
			int count = Coords.around.Length;
			for (int i = 0; i < count; i++)
			{
				BoardMove move = new BoardMove(_Player, start);
				// The direction where to run the grid
				Coords runVector = Coords.around[i];
				Coords currentPosition = start + runVector;

				// While the current position is in range and a free
				while (IsInRange(currentPosition))
				{
					// If the cell is free, the move is not possible
					if (m_Grid[currentPosition.x, currentPosition.y].isFree)
					{
						break;
					}

					// If the current cell is occupied with an opponent pawn
					else if (m_Grid[currentPosition.x, currentPosition.y].playerSide != _Player)
					{
						move.positions.Add(currentPosition);
						currentPosition += runVector;
					}

					// Else, if the current cell is occupied with this player's pawn
					else
					{
						// If this move has a score of at least 1, it's possible
						if (move.positions.Count > 0)
						{
							moves.Add(move);
						}
						break;
					}
				}
			}

			return moves;
		}

		private void ResolveMoves(List<BoardMove> _Moves)
		{
			int count = _Moves.Count;
			if (count > 0)
			{
				CellState playerCellState = CellState.GetCellStateForPlayer(_Moves[0].playerSide);
				SetCellState(_Moves[0].startPosition, playerCellState);

				for(int i = 0; i < count; i++)
				{
					int stepsCount = _Moves[i].positions.Count;
					for(int j = 0; j < stepsCount; j++)
					{
						SetCellState(_Moves[i].positions[j], playerCellState);
					}
				}
			}
		}

	#endregion

	
	#region Accessors

		/// <summary>
		/// Set the state of the cell at the given position, and reserialize board.
		/// </summary>
		public bool SetCellState(int _X, int _Y, CellState _CellState)
		{
			if(IsInRange(_X, _Y))
			{
				m_Grid[_X, _Y] = _CellState;

				BoardSerializer serializer = new BoardSerializer();
				m_SerializedGrid = serializer.SerializeBoard(m_Grid);
				return true;
			}

			return false;
		}

		/// <summary>
		/// Set the state of the cell at the given position, and reserialize board.
		/// </summary>
		public bool SetCellState(Coords _Position, CellState _CellState)
		{
			if (IsInRange(_Position))
			{
				// If the last cell state is different from the given one
				if (_CellState != m_Grid[_Position.x, _Position.y])
				{
					m_Grid[_Position.x, _Position.y] = _CellState;

					// Reserialize grid
					BoardSerializer serializer = new BoardSerializer();
					m_SerializedGrid = serializer.SerializeBoard(m_Grid);

					if(OnSetCellState != null)
					{
						OnSetCellState(_Position, _CellState);
					}
				}
			}

			return false;
		}

		public bool IsCellAvailable(int _X, int _Y)
		{
			return (IsInRange(_X, _Y) && this[_X, _Y].isFree);
		}

		public bool IsCellAvailable(Coords _Coords)
		{
			return IsCellAvailable(_Coords.x, _Coords.y);
		}

		public CellState this[int _X, int _Y]
		{
			get { return m_Grid[_X, _Y]; }
		}

		private CellState this[Coords _Coords]
		{
			get { return this[_Coords.x, _Coords.y]; }
		}

		/// <summary>
		/// Checks if the given coordinates are in this BoardState's grid range.
		/// </summary>
		public bool IsInRange(int _X, int _Y)
		{
			return
			(
				_X >= 0 && _X < m_Width &&
				_Y >= 0 && _Y < m_Height
			);
		}

		/// <summary>
		/// Checks if the given coordinates are in this BoardState's grid range.
		/// </summary>
		public bool IsInRange(Coords _Position)
		{
			return IsInRange(_Position.x, _Position.y);
		}

		/// <summary>
		/// Checks if given dimensions fit the min and max possible values.
		/// </summary>
		public bool AreDimensionsValid(int _Width, int _Height)
		{
			return
			(
				_Width >= c_MinBoardDimension && _Width <= c_MaxBoardDimension &&
				_Height >= c_MinBoardDimension && _Height <= c_MaxBoardDimension
			);
		}

		public int Width
		{
			get { return m_Width; }
		}

		public int Height
		{
			get { return m_Height; }
		}

	#endregion


	#region Debug & Editor

		#if UNITY_EDITOR

		public string GetSerializedBoard()
		{
			return m_SerializedGrid;
		}

		/// <summary>
		/// Resizes the grid, adding empty cells if necessary, and the update the serialized grid.
		/// </summary>
		public bool Resize(int _Width, int _Height)
		{
			if(!AreDimensionsValid(_Width, _Height))
			{
				Debug.LogWarning("The gievn dimensions are smaller or bigger than min/max dimensions (" + c_MinBoardDimension + " / " + c_MaxBoardDimension + ", given " + _Width + " / " + _Height + "). The Grid can't be resized");
				return false;
			}

			int lastWidth = m_Width;
			int lastHeight = m_Height;

			m_Width = _Width;
			m_Height = _Height;

			// Make new grid using the current grid cells, of new empty cells if we're extending the grid.
			CellState[,] newGrid = new CellState[m_Width, m_Height];
			for(int x = 0; x < m_Width; x++)
			{
				for(int y = 0; y < m_Height; y++)
				{
					if(x < lastWidth && y < lastHeight)
					{
						newGrid[x, y] = m_Grid[x, y];
					}
					else
					{
						newGrid[x, y] = CellState.Empty;
					}
				}
			}

			m_Grid = newGrid;

			// Re-serialize the grid and store the serialized string.
			BoardSerializer serializer = new BoardSerializer();
			m_SerializedGrid = serializer.SerializeBoard(m_Grid);

			return true;
		}

		public CellState[,] GetGrid()
		{
			return m_Grid;
		}

		#endif

	#endregion

}