﻿///<summary>
/// 
///		Represents the state of a Board's cell.
/// 
///</summary>

[System.Serializable]
public struct CellState
{

	#region Attributes

		// Statics

		public static readonly CellState Empty = new CellState(true, EPlayerSide.Black);
		public static readonly CellState Empty2 = new CellState(true, EPlayerSide.White);
		public static readonly CellState Black = new CellState(false, EPlayerSide.Black);
		public static readonly CellState White = new CellState(false, EPlayerSide.White);

		// Settings

		public bool isFree;
		public EPlayerSide playerSide;

	#endregion


	#region Initialization

		public CellState(bool _IsFree, EPlayerSide _PlayerSide)
		{
			isFree		= _IsFree;
			playerSide	= _PlayerSide;
		}

	#endregion


	#region Public methods

		public override string ToString()
		{
			string state = "free";
			if(!isFree)
			{
				state = (playerSide == EPlayerSide.Black) ? "black" : "white";
			}

			return "[CellState : " + state + "]";
		}

	#endregion


	#region Accessors
		
		public static CellState GetCellStateForPlayer(EPlayerSide _PlayerSide)
		{
			return (_PlayerSide == EPlayerSide.Black) ? CellState.Black : CellState.White;
		}

	#endregion


	#region Operators

		public bool Equals(CellState _Other)
		{
			return
			(
				isFree == _Other.isFree &&
				playerSide == _Other.playerSide
			);
		}

		public override int GetHashCode()
		{
			return ((isFree) ? 1 : 0) ^ playerSide.GetHashCode();
		}

		public override bool Equals(object _Other)
		{
			return Equals((CellState)_Other);
		}

		public static bool operator ==(CellState _A, CellState _B)
		{
			return _A.Equals(_B);
		}

		public static bool operator !=(CellState _A, CellState _B)
		{
			return !_A.Equals(_B);
		}

	#endregion

}