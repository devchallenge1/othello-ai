﻿#region Headers

	using System.Collections.Generic;

	using UnityEngine;

	using MuffinTools;
	using MovementEffects;

#endregion


///<summary>
/// 
///</summary>
public class Board : MonoBehaviour, IBoard
{

	#region Attributes

        // Constants & Statics

        private const float c_CellWidth = 1.0f;
        private const float c_CellHeight = 1.0f;

		// References

		private BoardState m_BoardState = null;

		// Settings

		[Header("Animation settings")]

		[SerializeField, Min(0.0f)]
		private float m_AnimationDecay = 0.2f;

        // Flow

        private Cell[,] m_Cells = null;
		private int m_StepsLeft = 0;

	#endregion

	
	#region Engine Methods
	#endregion

	
	#region Public Methods

		public bool CanAppendPawn(int _X, int _Y, EPlayerSide _PlayerSide)
		{
			return m_BoardState.CanAppendPawn(_X, _Y, _PlayerSide);
		}

		public bool AppendPawn(int _X, int _Y, EPlayerSide _PlayerSide)
		{
			if (CanAppendPawn(_X, _Y, _PlayerSide))
			{
				if (m_BoardState.AppendPawn(_X, _Y, _PlayerSide))
				{
					EPlayerSide opponent = (_PlayerSide == EPlayerSide.Black) ? EPlayerSide.White : EPlayerSide.Black;
					EventsManager.TriggerEvent(EventsList.c_PlayerMoveSuccess, new PlayerEventInfos(_PlayerSide, null, CountPawns(_PlayerSide), CountPawns(opponent)));

					return true;
				}
			}
			return false;
		}

		public int CountPawns()
		{
			return m_BoardState.CountPawns();
		}

		public int CountPawns(EPlayerSide _PlayerSide)
		{
			return m_BoardState.CountPawns(_PlayerSide);
		}

		public List<BoardMove> FindPossibleMoves(EPlayerSide _PlayerSide)
		{
			return m_BoardState.FindPossibleMoves(_PlayerSide);
		}

		public bool HasPossibleMoves(EPlayerSide _PlayerSide)
		{
			return m_BoardState.HasPossibleMoves(_PlayerSide);
		}

		public void SetCellStateRender(Coords _Coords, CellState _NewCellState)
		{
			Timing.RunCoroutine(DecayCellUpdateState(m_StepsLeft * m_AnimationDecay, _Coords, _NewCellState));
			m_StepsLeft++;
		}

		private IEnumerator<float> DecayCellUpdateState(float _Decay, Coords _Coords, CellState _CellState)
		{
			yield return Timing.WaitForSeconds(_Decay);
			m_Cells[_Coords.x, _Coords.y].UpdateState(_CellState);
			m_StepsLeft--;
		}

	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods

		/// <summary>
		/// Sets the BoardState if it's not already initialized, or log an error.
		/// </summary>
		public bool InitBoardState(BoardState _BoardState)
		{
            if (_BoardState == null)
            {
                Debug.LogError("The given BoardState is null");
                return false;
            }

			if (m_BoardState == null)
			{
				m_BoardState = Object.Instantiate<BoardState>(_BoardState);
                OnInitBoardState();
				m_BoardState.OnSetCellState += SetCellStateRender;

				return true;
			}

			else
			{
				Debug.LogError("The BoardState of the is already assign... Did someone wanted to cheat ? :)");
			}

			return false;
		}

        /// <summary>
        /// Spawns and intializes this Board's Cells.
        /// </summary>
        /// <param name="_BoardState">Assuming the given BoardState is valid.</param>
        private void InitGrid(BoardState _BoardState)
        {
            int width = _BoardState.Width;
            int height = _BoardState.Height;

            Cell prefab = GetCellPrefab();
            if (prefab == null)
            {
                return;
            }

            m_Cells = new Cell[width, height];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
					m_Cells[x, y] = SpawnCell(prefab, _BoardState[x, y], x, y);
					SetCellStateRender(x, y, _BoardState[x, y]);
                }
            }
        }

        private void OnInitBoardState()
        {
            InitGrid(m_BoardState);
        }

        /// <summary>
        /// Spawn and initialize a Cell based on the given state, at the given position
        /// </summary>
        /// <param name="_Prefab">Assuming the prefab is valid.</param>
        private Cell SpawnCell(Cell _Prefab, CellState _State, int _X, int _Y)
        {
            Vector3 pos = transform.position;
            pos.x += (_X * c_CellWidth);
            pos.y -= (_Y * c_CellHeight);

            GameObject obj = Instantiate(_Prefab.gameObject, pos, Quaternion.identity, transform);
            Cell cell = obj.GetComponent<Cell>();

            cell.name = "Cell";
			cell.Coords = new Coords(_X, _Y);

            return cell;
        }

		public void SetCellStateRender(int _X, int _Y, CellState _NewCellState)
		{
			m_Cells[_X, _Y].UpdateState(_NewCellState);
		}

	#endregion

	
	#region Accessors

		public BoardState GetBoardStateCopy()
		{
			if(m_BoardState != null)
			{
				return Object.Instantiate<BoardState>(m_BoardState);
			}
			return null;
		}

        private Cell GetCellPrefab()
        {
            try
            {
                return GameConfiguration.GameResources.CellPrefab;
            }
            catch
            {
                Debug.LogError("The Cell prefab has not been set in GameResources asset in the GameConfiguration asset (or maybe it doesn't have Cell component).");
				return null;
            }
        }

		public Coords GetDimensionsAsCoords()
		{
			return new Coords(m_BoardState.Width, m_BoardState.Height);
		}

	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}