﻿#region Headers

	using System.Collections.Generic;

#endregion

///<summary>
/// 
///		Represents a Player's move on the board.
/// 
///</summary>
public struct BoardMove
{

	#region Attributes

		// Values
		
		public EPlayerSide playerSide;
		public List<Coords> positions;
		public Coords startPosition;

	#endregion

	
	#region Initialization / Destruction

		public BoardMove(EPlayerSide _Player, Coords _StartPosition)
		{
			playerSide		= _Player;
			startPosition	= _StartPosition;
			positions		= new List<Coords>();
		}

	#endregion

}