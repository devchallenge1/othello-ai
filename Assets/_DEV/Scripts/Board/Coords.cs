﻿#region Headers

	using UnityEngine;

#endregion

[System.Serializable]
public struct Coords
{

	#region Attributes

		// Constants & Statics

		public static readonly Coords zero      = new Coords(0, 0);
        public static readonly Coords up        = new Coords(0, -1);
        public static readonly Coords down      = new Coords(0, 1);
        public static readonly Coords left      = new Coords(-1, 0);
        public static readonly Coords right     = new Coords(1, 0);
        public static readonly Coords upLeft    = new Coords(-1, -1);
        public static readonly Coords upRight   = new Coords(1, -1);
        public static readonly Coords downLeft  = new Coords(-1, 1);
        public static readonly Coords downRight = new Coords(1, 1);

		public static readonly Coords[] around = { };

		// Values

		public int x;
		public int y;

	#endregion


	#region Initialization

		static Coords()
		{
			around = new Coords[8]
			{
				Coords.upLeft,		Coords.up,
				Coords.upRight,		Coords.right,
				Coords.downRight,	Coords.down,
				Coords.downLeft,	Coords.left
			};
		}
		
		public Coords(int _X, int _Y)
		{
			x = _X;
			y = _Y;
		}

    #endregion


	#region Public methods

		public override string ToString()
		{
			return "Coords(" + x + "; " + y + ")";
		}

	#endregion


    #region Accessors

        /// <summary>
        /// Normalize the coords to values of 1, -1 or 0.
        /// </summary>
        public Coords Normalized
        {
            get
            {
                Coords coords = Coords.zero;
                if (x != 0) { coords.x = (x > 0) ? 1 : -1; }
                if (y != 0) { coords.y = (y > 0) ? 1 : -1; }
                return coords;
            }
        }

        /// <summary>
        /// Returns the opposite coords direction.
        /// </summary>
        public Coords Opposite
        {
            get
            {
                Coords normalized = Normalized;
                normalized.x *= -1;
                normalized.y *= -1;
                return normalized;
            }
        }

    #endregion


    #region Operators

        public static Coords operator +(Coords _A, Coords _B)
        {
            return new Coords(_A.x + _B.x, _A.y + _B.y);
        }

        public static Coords operator -(Coords _A, Coords _B)
        {
            return new Coords(_A.x - _B.x, _A.y - _B.y);
        }

    #endregion


}