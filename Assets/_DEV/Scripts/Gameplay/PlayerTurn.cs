﻿///<summary>
/// 
///		This class contains all informations and useful method for a Player to analyze the board and
///	make a move.
/// 
///</summary>
public sealed class PlayerTurn
{

	#region Attributes

		private GameManager m_GameManager	= null;
		private Board		m_Board			= null;
		private BoardPlayer m_CurrentPlayer	= null;

		private bool		m_IsTurnActive	= false;

	#endregion

	
	#region Initialization / Destruction

		public PlayerTurn(GameManager _GameManager, Board _Board, BoardPlayer _CurrentPlayer)
		{
			m_GameManager	= _GameManager;
			m_Board			= _Board;
			m_CurrentPlayer	= _CurrentPlayer;
			m_IsTurnActive	= true;
		}

	#endregion

	
	#region Public Methods

		/// <summary>
		/// Add a new pawn on the board at the given position.
		/// </summary>
		public bool AppendPawn(Coords _Position)
		{
			return AppendPawn(_Position.x, _Position.y);
		}

		public bool AppendPawn(int _X, int _Y)
		{
			if (IsTurnActive && m_Board.AppendPawn(_X, _Y, m_CurrentPlayer.PlayerSide))
			{
				m_IsTurnActive = false;
				m_CurrentPlayer.EndTurn();
				m_GameManager.RequireNextTurn();
				return true;
			}
			else
			{
				return false;
			}
		}

	#endregion

	
	#region Private Methods
	#endregion

	
	#region Accessors

		public BoardState GetBoardStateCopy()
		{
			return (m_Board != null) ? m_Board.GetBoardStateCopy() : null;
		}

		public bool IsTurnActive
		{
			get { return m_IsTurnActive; }
		}

		public EPlayerSide PlayerSide
		{
			get { return m_CurrentPlayer.PlayerSide; }
		}

	#endregion

}