﻿#region Headers

	using UnityEngine;

	using MuffinTools;

	[AddComponentMenu("Scripts/Gameplay/Game Manager")]

#endregion

///<summary>
/// 
///		
/// 
///</summary>
public class GameManager : MonoBehaviour
{

	#region Enums & Subclasses

		private enum ETurnManagement
		{
			Auto,
			Manual
		}
		 
	#endregion


	#region Attributes

		// Constants & Statics

		public const int c_NbPlayers = 2;
		private const EPlayerSide c_FirstPlayerSide = EPlayerSide.Black;

		// References

		private Board m_Board = null;
		private BoardPlayer[] m_Players = null;

		// Settings

		[Header("Settings")]

		[SerializeField]
		private ETurnManagement m_TurnManagement = ETurnManagement.Auto;

		[SerializeField]
		private bool m_ActivateDebug = false;

		// Flow

		private int m_CurrentPlayerIndex = 0;
		private bool m_WaitForNextTurn = false;

	#endregion

	
	#region Engine Methods

		private void Start()
		{
			InitBoard();
			InitPlayers();

			// Starts the first turn.
			NextTurn();
		}

	#endregion

	
	#region Public Methods

		public void RequireNextTurn()
		{
			EventsManager.TriggerEvent(EventsList.c_GameEndTurn);
			if(m_TurnManagement == ETurnManagement.Auto)
			{
				NextTurn();
				return;
			}

			m_WaitForNextTurn = !m_WaitForNextTurn;
			if(!m_WaitForNextTurn)
			{
				NextTurn();
			}
			else
			{
				EventsManager.TriggerEvent(EventsList.c_GameRequireNextTurn);
			}
		}

	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods

		/// <summary>
		/// Spawns Board in scene.
		/// </summary>
		private void InitBoard()
		{
			try
			{
				GameObject obj = Instantiate(GameConfiguration.GameResources.BoardPrefab.gameObject, Vector3.zero, Quaternion.identity, transform);
				m_Board = obj.GetComponent<Board>();
                InitBoardState(m_Board);
				EventsManager.TriggerEvent(EventsList.c_BoardInit, m_Board.GetDimensionsAsCoords());
			}
			catch
			{
				Debug.LogError("The Board prefab has not been set in GameResources asset in the GameConfiguration asset (or maybe it doesn't have Board component).");
			}
		}

		/// <summary>
		/// Initialized the given board.
		/// </summary>
        private void InitBoardState(Board _Board)
        {
            try
			{
				if (_Board != null)
                {
					_Board.InitBoardState(GameConfiguration.GameSettings.InitialBoardState);
                }
                else
                {
                    Debug.LogError("The given Board is null");
                }
			}
			catch
			{
				Debug.LogError("The initial BoardState has not been set in GameSettings asset in the GameConfiguration asset.");
			}
        }

		/// <summary>
		/// Spawn and initialize Players in scene.
		/// </summary>
		private void InitPlayers()
		{
			PlayerAsset[] assets = GetPlayerAssets();
			if(assets != null && assets.Length < c_NbPlayers)
			{
				Debug.LogError("There's not enough PlayerAsset set in GameSettings in the GameConfiguration asset. You must set at least 2 PlayerAsset (if there's more of 2 asset defined in the Players array, they'll be ignored)");
				return;
			}

			Player prefab = GetPlayerPrefab();

			GameObject root = new GameObject("Players");
			m_Players = new BoardPlayer[c_NbPlayers];

			GameSettings.EPlayerChoice firstPlayer = GameConfiguration.GameSettings.GetFirstPlayer();

			for(int i = 0; i < c_NbPlayers; i++)
			{
				EPlayerSide side = EPlayerSide.White;
				if
				(
					i == 0 && firstPlayer == GameSettings.EPlayerChoice.Player1 ||
					i == 1 && firstPlayer == GameSettings.EPlayerChoice.Player2
				)
				{
					m_CurrentPlayerIndex = (i == 0) ? 1 : 0;
					side = EPlayerSide.Black;
				}
				m_Players[i] = SpawnPlayer(assets[i], prefab, side, root.transform);
				EventsManager.TriggerEvent(EventsList.c_PlayerInit, new PlayerEventInfos(side, m_Players[i].PlayerName, m_Board.CountPawns(side)));
			}
		}

		/// <summary>
		/// Spawn a Player and return it.
		/// </summary>
		/// <param name="_PlayerLabel">Add this label at the end of the player's name.</param>
		/// <param name="_Prefab">Assumes that this prefab is valid.</param>
		/// <param name="_Parent">Assumes that this parent object is valid.</param>
		private BoardPlayer SpawnPlayer(PlayerAsset _Player, Player _Prefab, EPlayerSide _PlayerSide, Transform _Parent = null)
		{
			GameObject obj = Instantiate(_Prefab.gameObject, Vector3.zero, Quaternion.identity, _Parent);
			
			Player player = obj.GetComponent<Player>();
			player.SetPlayerAsset(_Player);
			player.name = player.PlayerName + " (Player" + (_PlayerSide == EPlayerSide.Black ? 1 : 2) + ")";

			return new BoardPlayer(player, _PlayerSide);
		}

		/// <summary>
		/// Starts the turn of the given BoardPlayer.
		/// </summary>
		private void BeginTurn(BoardPlayer _Player)
		{
			if(m_ActivateDebug)
			{
				Debug.Log("Begin turn of " + m_Players[m_CurrentPlayerIndex].PlayerName + " (" + OthelloHelpers.GetPlayerSideName(m_Players[m_CurrentPlayerIndex].PlayerSide) + ")");
			}
			EventsManager.TriggerEvent(EventsList.c_GameBeginTurn, _Player.PlayerSide);
			PlayerTurn turn = new PlayerTurn(this, m_Board, _Player);
			m_Players[m_CurrentPlayerIndex].BeginTurn(turn);
		}

		/// <summary>
		/// Determines who is the winner and throw an event to declare it.
		/// </summary>
		private void DeclareWinner()
		{
			int player1Pawns = m_Board.CountPawns(EPlayerSide.Black);
			int player2Pawns = m_Board.CountPawns(EPlayerSide.White);

			if(player1Pawns == player2Pawns)
			{
				EventsManager.TriggerEvent(EventsList.c_GameWon);
				return;
			}

			EPlayerSide winnerSide = (player1Pawns > player2Pawns) ? EPlayerSide.Black : EPlayerSide.White;
			int maxPawns = Mathf.Max(player1Pawns, player2Pawns);

			WinnerInfos infos = new WinnerInfos(GetBoardPlayer(winnerSide).PlayerName, winnerSide, maxPawns);

			EventsManager.TriggerEvent(EventsList.c_GameWon, (object)infos);
		}

		private void NextTurn()
		{
			m_CurrentPlayerIndex = GetNextPlayerIndex();

			if (m_Board.HasPossibleMoves(m_Players[m_CurrentPlayerIndex].PlayerSide))
			{
				BeginTurn(m_Players[m_CurrentPlayerIndex]);
			}

			else
			{
				int opponentIndex = GetNextPlayerIndex();
				if (m_Board.HasPossibleMoves(m_Players[opponentIndex].PlayerSide))
				{
					m_CurrentPlayerIndex = opponentIndex;
					BeginTurn(m_Players[m_CurrentPlayerIndex]);
				}

				else
				{
					EventsManager.TriggerEvent(EventsList.c_GameEnd);
					DeclareWinner();
				}
			}
		}

	#endregion

	
	#region Accessors

		/// <summary>
		/// Returns the first found BoardPlayer in the list of the given side.
		/// </summary>
		private BoardPlayer GetBoardPlayer(EPlayerSide _PlayerSide)
		{
			int count = m_Players.Length;
			for(int i = 0; i < count; i++)
			{
				if(m_Players[i].PlayerSide == _PlayerSide)
				{
					return m_Players[i];
				}
			}

			return null;
		}

		private PlayerAsset[] GetPlayerAssets()
		{
			try
			{
				return GameConfiguration.GameSettings.Players;
			}
			catch
			{
				Debug.LogError("The Player Assets has not been set in GameSettings asset in the GameConfiguration asset. Note that you must set 2 of them.");
				return null;
			}
		}

		private Player GetPlayerPrefab()
		{
			try
			{
				return GameConfiguration.GameResources.PlayerPrefab;
			}
			catch
			{
				Debug.LogError("The Player prefab has not been set in GameResources asset in the GameConfiguration asset (or maybe it doesn't have Player component).");
				return null;
			}
		}

		/// <summary>
		/// Switch to the next player index.
		/// </summary>
		private int GetNextPlayerIndex()
		{
			return (m_CurrentPlayerIndex == 0) ? 1 : 0;
		}

	#endregion

}