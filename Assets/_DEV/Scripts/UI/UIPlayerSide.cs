﻿#region Headers

	using UnityEngine;
	using UnityEngine.UI;
	
	using MuffinTools;

#endregion


///<summary>
/// 
///</summary>
public class UIPlayerSide : MonoBehaviour
{

	#region Attributes

		// Reference

		[Header("References")]

		[SerializeField]
		private Text m_PlayerNameText = null;

		[SerializeField]
		private Text m_PlayerScoreText = null;

		[SerializeField]
		private Image m_PlayerAvatar = null;

		[SerializeField]
		private GameObject m_CurrentTurnFeedback = null;

		// Settings

		[Header("Settings")]

		[SerializeField]
		private EPlayerSide m_PlayerSide = EPlayerSide.Black;

	#endregion

	
	#region Engine Methods

		private void Awake()
		{
			m_CurrentTurnFeedback.SetActive(false);
		}

		private void OnEnable()
		{
			EventsManager.StartListening(EventsList.c_PlayerInit, OnPlayerInit);
			EventsManager.StartListening(EventsList.c_PlayerMoveSuccess, OnPlayerMoveSuccess);
			EventsManager.StartListening(EventsList.c_GameBeginTurn, OnBeginTurn);
			EventsManager.StartListening(EventsList.c_GameEndTurn, OnEndTurn);
		}

		private void OnDisable()
		{
			EventsManager.StopListening(EventsList.c_PlayerInit, OnPlayerInit);
			EventsManager.StopListening(EventsList.c_PlayerMoveSuccess, OnPlayerMoveSuccess);
			EventsManager.StopListening(EventsList.c_GameBeginTurn, OnBeginTurn);
			EventsManager.StopListening(EventsList.c_GameEndTurn, OnEndTurn);
		}

	#endregion

	
	#region Private Methods

		private void OnPlayerMoveSuccess(object _Params)
		{
			PlayerEventInfos infos = (PlayerEventInfos)_Params;
			if(infos.playerSide == m_PlayerSide)
			{
				m_PlayerScoreText.text = infos.playerScore.ToString();
			}
			else
			{
				m_PlayerScoreText.text = infos.opponentScore.ToString();
			}
		}

		private void OnPlayerInit(object _Params)
		{
			PlayerEventInfos infos = (PlayerEventInfos)_Params;
			if(infos.playerSide == m_PlayerSide)
			{
				m_PlayerNameText.text	= infos.playerName;
				m_PlayerScoreText.text	= infos.playerScore.ToString();
				m_PlayerAvatar.color	= GameConfiguration.GameSettings.GetPlayerColor(m_PlayerSide);
			}
		}

		private void OnBeginTurn(object _Params)
		{
			EPlayerSide playerSide = (EPlayerSide)_Params;
			if(playerSide == m_PlayerSide)
			{
				m_CurrentTurnFeedback.SetActive(true);
			}
		}

		private void OnEndTurn(object _Params)
		{
			m_CurrentTurnFeedback.SetActive(false);
		}

	#endregion

}