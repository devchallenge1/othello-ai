﻿#region Headers

	using UnityEngine;
	
	using MuffinTools;

#endregion

public class UIProcessControls : MonoBehaviour
{

	#region Attributes

		// Constants & Statics

		private const string c_AnimBool = "RequireNextTurn";

		// References

		[Header("References")]

		[SerializeField]
		private GameObject m_NextTurnButton = null;

	#endregion

	
	#region Engine Methods

		private void Awake()
		{
			m_NextTurnButton.SetActive(false);
		}

		private void OnEnable()
		{
			EventsManager.StartListening(EventsList.c_GameBeginTurn, OnBeginTurn);
			EventsManager.StartListening(EventsList.c_GameRequireNextTurn, OnRequireNextTurn);
			EventsManager.StartListening(EventsList.c_GameEnd, OnGameEnd);
		}

		private void OnDisable()
		{
			EventsManager.StopListening(EventsList.c_GameBeginTurn, OnBeginTurn);
			EventsManager.StopListening(EventsList.c_GameRequireNextTurn, OnRequireNextTurn);
			EventsManager.StopListening(EventsList.c_GameEnd, OnGameEnd);
		}

	#endregion

	
	#region Private Methods

		private void OnBeginTurn(object _Params)
		{
			if(m_NextTurnButton.activeSelf)
			{
				m_NextTurnButton.GetComponent<Animator>().SetBool(c_AnimBool, false);
				m_NextTurnButton.SetActive(false);
			}
		}

		private void OnRequireNextTurn(object _Params)
		{
			m_NextTurnButton.SetActive(true);
			m_NextTurnButton.GetComponent<Animator>().SetBool(c_AnimBool, true);
		}

		private void OnGameEnd(object _Params)
		{
			m_NextTurnButton.SetActive(false);
		}

	#endregion

}