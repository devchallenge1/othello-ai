﻿#region Headers

	using UnityEngine;
	using UnityEngine.SceneManagement;

#endregion


public class UIControls : MonoBehaviour
{

	#region Attributes
	#endregion

	
	#region Engine Methods

		public void RestartGame()
		{
			SceneManager.LoadScene(0);
		}

	#endregion

	
	#region Public Methods
	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods
	#endregion

	
	#region Accessors
	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}