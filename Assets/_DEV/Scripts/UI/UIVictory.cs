﻿#region Headers

	using UnityEngine;
	using UnityEngine.UI;

	using MuffinTools;

#endregion

public class UIVictory : MonoBehaviour
{

	#region Attributes

		// References

		[Header("References")]

		[SerializeField]
		private GameObject m_WinnerView = null;

		[SerializeField]
		private Text m_WinnerText = null;

		[SerializeField]
		private GameObject m_ExAequoView = null;

		// Settings

		[Header("Settings")]

		[SerializeField]
		private string m_WinPhrase = "wins the game !";

	#endregion

	
	#region Engine Methods

		private void Awake()
		{
			m_WinnerView.SetActive(false);
			m_ExAequoView.SetActive(false);
		}

		private void OnEnable()
		{
			EventsManager.StartListening(EventsList.c_GameWon, OnGameWon);
		}

		private void OnDisable()
		{
			EventsManager.StopListening(EventsList.c_GameWon, OnGameWon);
		}

	#endregion

	
	#region Private Methods

		private void OnGameWon(object _Params)
		{
			if(_Params == null)
			{
				m_ExAequoView.SetActive(true);
			}

			else
			{
				WinnerInfos infos = (WinnerInfos)_Params;
				m_WinnerText.text = infos.PlayerName + " " + m_WinPhrase;
				m_WinnerView.SetActive(true);
			}
		}

	#endregion

}