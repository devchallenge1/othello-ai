﻿///<summary>
/// 
///		Contract for Player entities.
/// 
///</summary>
public interface IPlayer
{

	#region Methods

		/// <summary>
		/// Starts the turn of the current Player.
		/// </summary>
		/// <param name="_Turn">Informations about the current turn.</param>
		void BeginTurn(PlayerTurn _Turn);

		/// <summary>
		/// Called when this Player's turn ends (just-after it placed a pawn on the board).
		/// </summary>
		void EndTurn();

		/// <summary>
		/// Ends the game... making the current player lose.
		/// </summary>
		void Surrender();

	#endregion

}