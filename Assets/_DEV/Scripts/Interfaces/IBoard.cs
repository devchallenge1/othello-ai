﻿#region Headers

	using System.Collections.Generic;

#endregion

///<summary>
/// 
///		Contract for object that implement methods to control and analyze the game board.
/// 
///</summary>
public interface IBoard
{

	#region Methods

		/// <summary>
		/// Check if a new pawn can 
		/// </summary>
		/// Count all of the pawns on the board or just for a given Player.
		/// </summary>
		/// <param name="_Player">The Player who 
		/// <returns></returns>
		bool CanAppendPawn(int _X, int _Y, EPlayerSide _PlayerSide);

		/// <summary>
		/// Count all of the pawns on the board.
		/// </summary>
		int CountPawns();

		/// <summary>
		/// Count all of the pawns of a given PlayerSide on the board.
		/// </summary>
		/// <param name="_PlayerSide">The player's side whom the pawns are to be counted.</param>
		int CountPawns(EPlayerSide _PlayerSide);

		/// <summary>
		/// Add a new pawn on the board, if possible.
		/// </summary>
		/// <param name="_X">X position on the board's grid.</param>
		/// <param name="_Y">Y position on the board's grid.</param>
		/// <param name="_PlayerSide">The player side of the pawn to add.</param>
		/// <returns>Returns true if the opeartion is successful, false if not.</returns>
		bool AppendPawn(int _X, int _Y, EPlayerSide _PlayerSide);

		/// <summary>
		/// Get all the possible moves of a give player side.
		/// </summary>
		List<BoardMove> FindPossibleMoves(EPlayerSide _PlayerSide);

	#endregion

	
	#region Accessors

		/// <summary>
		/// Checks if the given player side has possible moves.
		/// </summary>
		bool HasPossibleMoves(EPlayerSide _PlayerSide);

	#endregion

}