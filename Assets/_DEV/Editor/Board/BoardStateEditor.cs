﻿#region Headers

	using UnityEngine;
	using UnityEditor;

	using MuffinTools;

#endregion

public class BoardStateEditor : EditorWindow
{

	#region Enums & Subclasses

		private struct CellStateGUIStyle
		{
			public CellState cellState;
			public GUIStyle style;

			public CellStateGUIStyle(CellState _CellState, GUIStyle _Style)
			{
				cellState	= _CellState;
				style		= _Style;
			}
		}
		
	#endregion


	#region Attributes

		// Constants & Statics

			// Menu Items

		private const string c_MenuItem = "Tools/Board State Editor";
		private const string c_ContextMenuItem = "Assets/Othello/Edit Board State";
		private const string c_WindowTitle = "Board State Editor";

		private const int c_Priority = 1;

			// Values

		private const string c_BoardTab = "Board";
		private const string c_FileTab = "File";
		private static string[] s_Tabs = { c_BoardTab, c_FileTab };

		private const string c_DefaultBoardStatesPath = "Assets/Resources";
		private const string c_AssetExtension = ".asset";

		private const int c_MinBoardDimension = 2;
		private const int c_MaxBoardDimension = 1000;

			// UI

				// Dimensions

		private const float c_OpenBoardStateMaxWidth = 250.0f;
		private const float c_OpenBoardStateButtonHeight = 30.0f;
		private const float c_LeftSideWidth = 220.0f;
		private const float c_TabsHeight = 22.0f;
		private const float c_FieldHeight = 16.0f;
		private const float c_CellWidth = 70.0f;
		private const float c_CellHeight = 70.0f;
		private const float c_CellLabelSize = 40.0f;
			
				// Margins

		private const float c_SmallMargin = 2.0f;
		private const float c_MediumMargin = 6.0f;
		private const float c_Margin = 10.0f;

				// Styles

		private CellStateGUIStyle[] m_CellStateStyles;

		// Flow

		private BoardState m_BoardState = null;

		private int m_SelectedTabIndex = 0;
		private Vector2 m_RightSizePosition = Vector2.zero;

	#endregion


	#region Engine Methods

		private void OnEnable()
		{
			InitCellStateStyles();
		}

		private void OnGUI()
		{
			Vector2 rectPos = new Vector2(c_MediumMargin, c_MediumMargin);
			Vector2 rectSize = position.size;
			rectSize.y -= c_MediumMargin * 2;
			rectSize.x -= c_MediumMargin * 2;

			if(m_BoardState == null)
			{
				DrawOpenBoardState(rectPos, rectSize);
				return;
			}

			rectSize = new Vector2(c_LeftSideWidth, position.height - c_MediumMargin * 2);

			DrawLeftSide(rectPos, rectSize);

			rectPos.x += rectSize.x + c_MediumMargin * 2;
			rectSize.y += c_MediumMargin;
			rectSize.x = position.width - (c_LeftSideWidth + c_Margin + c_MediumMargin);

			DrawRightSide(rectPos, rectSize);
		}

	#endregion


	#region Initialization

		private void InitCellStateStyles()
		{
			GUIStyle style = new GUIStyle();
			m_CellStateStyles = new CellStateGUIStyle[4];

			Texture2D texture = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/_DEV/Editor/Images/TX_White_Cell_01.png");
			style.normal.background = texture;
			style.normal.textColor = Color.black;
			m_CellStateStyles[0] = new CellStateGUIStyle(CellState.White, new GUIStyle(style));

			texture = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/_DEV/Editor/Images/TX_Black_Cell_01.png");
			style.normal.background = texture;
			style.normal.textColor = Color.white;
			m_CellStateStyles[1] = new CellStateGUIStyle(CellState.Black, new GUIStyle(style));

			texture = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/_DEV/Editor/Images/TX_Empty_Cell_01.png");
			style.normal.background = texture;
			m_CellStateStyles[2] = new CellStateGUIStyle(CellState.Empty, new GUIStyle(style));
			m_CellStateStyles[3] = new CellStateGUIStyle(CellState.Empty2, new GUIStyle(style));
		}

	#endregion


	#region Public Methods

		/// <summary>
		/// Show the BoardStateEditor window with the given BoardState open.
		/// </summary>
		public static void OpenEditor(BoardState _BoardState)
		{
			if(_BoardState == null)
			{
				Debug.LogWarning("The BoardState to edit is null");
				return;
			}

			BoardStateEditor window = EditorWindow.GetWindow<BoardStateEditor>(false, c_WindowTitle, true) as BoardStateEditor;
			window.m_BoardState = _BoardState;
			window.OnOpenBoardState();
			window.Show();
		}

	#endregion


	#region Private Methods
		
		/// <summary>
		/// Get the window of this tool, or create it if it's not already open.
		/// </summary>
		[MenuItem(c_MenuItem, false, c_Priority)]
		private static void ShowWindow()
		{
			BoardStateEditor window = EditorWindow.GetWindow<BoardStateEditor>(false, c_WindowTitle, true) as BoardStateEditor;
			window.Show();
		}

		/// <summary>
		/// Show the BoardStateEditor window with the selected BoardState open.
		/// </summary>
		[MenuItem(c_ContextMenuItem, false, c_Priority)]
		private static void ShowWindowContext()
		{
			OpenEditor(Selection.activeObject as BoardState);
		}

		/// <summary>
		/// Checks if the currently selected element in Project view is a BoardState asset.
		/// </summary>
		[MenuItem(c_ContextMenuItem, true)]
		private static bool ShowWindowContextValidation()
		{
			return (Selection.activeObject.GetType() == typeof(BoardState));
		}

		/// <summary>
		/// Draws an "Open Board State" invite.
		/// </summary>
		private void DrawOpenBoardState(Vector2 _Position, Vector2 _Size)
		{
			_Size.x = Mathf.Clamp(_Size.x, 0, c_OpenBoardStateMaxWidth);
			_Position.x = (position.width / 2 - _Size.x / 2);
			_Position.y = _Size.y / 2 - (c_OpenBoardStateButtonHeight * 2 + c_FieldHeight) / 2;

			DrawLoadBoardStateCommands(_Position, _Size);
		}

		private void DrawLoadBoardStateCommands(Vector2 _Position, Vector2 _Size)
		{
			_Size.y = c_FieldHeight;

			BoardState boardState = EditorGUI.ObjectField(new Rect(_Position, _Size), m_BoardState, typeof(BoardState), false) as BoardState;
			if (boardState != m_BoardState)
			{
				m_BoardState = boardState;
				OnOpenBoardState();
				return;
			}

			_Position.y += _Size.y + c_MediumMargin;
			_Size.y = c_OpenBoardStateButtonHeight;

			// Display "Open board state" button
			if (GUI.Button(new Rect(_Position, _Size), "Browse Board State..."))
			{
				OpenBoardState();
			}

			_Position.y += c_OpenBoardStateButtonHeight + c_MediumMargin;

			// Display "Create new board state" button
			if (GUI.Button(new Rect(_Position, _Size), "Create New Baord State"))
			{
				string path = CreateNewBoardState();
				if (!string.IsNullOrEmpty(path))
				{
					LoadBoardStateAtPath(path);
				}
			}
		}

		/// <summary>
		/// Draws the left side panel.
		/// </summary>
		private void DrawLeftSide(Vector2 _Position, Vector2 _Size)
		{
			Vector2 originalSize = _Size;

			// Draw toolbar
			_Size.y = c_TabsHeight;
			m_SelectedTabIndex = GUI.Toolbar(new Rect(_Position, _Size), m_SelectedTabIndex, s_Tabs);

			_Position.y += c_Margin + _Size.y;
			_Size.y = originalSize.y - _Position.y;

			// Draw tab GUI
			if(SelectedTab == c_BoardTab)
			{
				DrawBoardTab(_Position, _Size);
			}
			else if(SelectedTab == c_FileTab)
			{
				DrawFileTab(_Position, _Size);
			}
			else
			{
				EditorGUI.HelpBox(new Rect(_Position, _Size), "The current tab has no defined behaviour...", MessageType.Error);
			}
		}

		/// <summary>
		/// Draws Board settings in left side.
		/// </summary>
		private void DrawBoardTab(Vector2 _Position, Vector2 _Size)
		{
			// Draw title
			_Size.y = c_FieldHeight;
			EditorGUI.LabelField(new Rect(_Position, _Size), "Board", EditorStyles.boldLabel);

			// Draw "Width field"
			_Position.y += c_FieldHeight + c_SmallMargin;
			int boardWidth = Mathf.Clamp(EditorGUI.IntField(new Rect(_Position, _Size), "Width", m_BoardState.Width), c_MinBoardDimension, c_MaxBoardDimension);

			// Draw "Height" field
			_Position.y += c_FieldHeight + c_SmallMargin;
			int boardHeight = Mathf.Clamp(EditorGUI.IntField(new Rect(_Position, _Size), "Height", m_BoardState.Height), c_MinBoardDimension, c_MaxBoardDimension);

			// Resize board if necessary
			if(boardWidth != m_BoardState.Width || boardHeight != m_BoardState.Height)
			{
				m_BoardState.Resize(boardWidth, boardHeight);
			}
		}

		/// <summary>
		/// Draws File tab.
		/// </summary>
		private void DrawFileTab(Vector2 _Position, Vector2 _Size)
		{
			_Size.y = c_FieldHeight;
			EditorGUI.LabelField(new Rect(_Position, _Size), "File", EditorStyles.boldLabel);

			_Position.y += c_FieldHeight + c_SmallMargin;
			DrawLoadBoardStateCommands(_Position, _Size);
		}

		/// <summary>
		/// Draw the window's right part (board).
		/// </summary>
		private void DrawRightSide(Vector2 _Position, Vector2 _Size)
		{
			if(m_BoardState != null)
			{
				Vector2 boxPosition = _Position;
				Vector2 boxSize = new Vector2(c_CellWidth, c_CellHeight);
				
				Rect scrollViewRect = new Rect(_Position, _Size);
				scrollViewRect.width = m_BoardState.Width * boxSize.x;
				scrollViewRect.height = m_BoardState.Height * boxSize.y;

				// Draw board as a scroll view
				m_RightSizePosition = GUI.BeginScrollView(new Rect(_Position, _Size), m_RightSizePosition, scrollViewRect);
				{
					for(int y = 0; y < m_BoardState.Height; y++)
					{
						boxPosition.x = _Position.x;
						for(int x = 0; x < m_BoardState.Width; x++)
						{
							CellState state = m_BoardState[x, y];

							GUI.Box(new Rect(boxPosition, boxSize), "", GetStyleForCellState(state));

							Vector2 pos = new Vector2(boxPosition.x + c_SmallMargin * 2, boxPosition.y + c_SmallMargin * 2);
							Vector2 size = new Vector2(boxSize.x - c_SmallMargin * 2, c_FieldHeight);

							// Draw coords
							EditorGUI.LabelField(new Rect(pos, size), "   (" + x + "; " + y + ")");

							// Draw "Is free" field
							pos.y += c_FieldHeight + c_Margin;
							EditorGUI.LabelField(new Rect(pos, new Vector2(c_CellLabelSize, size.y)), "Free");
							state.isFree = EditorGUI.Toggle(new Rect(new Vector2(pos.x + c_CellLabelSize + c_SmallMargin, pos.y), new Vector2(c_FieldHeight, c_FieldHeight)), state.isFree);

							// Draw "Player side" field
							if(state.isFree)
							{
								GUI.enabled = false;
							}
							pos.y += c_FieldHeight + c_SmallMargin;
							state.playerSide = (EPlayerSide)EditorGUI.EnumPopup(new Rect(pos, size), state.playerSide);
							GUI.enabled = true;

							// Apply changes
							if(state != m_BoardState[x, y])
							{
								m_BoardState.SetCellState(x, y, state);
							}

							boxPosition.x += boxSize.x;
						}
						boxPosition.y += boxSize.y;
					}
				}
				GUI.EndScrollView();
			}
		}

		/// <summary>
		/// Pop an "Open file panel" and load the selected BoardState if possible.
		/// </summary>
		private void OpenBoardState()
		{
			string absPath = EditorUtility.OpenFilePanel("Open Board State", c_DefaultBoardStatesPath, "asset");
			if(EditorHelpers.IsPathRelativeToCurrentProjectFolder(absPath))
			{
				LoadBoardStateAtPath(EditorHelpers.GetPathRelativeToCurrentProjectFolder(absPath, true));
			}
			else
			{
				Debug.LogWarning("You must select an asset in your project's Assets folder.");
			}
		}

		/// <summary>
		/// Pop a "Save file panel" and return the created asset's relative path.
		/// </summary>
		private string CreateNewBoardState()
		{
			string absPath = EditorUtility.SaveFilePanel("Create new Board State", c_DefaultBoardStatesPath, "NewBoardState", "asset");
			if (EditorHelpers.IsPathRelativeToCurrentProjectFolder(absPath))
			{
				AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<BoardState>(), EditorHelpers.GetPathRelativeToCurrentProjectFolder(absPath, true));
				return EditorHelpers.GetPathRelativeToCurrentProjectFolder(absPath, true);
			}
			else
			{
				Debug.LogWarning("You must place this asset in your project's Assets folder.");
			}

			return string.Empty;
		}

		/// <summary>
		/// Open the BoardState asset at the given path if it's valid.
		/// </summary>
		private void LoadBoardStateAtPath(string _RelativePath)
		{
			BoardState boardState = AssetDatabase.LoadAssetAtPath<BoardState>(_RelativePath);

			if(boardState != null)
			{
				m_BoardState = boardState;
				OnOpenBoardState();
			}
			else
			{
				Debug.LogWarning("The BoardState to edit is null");
			}
		}

		private void OnOpenBoardState()
		{
			
		}

	#endregion


	#region Accessors

		private string SelectedTab
		{
			get { return s_Tabs[m_SelectedTabIndex]; }
		}

		private GUIStyle GetStyleForCellState(CellState _CellState, bool debug = false)
		{
			int count = m_CellStateStyles.Length;
			for(int i = 0; i < count; i++)
			{
				if (_CellState == m_CellStateStyles[i].cellState)
				{
					if(debug)
					{
						Debug.Log("Style index = " + i);
					}
					return m_CellStateStyles[i].style;
				}
			}

			return GUIStyle.none;
		}

	#endregion

}