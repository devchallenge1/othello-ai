﻿#region Headers

	using UnityEngine;
	using UnityEditor;
	
	using MuffinTools;

#endregion

[CustomEditor(typeof(BoardState))]
public class BoardStateCustomInspector : TargetEditor<BoardState>
{

	#region Engine Methods

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if(GUILayout.Button("Open Board State"))
			{
				BoardStateEditor.OpenEditor(TypedTarget);
			}
		}

	#endregion

}